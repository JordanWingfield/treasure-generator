﻿namespace Treasure.UI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.LevelBoxLabel_20 = new System.Windows.Forms.Label();
            this.LevelBoxLabel_19 = new System.Windows.Forms.Label();
            this.LevelBoxLabel_18 = new System.Windows.Forms.Label();
            this.LevelBoxLabel_17 = new System.Windows.Forms.Label();
            this.LevelBoxLabel_16 = new System.Windows.Forms.Label();
            this.LevelBoxLabel_15 = new System.Windows.Forms.Label();
            this.LevelBoxLabel_14 = new System.Windows.Forms.Label();
            this.LevelBoxLabel_13 = new System.Windows.Forms.Label();
            this.LevelBoxLabel_12 = new System.Windows.Forms.Label();
            this.LevelBoxLabel_11 = new System.Windows.Forms.Label();
            this.LevelBoxLabel_10 = new System.Windows.Forms.Label();
            this.LevelBoxLabel_9 = new System.Windows.Forms.Label();
            this.LevelBoxLabel_8 = new System.Windows.Forms.Label();
            this.LevelBoxLabel_7 = new System.Windows.Forms.Label();
            this.LevelBoxLabel_6 = new System.Windows.Forms.Label();
            this.LevelBoxLabel_5 = new System.Windows.Forms.Label();
            this.LevelBoxLabel_4 = new System.Windows.Forms.Label();
            this.LevelBoxLabel_3 = new System.Windows.Forms.Label();
            this.LevelBoxLabel_2 = new System.Windows.Forms.Label();
            this.LevelSelectBox_20 = new System.Windows.Forms.CheckBox();
            this.LevelSelectBox_19 = new System.Windows.Forms.CheckBox();
            this.LevelSelectBox_18 = new System.Windows.Forms.CheckBox();
            this.LevelSelectBox_17 = new System.Windows.Forms.CheckBox();
            this.LevelSelectBox_16 = new System.Windows.Forms.CheckBox();
            this.LevelSelectBox_15 = new System.Windows.Forms.CheckBox();
            this.LevelSelectBox_14 = new System.Windows.Forms.CheckBox();
            this.LevelSelectBox_13 = new System.Windows.Forms.CheckBox();
            this.LevelSelectBox_12 = new System.Windows.Forms.CheckBox();
            this.LevelSelectBox_11 = new System.Windows.Forms.CheckBox();
            this.LevelSelectBox_10 = new System.Windows.Forms.CheckBox();
            this.LevelSelectBox_9 = new System.Windows.Forms.CheckBox();
            this.LevelSelectBox_8 = new System.Windows.Forms.CheckBox();
            this.LevelSelectBox_7 = new System.Windows.Forms.CheckBox();
            this.LevelSelectBox_6 = new System.Windows.Forms.CheckBox();
            this.LevelSelectBox_5 = new System.Windows.Forms.CheckBox();
            this.LevelSelectBox_4 = new System.Windows.Forms.CheckBox();
            this.LevelSelectBox_3 = new System.Windows.Forms.CheckBox();
            this.LevelSelectBox_2 = new System.Windows.Forms.CheckBox();
            this.LevelSelectBox_1 = new System.Windows.Forms.CheckBox();
            this.LevelBoxLabel_1 = new System.Windows.Forms.Label();
            this.LevelTitleLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.CoinsLoot_Checkbox = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.GoodsLoot_Checkbox = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.GoodsLoot_Gems_Checkbox = new System.Windows.Forms.CheckBox();
            this.GoodsLoot_Art_Checkbox = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.ItemsLoot_Checkbox = new System.Windows.Forms.CheckBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.ItemsLoot_AlchemicalBox = new System.Windows.Forms.CheckBox();
            this.ItemsLoot_RingsBox = new System.Windows.Forms.CheckBox();
            this.ItemsLoot_RodsBox = new System.Windows.Forms.CheckBox();
            this.ItemsLoot_ScrollsBox = new System.Windows.Forms.CheckBox();
            this.ItemsLoot_StaffsBox = new System.Windows.Forms.CheckBox();
            this.ItemsLoot_WandsBox = new System.Windows.Forms.CheckBox();
            this.ItemsLoot_WondrousBox = new System.Windows.Forms.CheckBox();
            this.ItemsLoot_PotionsBox = new System.Windows.Forms.CheckBox();
            this.ItemsLoot_ToolsGearBox = new System.Windows.Forms.CheckBox();
            this.ItemsLoot_WeaponsBox = new System.Windows.Forms.CheckBox();
            this.ItemsLoot_ArmourShieldsBox = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.SelectAll_Button = new System.Windows.Forms.Button();
            this.Deselect_Button = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.Close_Button = new System.Windows.Forms.Button();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.Clear_Button = new System.Windows.Forms.Button();
            this.Generate_Button = new System.Windows.Forms.Button();
            this.DisplayWindow = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66667F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.LevelTitleLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel9, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel10, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(13, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(767, 570);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 20;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_20, 19, 1);
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_19, 18, 1);
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_18, 17, 1);
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_17, 16, 1);
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_16, 15, 1);
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_15, 14, 1);
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_14, 13, 1);
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_13, 12, 1);
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_12, 11, 1);
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_11, 10, 1);
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_10, 9, 1);
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_9, 8, 1);
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_8, 7, 1);
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_7, 6, 1);
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_6, 5, 1);
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_5, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_4, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_3, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_2, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_20, 19, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_19, 18, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_18, 17, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_17, 16, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_16, 15, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_15, 14, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_14, 13, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_13, 12, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_12, 11, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_11, 10, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_10, 9, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_9, 8, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_8, 7, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_7, 6, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_6, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_5, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_4, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_3, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelSelectBox_1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.LevelBoxLabel_1, 0, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(258, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(506, 51);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // LevelBoxLabel_20
            // 
            this.LevelBoxLabel_20.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_20.AutoSize = true;
            this.LevelBoxLabel_20.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_20.Location = new System.Drawing.Point(479, 30);
            this.LevelBoxLabel_20.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_20.Name = "LevelBoxLabel_20";
            this.LevelBoxLabel_20.Size = new System.Drawing.Size(22, 16);
            this.LevelBoxLabel_20.TabIndex = 39;
            this.LevelBoxLabel_20.Text = "20";
            // 
            // LevelBoxLabel_19
            // 
            this.LevelBoxLabel_19.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_19.AutoSize = true;
            this.LevelBoxLabel_19.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_19.Location = new System.Drawing.Point(452, 30);
            this.LevelBoxLabel_19.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_19.Name = "LevelBoxLabel_19";
            this.LevelBoxLabel_19.Size = new System.Drawing.Size(21, 16);
            this.LevelBoxLabel_19.TabIndex = 38;
            this.LevelBoxLabel_19.Text = "19";
            // 
            // LevelBoxLabel_18
            // 
            this.LevelBoxLabel_18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_18.AutoSize = true;
            this.LevelBoxLabel_18.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_18.Location = new System.Drawing.Point(427, 30);
            this.LevelBoxLabel_18.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_18.Name = "LevelBoxLabel_18";
            this.LevelBoxLabel_18.Size = new System.Drawing.Size(21, 16);
            this.LevelBoxLabel_18.TabIndex = 37;
            this.LevelBoxLabel_18.Text = "18";
            // 
            // LevelBoxLabel_17
            // 
            this.LevelBoxLabel_17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_17.AutoSize = true;
            this.LevelBoxLabel_17.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_17.Location = new System.Drawing.Point(402, 30);
            this.LevelBoxLabel_17.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_17.Name = "LevelBoxLabel_17";
            this.LevelBoxLabel_17.Size = new System.Drawing.Size(21, 16);
            this.LevelBoxLabel_17.TabIndex = 36;
            this.LevelBoxLabel_17.Text = "17";
            // 
            // LevelBoxLabel_16
            // 
            this.LevelBoxLabel_16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_16.AutoSize = true;
            this.LevelBoxLabel_16.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_16.Location = new System.Drawing.Point(377, 30);
            this.LevelBoxLabel_16.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_16.Name = "LevelBoxLabel_16";
            this.LevelBoxLabel_16.Size = new System.Drawing.Size(21, 16);
            this.LevelBoxLabel_16.TabIndex = 35;
            this.LevelBoxLabel_16.Text = "16";
            // 
            // LevelBoxLabel_15
            // 
            this.LevelBoxLabel_15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_15.AutoSize = true;
            this.LevelBoxLabel_15.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_15.Location = new System.Drawing.Point(352, 30);
            this.LevelBoxLabel_15.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_15.Name = "LevelBoxLabel_15";
            this.LevelBoxLabel_15.Size = new System.Drawing.Size(21, 16);
            this.LevelBoxLabel_15.TabIndex = 34;
            this.LevelBoxLabel_15.Text = "15";
            // 
            // LevelBoxLabel_14
            // 
            this.LevelBoxLabel_14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_14.AutoSize = true;
            this.LevelBoxLabel_14.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_14.Location = new System.Drawing.Point(327, 30);
            this.LevelBoxLabel_14.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_14.Name = "LevelBoxLabel_14";
            this.LevelBoxLabel_14.Size = new System.Drawing.Size(21, 16);
            this.LevelBoxLabel_14.TabIndex = 33;
            this.LevelBoxLabel_14.Text = "14";
            // 
            // LevelBoxLabel_13
            // 
            this.LevelBoxLabel_13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_13.AutoSize = true;
            this.LevelBoxLabel_13.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_13.Location = new System.Drawing.Point(302, 30);
            this.LevelBoxLabel_13.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_13.Name = "LevelBoxLabel_13";
            this.LevelBoxLabel_13.Size = new System.Drawing.Size(21, 16);
            this.LevelBoxLabel_13.TabIndex = 32;
            this.LevelBoxLabel_13.Text = "13";
            // 
            // LevelBoxLabel_12
            // 
            this.LevelBoxLabel_12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_12.AutoSize = true;
            this.LevelBoxLabel_12.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_12.Location = new System.Drawing.Point(277, 30);
            this.LevelBoxLabel_12.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_12.Name = "LevelBoxLabel_12";
            this.LevelBoxLabel_12.Size = new System.Drawing.Size(21, 16);
            this.LevelBoxLabel_12.TabIndex = 31;
            this.LevelBoxLabel_12.Text = "12";
            // 
            // LevelBoxLabel_11
            // 
            this.LevelBoxLabel_11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_11.AutoSize = true;
            this.LevelBoxLabel_11.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_11.Location = new System.Drawing.Point(252, 30);
            this.LevelBoxLabel_11.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_11.Name = "LevelBoxLabel_11";
            this.LevelBoxLabel_11.Size = new System.Drawing.Size(20, 16);
            this.LevelBoxLabel_11.TabIndex = 30;
            this.LevelBoxLabel_11.Text = "11";
            // 
            // LevelBoxLabel_10
            // 
            this.LevelBoxLabel_10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_10.AutoSize = true;
            this.LevelBoxLabel_10.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_10.Location = new System.Drawing.Point(227, 30);
            this.LevelBoxLabel_10.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_10.Name = "LevelBoxLabel_10";
            this.LevelBoxLabel_10.Size = new System.Drawing.Size(21, 16);
            this.LevelBoxLabel_10.TabIndex = 29;
            this.LevelBoxLabel_10.Text = "10";
            // 
            // LevelBoxLabel_9
            // 
            this.LevelBoxLabel_9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_9.AutoSize = true;
            this.LevelBoxLabel_9.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_9.Location = new System.Drawing.Point(205, 30);
            this.LevelBoxLabel_9.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_9.Name = "LevelBoxLabel_9";
            this.LevelBoxLabel_9.Size = new System.Drawing.Size(15, 16);
            this.LevelBoxLabel_9.TabIndex = 28;
            this.LevelBoxLabel_9.Text = "9";
            // 
            // LevelBoxLabel_8
            // 
            this.LevelBoxLabel_8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_8.AutoSize = true;
            this.LevelBoxLabel_8.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_8.Location = new System.Drawing.Point(180, 30);
            this.LevelBoxLabel_8.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_8.Name = "LevelBoxLabel_8";
            this.LevelBoxLabel_8.Size = new System.Drawing.Size(15, 16);
            this.LevelBoxLabel_8.TabIndex = 27;
            this.LevelBoxLabel_8.Text = "8";
            // 
            // LevelBoxLabel_7
            // 
            this.LevelBoxLabel_7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_7.AutoSize = true;
            this.LevelBoxLabel_7.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_7.Location = new System.Drawing.Point(155, 30);
            this.LevelBoxLabel_7.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_7.Name = "LevelBoxLabel_7";
            this.LevelBoxLabel_7.Size = new System.Drawing.Size(15, 16);
            this.LevelBoxLabel_7.TabIndex = 26;
            this.LevelBoxLabel_7.Text = "7";
            // 
            // LevelBoxLabel_6
            // 
            this.LevelBoxLabel_6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_6.AutoSize = true;
            this.LevelBoxLabel_6.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_6.Location = new System.Drawing.Point(130, 30);
            this.LevelBoxLabel_6.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_6.Name = "LevelBoxLabel_6";
            this.LevelBoxLabel_6.Size = new System.Drawing.Size(15, 16);
            this.LevelBoxLabel_6.TabIndex = 25;
            this.LevelBoxLabel_6.Text = "6";
            // 
            // LevelBoxLabel_5
            // 
            this.LevelBoxLabel_5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_5.AutoSize = true;
            this.LevelBoxLabel_5.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_5.Location = new System.Drawing.Point(105, 30);
            this.LevelBoxLabel_5.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_5.Name = "LevelBoxLabel_5";
            this.LevelBoxLabel_5.Size = new System.Drawing.Size(15, 16);
            this.LevelBoxLabel_5.TabIndex = 24;
            this.LevelBoxLabel_5.Text = "5";
            // 
            // LevelBoxLabel_4
            // 
            this.LevelBoxLabel_4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_4.AutoSize = true;
            this.LevelBoxLabel_4.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_4.Location = new System.Drawing.Point(80, 30);
            this.LevelBoxLabel_4.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_4.Name = "LevelBoxLabel_4";
            this.LevelBoxLabel_4.Size = new System.Drawing.Size(15, 16);
            this.LevelBoxLabel_4.TabIndex = 23;
            this.LevelBoxLabel_4.Text = "4";
            // 
            // LevelBoxLabel_3
            // 
            this.LevelBoxLabel_3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_3.AutoSize = true;
            this.LevelBoxLabel_3.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_3.Location = new System.Drawing.Point(55, 30);
            this.LevelBoxLabel_3.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_3.Name = "LevelBoxLabel_3";
            this.LevelBoxLabel_3.Size = new System.Drawing.Size(15, 16);
            this.LevelBoxLabel_3.TabIndex = 22;
            this.LevelBoxLabel_3.Text = "3";
            // 
            // LevelBoxLabel_2
            // 
            this.LevelBoxLabel_2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_2.AutoSize = true;
            this.LevelBoxLabel_2.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_2.Location = new System.Drawing.Point(30, 30);
            this.LevelBoxLabel_2.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_2.Name = "LevelBoxLabel_2";
            this.LevelBoxLabel_2.Size = new System.Drawing.Size(15, 16);
            this.LevelBoxLabel_2.TabIndex = 21;
            this.LevelBoxLabel_2.Text = "2";
            // 
            // LevelSelectBox_20
            // 
            this.LevelSelectBox_20.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_20.AutoSize = true;
            this.LevelSelectBox_20.Location = new System.Drawing.Point(483, 5);
            this.LevelSelectBox_20.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_20.Name = "LevelSelectBox_20";
            this.LevelSelectBox_20.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_20.TabIndex = 19;
            this.LevelSelectBox_20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_20.UseVisualStyleBackColor = true;
            this.LevelSelectBox_20.CheckedChanged += new System.EventHandler(this.LevelSelectBox_20_CheckedChanged);
            this.LevelSelectBox_20.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_20_CheckedChanged);
            // 
            // LevelSelectBox_19
            // 
            this.LevelSelectBox_19.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_19.AutoSize = true;
            this.LevelSelectBox_19.Location = new System.Drawing.Point(455, 5);
            this.LevelSelectBox_19.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_19.Name = "LevelSelectBox_19";
            this.LevelSelectBox_19.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_19.TabIndex = 18;
            this.LevelSelectBox_19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_19.UseVisualStyleBackColor = true;
            this.LevelSelectBox_19.CheckedChanged += new System.EventHandler(this.LevelSelectBox_19_CheckedChanged);
            this.LevelSelectBox_19.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_19_CheckedChanged);
            // 
            // LevelSelectBox_18
            // 
            this.LevelSelectBox_18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_18.AutoSize = true;
            this.LevelSelectBox_18.Location = new System.Drawing.Point(430, 5);
            this.LevelSelectBox_18.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_18.Name = "LevelSelectBox_18";
            this.LevelSelectBox_18.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_18.TabIndex = 17;
            this.LevelSelectBox_18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_18.UseVisualStyleBackColor = true;
            this.LevelSelectBox_18.CheckedChanged += new System.EventHandler(this.LevelSelectBox_18_CheckedChanged);
            this.LevelSelectBox_18.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_18_CheckedChanged);
            // 
            // LevelSelectBox_17
            // 
            this.LevelSelectBox_17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_17.AutoSize = true;
            this.LevelSelectBox_17.Location = new System.Drawing.Point(405, 5);
            this.LevelSelectBox_17.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_17.Name = "LevelSelectBox_17";
            this.LevelSelectBox_17.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_17.TabIndex = 16;
            this.LevelSelectBox_17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_17.UseVisualStyleBackColor = true;
            this.LevelSelectBox_17.CheckedChanged += new System.EventHandler(this.LevelSelectBox_17_CheckedChanged);
            this.LevelSelectBox_17.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_17_CheckedChanged);
            // 
            // LevelSelectBox_16
            // 
            this.LevelSelectBox_16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_16.AutoSize = true;
            this.LevelSelectBox_16.Location = new System.Drawing.Point(380, 5);
            this.LevelSelectBox_16.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_16.Name = "LevelSelectBox_16";
            this.LevelSelectBox_16.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_16.TabIndex = 15;
            this.LevelSelectBox_16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_16.UseVisualStyleBackColor = true;
            this.LevelSelectBox_16.CheckedChanged += new System.EventHandler(this.LevelSelectBox_16_CheckedChanged);
            this.LevelSelectBox_16.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_16_CheckedChanged);
            // 
            // LevelSelectBox_15
            // 
            this.LevelSelectBox_15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_15.AutoSize = true;
            this.LevelSelectBox_15.Location = new System.Drawing.Point(355, 5);
            this.LevelSelectBox_15.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_15.Name = "LevelSelectBox_15";
            this.LevelSelectBox_15.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_15.TabIndex = 14;
            this.LevelSelectBox_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_15.UseVisualStyleBackColor = true;
            this.LevelSelectBox_15.CheckedChanged += new System.EventHandler(this.LevelSelectBox_15_CheckedChanged);
            this.LevelSelectBox_15.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_15_CheckedChanged);
            // 
            // LevelSelectBox_14
            // 
            this.LevelSelectBox_14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_14.AutoSize = true;
            this.LevelSelectBox_14.Location = new System.Drawing.Point(330, 5);
            this.LevelSelectBox_14.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_14.Name = "LevelSelectBox_14";
            this.LevelSelectBox_14.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_14.TabIndex = 13;
            this.LevelSelectBox_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_14.UseVisualStyleBackColor = true;
            this.LevelSelectBox_14.CheckedChanged += new System.EventHandler(this.LevelSelectBox_14_CheckedChanged);
            this.LevelSelectBox_14.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_14_CheckedChanged);
            // 
            // LevelSelectBox_13
            // 
            this.LevelSelectBox_13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_13.AutoSize = true;
            this.LevelSelectBox_13.Location = new System.Drawing.Point(305, 5);
            this.LevelSelectBox_13.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_13.Name = "LevelSelectBox_13";
            this.LevelSelectBox_13.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_13.TabIndex = 12;
            this.LevelSelectBox_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_13.UseVisualStyleBackColor = true;
            this.LevelSelectBox_13.CheckedChanged += new System.EventHandler(this.LevelSelectBox_13_CheckedChanged);
            this.LevelSelectBox_13.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_13_CheckedChanged);
            // 
            // LevelSelectBox_12
            // 
            this.LevelSelectBox_12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_12.AutoSize = true;
            this.LevelSelectBox_12.Location = new System.Drawing.Point(280, 5);
            this.LevelSelectBox_12.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_12.Name = "LevelSelectBox_12";
            this.LevelSelectBox_12.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_12.TabIndex = 11;
            this.LevelSelectBox_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_12.UseVisualStyleBackColor = true;
            this.LevelSelectBox_12.CheckedChanged += new System.EventHandler(this.LevelSelectBox_12_CheckedChanged);
            this.LevelSelectBox_12.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_12_CheckedChanged);
            // 
            // LevelSelectBox_11
            // 
            this.LevelSelectBox_11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_11.AutoSize = true;
            this.LevelSelectBox_11.Location = new System.Drawing.Point(255, 5);
            this.LevelSelectBox_11.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_11.Name = "LevelSelectBox_11";
            this.LevelSelectBox_11.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_11.TabIndex = 10;
            this.LevelSelectBox_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_11.UseVisualStyleBackColor = true;
            this.LevelSelectBox_11.CheckedChanged += new System.EventHandler(this.LevelSelectBox_11_CheckedChanged);
            this.LevelSelectBox_11.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_11_CheckedChanged);
            // 
            // LevelSelectBox_10
            // 
            this.LevelSelectBox_10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_10.AutoSize = true;
            this.LevelSelectBox_10.Location = new System.Drawing.Point(230, 5);
            this.LevelSelectBox_10.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_10.Name = "LevelSelectBox_10";
            this.LevelSelectBox_10.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_10.TabIndex = 9;
            this.LevelSelectBox_10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_10.UseVisualStyleBackColor = true;
            this.LevelSelectBox_10.CheckedChanged += new System.EventHandler(this.LevelSelectBox_10_CheckedChanged);
            this.LevelSelectBox_10.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_10_CheckedChanged);
            // 
            // LevelSelectBox_9
            // 
            this.LevelSelectBox_9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_9.AutoSize = true;
            this.LevelSelectBox_9.Location = new System.Drawing.Point(205, 5);
            this.LevelSelectBox_9.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_9.Name = "LevelSelectBox_9";
            this.LevelSelectBox_9.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_9.TabIndex = 8;
            this.LevelSelectBox_9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_9.UseVisualStyleBackColor = true;
            this.LevelSelectBox_9.CheckedChanged += new System.EventHandler(this.LevelSelectBox_9_CheckedChanged);
            this.LevelSelectBox_9.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_9_CheckedChanged);
            // 
            // LevelSelectBox_8
            // 
            this.LevelSelectBox_8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_8.AutoSize = true;
            this.LevelSelectBox_8.Location = new System.Drawing.Point(180, 5);
            this.LevelSelectBox_8.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_8.Name = "LevelSelectBox_8";
            this.LevelSelectBox_8.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_8.TabIndex = 7;
            this.LevelSelectBox_8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_8.UseVisualStyleBackColor = true;
            this.LevelSelectBox_8.CheckedChanged += new System.EventHandler(this.LevelSelectBox_8_CheckedChanged);
            this.LevelSelectBox_8.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_8_CheckedChanged);
            // 
            // LevelSelectBox_7
            // 
            this.LevelSelectBox_7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_7.AutoSize = true;
            this.LevelSelectBox_7.Location = new System.Drawing.Point(155, 5);
            this.LevelSelectBox_7.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_7.Name = "LevelSelectBox_7";
            this.LevelSelectBox_7.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_7.TabIndex = 6;
            this.LevelSelectBox_7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_7.UseVisualStyleBackColor = true;
            this.LevelSelectBox_7.CheckedChanged += new System.EventHandler(this.LevelSelectBox_7_CheckedChanged);
            this.LevelSelectBox_7.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_7_CheckedChanged);
            // 
            // LevelSelectBox_6
            // 
            this.LevelSelectBox_6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_6.AutoSize = true;
            this.LevelSelectBox_6.Location = new System.Drawing.Point(130, 5);
            this.LevelSelectBox_6.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_6.Name = "LevelSelectBox_6";
            this.LevelSelectBox_6.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_6.TabIndex = 5;
            this.LevelSelectBox_6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_6.UseVisualStyleBackColor = true;
            this.LevelSelectBox_6.CheckedChanged += new System.EventHandler(this.LevelSelectBox_6_CheckedChanged);
            this.LevelSelectBox_6.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_6_CheckedChanged);
            // 
            // LevelSelectBox_5
            // 
            this.LevelSelectBox_5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_5.AutoSize = true;
            this.LevelSelectBox_5.Location = new System.Drawing.Point(105, 5);
            this.LevelSelectBox_5.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_5.Name = "LevelSelectBox_5";
            this.LevelSelectBox_5.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_5.TabIndex = 4;
            this.LevelSelectBox_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_5.UseVisualStyleBackColor = true;
            this.LevelSelectBox_5.CheckedChanged += new System.EventHandler(this.LevelSelectBox_5_CheckedChanged);
            this.LevelSelectBox_5.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_5_CheckedChanged);
            // 
            // LevelSelectBox_4
            // 
            this.LevelSelectBox_4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_4.AutoSize = true;
            this.LevelSelectBox_4.Location = new System.Drawing.Point(80, 5);
            this.LevelSelectBox_4.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_4.Name = "LevelSelectBox_4";
            this.LevelSelectBox_4.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_4.TabIndex = 3;
            this.LevelSelectBox_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_4.UseVisualStyleBackColor = true;
            this.LevelSelectBox_4.CheckedChanged += new System.EventHandler(this.LevelSelectBox_4_CheckedChanged);
            this.LevelSelectBox_4.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_4_CheckedChanged);
            // 
            // LevelSelectBox_3
            // 
            this.LevelSelectBox_3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_3.AutoSize = true;
            this.LevelSelectBox_3.Location = new System.Drawing.Point(55, 5);
            this.LevelSelectBox_3.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_3.Name = "LevelSelectBox_3";
            this.LevelSelectBox_3.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_3.TabIndex = 2;
            this.LevelSelectBox_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_3.UseVisualStyleBackColor = true;
            this.LevelSelectBox_3.CheckedChanged += new System.EventHandler(this.LevelSelectBox_3_CheckedChanged);
            this.LevelSelectBox_3.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_3_CheckedChanged);
            // 
            // LevelSelectBox_2
            // 
            this.LevelSelectBox_2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_2.AutoSize = true;
            this.LevelSelectBox_2.Location = new System.Drawing.Point(30, 5);
            this.LevelSelectBox_2.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_2.Name = "LevelSelectBox_2";
            this.LevelSelectBox_2.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_2.TabIndex = 1;
            this.LevelSelectBox_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_2.UseVisualStyleBackColor = true;
            this.LevelSelectBox_2.CheckedChanged += new System.EventHandler(this.LevelSelectBox_2_CheckedChanged);
            this.LevelSelectBox_2.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_2_CheckedChanged);
            // 
            // LevelSelectBox_1
            // 
            this.LevelSelectBox_1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelSelectBox_1.AutoSize = true;
            this.LevelSelectBox_1.Checked = true;
            this.LevelSelectBox_1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.LevelSelectBox_1.Location = new System.Drawing.Point(5, 5);
            this.LevelSelectBox_1.Margin = new System.Windows.Forms.Padding(0);
            this.LevelSelectBox_1.Name = "LevelSelectBox_1";
            this.LevelSelectBox_1.Size = new System.Drawing.Size(15, 14);
            this.LevelSelectBox_1.TabIndex = 0;
            this.LevelSelectBox_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LevelSelectBox_1.UseVisualStyleBackColor = true;
            this.LevelSelectBox_1.CheckedChanged += new System.EventHandler(this.LevelSelectBox_1_CheckedChanged);
            this.LevelSelectBox_1.CheckStateChanged += new System.EventHandler(this.LevelSelectBox_1_CheckedChanged);
            // 
            // LevelBoxLabel_1
            // 
            this.LevelBoxLabel_1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelBoxLabel_1.AutoSize = true;
            this.LevelBoxLabel_1.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelBoxLabel_1.Location = new System.Drawing.Point(5, 30);
            this.LevelBoxLabel_1.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.LevelBoxLabel_1.Name = "LevelBoxLabel_1";
            this.LevelBoxLabel_1.Size = new System.Drawing.Size(14, 16);
            this.LevelBoxLabel_1.TabIndex = 20;
            this.LevelBoxLabel_1.Text = "1";
            // 
            // LevelTitleLabel
            // 
            this.LevelTitleLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LevelTitleLabel.AutoSize = true;
            this.LevelTitleLabel.Font = new System.Drawing.Font("Imprint MT Shadow", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LevelTitleLabel.Location = new System.Drawing.Point(53, 17);
            this.LevelTitleLabel.MaximumSize = new System.Drawing.Size(200, 100);
            this.LevelTitleLabel.Name = "LevelTitleLabel";
            this.LevelTitleLabel.Size = new System.Drawing.Size(148, 23);
            this.LevelTitleLabel.TabIndex = 1;
            this.LevelTitleLabel.Text = "Encounter Level";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.CoinsLoot_Checkbox, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel7, 0, 2);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(36, 60);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.59833F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23.22176F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 63.17992F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(216, 478);
            this.tableLayoutPanel4.TabIndex = 6;
            // 
            // CoinsLoot_Checkbox
            // 
            this.CoinsLoot_Checkbox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.CoinsLoot_Checkbox.AutoSize = true;
            this.CoinsLoot_Checkbox.Checked = true;
            this.CoinsLoot_Checkbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CoinsLoot_Checkbox.Font = new System.Drawing.Font("Imprint MT Shadow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CoinsLoot_Checkbox.Location = new System.Drawing.Point(3, 21);
            this.CoinsLoot_Checkbox.Name = "CoinsLoot_Checkbox";
            this.CoinsLoot_Checkbox.Size = new System.Drawing.Size(67, 23);
            this.CoinsLoot_Checkbox.TabIndex = 1;
            this.CoinsLoot_Checkbox.Text = "Coins";
            this.CoinsLoot_Checkbox.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Controls.Add(this.GoodsLoot_Checkbox, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel6, 0, 1);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 68);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.58427F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 67.41573F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(200, 104);
            this.tableLayoutPanel5.TabIndex = 2;
            // 
            // GoodsLoot_Checkbox
            // 
            this.GoodsLoot_Checkbox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GoodsLoot_Checkbox.AutoSize = true;
            this.GoodsLoot_Checkbox.Checked = true;
            this.GoodsLoot_Checkbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.GoodsLoot_Checkbox.Font = new System.Drawing.Font("Imprint MT Shadow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GoodsLoot_Checkbox.Location = new System.Drawing.Point(0, 4);
            this.GoodsLoot_Checkbox.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.GoodsLoot_Checkbox.Name = "GoodsLoot_Checkbox";
            this.GoodsLoot_Checkbox.Size = new System.Drawing.Size(72, 23);
            this.GoodsLoot_Checkbox.TabIndex = 0;
            this.GoodsLoot_Checkbox.Text = "Goods";
            this.GoodsLoot_Checkbox.UseVisualStyleBackColor = true;
            this.GoodsLoot_Checkbox.CheckedChanged += new System.EventHandler(this.GoodsLoot_Checkbox_CheckedChanged);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.01031F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 82.98969F));
            this.tableLayoutPanel6.Controls.Add(this.GoodsLoot_Gems_Checkbox, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.GoodsLoot_Art_Checkbox, 1, 1);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 34);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(194, 58);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // GoodsLoot_Gems_Checkbox
            // 
            this.GoodsLoot_Gems_Checkbox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GoodsLoot_Gems_Checkbox.AutoSize = true;
            this.GoodsLoot_Gems_Checkbox.Font = new System.Drawing.Font("Imprint MT Shadow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GoodsLoot_Gems_Checkbox.Location = new System.Drawing.Point(35, 3);
            this.GoodsLoot_Gems_Checkbox.Name = "GoodsLoot_Gems_Checkbox";
            this.GoodsLoot_Gems_Checkbox.Size = new System.Drawing.Size(68, 23);
            this.GoodsLoot_Gems_Checkbox.TabIndex = 2;
            this.GoodsLoot_Gems_Checkbox.Text = "Gems";
            this.GoodsLoot_Gems_Checkbox.UseVisualStyleBackColor = true;
            this.GoodsLoot_Gems_Checkbox.CheckedChanged += new System.EventHandler(this.GoodsLoot_Gems_Checkbox_CheckedChanged);
            // 
            // GoodsLoot_Art_Checkbox
            // 
            this.GoodsLoot_Art_Checkbox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.GoodsLoot_Art_Checkbox.AutoSize = true;
            this.GoodsLoot_Art_Checkbox.Font = new System.Drawing.Font("Imprint MT Shadow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GoodsLoot_Art_Checkbox.Location = new System.Drawing.Point(35, 32);
            this.GoodsLoot_Art_Checkbox.Name = "GoodsLoot_Art_Checkbox";
            this.GoodsLoot_Art_Checkbox.Size = new System.Drawing.Size(51, 23);
            this.GoodsLoot_Art_Checkbox.TabIndex = 3;
            this.GoodsLoot_Art_Checkbox.Text = "Art";
            this.GoodsLoot_Art_Checkbox.UseVisualStyleBackColor = true;
            this.GoodsLoot_Art_Checkbox.CheckedChanged += new System.EventHandler(this.GoodsLoot_Art_Checkbox_CheckedChanged);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.ItemsLoot_Checkbox, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.comboBox1, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel8, 0, 2);
            this.tableLayoutPanel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 178);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(200, 296);
            this.tableLayoutPanel7.TabIndex = 3;
            // 
            // ItemsLoot_Checkbox
            // 
            this.ItemsLoot_Checkbox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ItemsLoot_Checkbox.AutoSize = true;
            this.ItemsLoot_Checkbox.Checked = true;
            this.ItemsLoot_Checkbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ItemsLoot_Checkbox.Font = new System.Drawing.Font("Imprint MT Shadow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemsLoot_Checkbox.Location = new System.Drawing.Point(0, 3);
            this.ItemsLoot_Checkbox.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.ItemsLoot_Checkbox.Name = "ItemsLoot_Checkbox";
            this.ItemsLoot_Checkbox.Size = new System.Drawing.Size(66, 23);
            this.ItemsLoot_Checkbox.TabIndex = 2;
            this.ItemsLoot_Checkbox.Text = "Items";
            this.ItemsLoot_Checkbox.UseVisualStyleBackColor = true;
            this.ItemsLoot_Checkbox.CheckedChanged += new System.EventHandler(this.ItemsLoot_Checkbox_CheckedChanged);
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "None",
            "Mundane",
            "Minor",
            "Medium",
            "Major"});
            this.comboBox1.Location = new System.Drawing.Point(3, 32);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(194, 23);
            this.comboBox1.TabIndex = 3;
            this.comboBox1.Text = "None";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.01031F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 82.98969F));
            this.tableLayoutPanel8.Controls.Add(this.ItemsLoot_AlchemicalBox, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.ItemsLoot_RingsBox, 1, 5);
            this.tableLayoutPanel8.Controls.Add(this.ItemsLoot_RodsBox, 1, 6);
            this.tableLayoutPanel8.Controls.Add(this.ItemsLoot_ScrollsBox, 1, 7);
            this.tableLayoutPanel8.Controls.Add(this.ItemsLoot_StaffsBox, 1, 8);
            this.tableLayoutPanel8.Controls.Add(this.ItemsLoot_WandsBox, 1, 9);
            this.tableLayoutPanel8.Controls.Add(this.ItemsLoot_WondrousBox, 1, 10);
            this.tableLayoutPanel8.Controls.Add(this.ItemsLoot_PotionsBox, 1, 4);
            this.tableLayoutPanel8.Controls.Add(this.ItemsLoot_ToolsGearBox, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.ItemsLoot_WeaponsBox, 1, 3);
            this.tableLayoutPanel8.Controls.Add(this.ItemsLoot_ArmourShieldsBox, 1, 2);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 61);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 11;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(194, 232);
            this.tableLayoutPanel8.TabIndex = 4;
            // 
            // ItemsLoot_AlchemicalBox
            // 
            this.ItemsLoot_AlchemicalBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ItemsLoot_AlchemicalBox.AutoSize = true;
            this.ItemsLoot_AlchemicalBox.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemsLoot_AlchemicalBox.Location = new System.Drawing.Point(34, 2);
            this.ItemsLoot_AlchemicalBox.Margin = new System.Windows.Forms.Padding(2, 2, 1, 0);
            this.ItemsLoot_AlchemicalBox.Name = "ItemsLoot_AlchemicalBox";
            this.ItemsLoot_AlchemicalBox.Size = new System.Drawing.Size(93, 19);
            this.ItemsLoot_AlchemicalBox.TabIndex = 3;
            this.ItemsLoot_AlchemicalBox.Text = "Alchemical";
            this.ItemsLoot_AlchemicalBox.UseVisualStyleBackColor = true;
            this.ItemsLoot_AlchemicalBox.CheckedChanged += new System.EventHandler(this.ItemsLoot_AlchemicalBox_CheckedChanged);
            // 
            // ItemsLoot_RingsBox
            // 
            this.ItemsLoot_RingsBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ItemsLoot_RingsBox.AutoSize = true;
            this.ItemsLoot_RingsBox.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemsLoot_RingsBox.Location = new System.Drawing.Point(34, 107);
            this.ItemsLoot_RingsBox.Margin = new System.Windows.Forms.Padding(2, 2, 1, 0);
            this.ItemsLoot_RingsBox.Name = "ItemsLoot_RingsBox";
            this.ItemsLoot_RingsBox.Size = new System.Drawing.Size(62, 19);
            this.ItemsLoot_RingsBox.TabIndex = 8;
            this.ItemsLoot_RingsBox.Text = "Rings";
            this.ItemsLoot_RingsBox.UseVisualStyleBackColor = true;
            this.ItemsLoot_RingsBox.CheckedChanged += new System.EventHandler(this.ItemsLoot_RingsBox_CheckedChanged);
            // 
            // ItemsLoot_RodsBox
            // 
            this.ItemsLoot_RodsBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ItemsLoot_RodsBox.AutoSize = true;
            this.ItemsLoot_RodsBox.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemsLoot_RodsBox.Location = new System.Drawing.Point(34, 128);
            this.ItemsLoot_RodsBox.Margin = new System.Windows.Forms.Padding(2, 2, 1, 0);
            this.ItemsLoot_RodsBox.Name = "ItemsLoot_RodsBox";
            this.ItemsLoot_RodsBox.Size = new System.Drawing.Size(58, 19);
            this.ItemsLoot_RodsBox.TabIndex = 9;
            this.ItemsLoot_RodsBox.Text = "Rods";
            this.ItemsLoot_RodsBox.UseVisualStyleBackColor = true;
            this.ItemsLoot_RodsBox.CheckedChanged += new System.EventHandler(this.ItemsLoot_RodsBox_CheckedChanged);
            // 
            // ItemsLoot_ScrollsBox
            // 
            this.ItemsLoot_ScrollsBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ItemsLoot_ScrollsBox.AutoSize = true;
            this.ItemsLoot_ScrollsBox.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemsLoot_ScrollsBox.Location = new System.Drawing.Point(34, 149);
            this.ItemsLoot_ScrollsBox.Margin = new System.Windows.Forms.Padding(2, 2, 1, 0);
            this.ItemsLoot_ScrollsBox.Name = "ItemsLoot_ScrollsBox";
            this.ItemsLoot_ScrollsBox.Size = new System.Drawing.Size(66, 19);
            this.ItemsLoot_ScrollsBox.TabIndex = 10;
            this.ItemsLoot_ScrollsBox.Text = "Scrolls";
            this.ItemsLoot_ScrollsBox.UseVisualStyleBackColor = true;
            this.ItemsLoot_ScrollsBox.CheckedChanged += new System.EventHandler(this.ItemsLoot_ScrollsBox_CheckedChanged);
            // 
            // ItemsLoot_StaffsBox
            // 
            this.ItemsLoot_StaffsBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ItemsLoot_StaffsBox.AutoSize = true;
            this.ItemsLoot_StaffsBox.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemsLoot_StaffsBox.Location = new System.Drawing.Point(34, 170);
            this.ItemsLoot_StaffsBox.Margin = new System.Windows.Forms.Padding(2, 2, 1, 0);
            this.ItemsLoot_StaffsBox.Name = "ItemsLoot_StaffsBox";
            this.ItemsLoot_StaffsBox.Size = new System.Drawing.Size(59, 19);
            this.ItemsLoot_StaffsBox.TabIndex = 11;
            this.ItemsLoot_StaffsBox.Text = "Staffs";
            this.ItemsLoot_StaffsBox.UseVisualStyleBackColor = true;
            this.ItemsLoot_StaffsBox.CheckedChanged += new System.EventHandler(this.ItemsLoot_StaffsBox_CheckedChanged);
            // 
            // ItemsLoot_WandsBox
            // 
            this.ItemsLoot_WandsBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ItemsLoot_WandsBox.AutoSize = true;
            this.ItemsLoot_WandsBox.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemsLoot_WandsBox.Location = new System.Drawing.Point(34, 191);
            this.ItemsLoot_WandsBox.Margin = new System.Windows.Forms.Padding(2, 2, 1, 0);
            this.ItemsLoot_WandsBox.Name = "ItemsLoot_WandsBox";
            this.ItemsLoot_WandsBox.Size = new System.Drawing.Size(69, 19);
            this.ItemsLoot_WandsBox.TabIndex = 12;
            this.ItemsLoot_WandsBox.Text = "Wands";
            this.ItemsLoot_WandsBox.UseVisualStyleBackColor = true;
            this.ItemsLoot_WandsBox.CheckedChanged += new System.EventHandler(this.ItemsLoot_WandsBox_CheckedChanged);
            // 
            // ItemsLoot_WondrousBox
            // 
            this.ItemsLoot_WondrousBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ItemsLoot_WondrousBox.AutoSize = true;
            this.ItemsLoot_WondrousBox.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemsLoot_WondrousBox.Location = new System.Drawing.Point(34, 212);
            this.ItemsLoot_WondrousBox.Margin = new System.Windows.Forms.Padding(2, 2, 1, 0);
            this.ItemsLoot_WondrousBox.Name = "ItemsLoot_WondrousBox";
            this.ItemsLoot_WondrousBox.Size = new System.Drawing.Size(128, 20);
            this.ItemsLoot_WondrousBox.TabIndex = 13;
            this.ItemsLoot_WondrousBox.Text = "Wondrous Items";
            this.ItemsLoot_WondrousBox.UseVisualStyleBackColor = true;
            this.ItemsLoot_WondrousBox.CheckedChanged += new System.EventHandler(this.ItemsLoot_WondrousBox_CheckedChanged);
            // 
            // ItemsLoot_PotionsBox
            // 
            this.ItemsLoot_PotionsBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ItemsLoot_PotionsBox.AutoSize = true;
            this.ItemsLoot_PotionsBox.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemsLoot_PotionsBox.Location = new System.Drawing.Point(34, 86);
            this.ItemsLoot_PotionsBox.Margin = new System.Windows.Forms.Padding(2, 2, 1, 0);
            this.ItemsLoot_PotionsBox.Name = "ItemsLoot_PotionsBox";
            this.ItemsLoot_PotionsBox.Size = new System.Drawing.Size(73, 19);
            this.ItemsLoot_PotionsBox.TabIndex = 7;
            this.ItemsLoot_PotionsBox.Text = "Potions";
            this.ItemsLoot_PotionsBox.UseVisualStyleBackColor = true;
            this.ItemsLoot_PotionsBox.CheckedChanged += new System.EventHandler(this.ItemsLoot_PotionsBox_CheckedChanged);
            // 
            // ItemsLoot_ToolsGearBox
            // 
            this.ItemsLoot_ToolsGearBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ItemsLoot_ToolsGearBox.AutoSize = true;
            this.ItemsLoot_ToolsGearBox.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemsLoot_ToolsGearBox.Location = new System.Drawing.Point(34, 23);
            this.ItemsLoot_ToolsGearBox.Margin = new System.Windows.Forms.Padding(2, 2, 1, 0);
            this.ItemsLoot_ToolsGearBox.Name = "ItemsLoot_ToolsGearBox";
            this.ItemsLoot_ToolsGearBox.Size = new System.Drawing.Size(118, 19);
            this.ItemsLoot_ToolsGearBox.TabIndex = 6;
            this.ItemsLoot_ToolsGearBox.Text = "Tools and Gear";
            this.ItemsLoot_ToolsGearBox.UseVisualStyleBackColor = true;
            this.ItemsLoot_ToolsGearBox.CheckedChanged += new System.EventHandler(this.ItemsLoot_ToolsGearBox_CheckedChanged);
            // 
            // ItemsLoot_WeaponsBox
            // 
            this.ItemsLoot_WeaponsBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ItemsLoot_WeaponsBox.AutoSize = true;
            this.ItemsLoot_WeaponsBox.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemsLoot_WeaponsBox.Location = new System.Drawing.Point(34, 65);
            this.ItemsLoot_WeaponsBox.Margin = new System.Windows.Forms.Padding(2, 2, 1, 0);
            this.ItemsLoot_WeaponsBox.Name = "ItemsLoot_WeaponsBox";
            this.ItemsLoot_WeaponsBox.Size = new System.Drawing.Size(82, 19);
            this.ItemsLoot_WeaponsBox.TabIndex = 5;
            this.ItemsLoot_WeaponsBox.Text = "Weapons";
            this.ItemsLoot_WeaponsBox.UseVisualStyleBackColor = true;
            this.ItemsLoot_WeaponsBox.CheckedChanged += new System.EventHandler(this.ItemsLoot_WeaponsBox_CheckedChanged);
            // 
            // ItemsLoot_ArmourShieldsBox
            // 
            this.ItemsLoot_ArmourShieldsBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ItemsLoot_ArmourShieldsBox.AutoSize = true;
            this.ItemsLoot_ArmourShieldsBox.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemsLoot_ArmourShieldsBox.Location = new System.Drawing.Point(34, 44);
            this.ItemsLoot_ArmourShieldsBox.Margin = new System.Windows.Forms.Padding(2, 2, 1, 0);
            this.ItemsLoot_ArmourShieldsBox.Name = "ItemsLoot_ArmourShieldsBox";
            this.ItemsLoot_ArmourShieldsBox.Size = new System.Drawing.Size(147, 19);
            this.ItemsLoot_ArmourShieldsBox.TabIndex = 4;
            this.ItemsLoot_ArmourShieldsBox.Text = "Armour and Shields";
            this.ItemsLoot_ArmourShieldsBox.UseVisualStyleBackColor = true;
            this.ItemsLoot_ArmourShieldsBox.CheckedChanged += new System.EventHandler(this.ItemsLoot_ArmourShieldsBox_CheckedChanged);
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.SelectAll_Button, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.Deselect_Button, 1, 0);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 544);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(249, 23);
            this.tableLayoutPanel9.TabIndex = 7;
            // 
            // SelectAll_Button
            // 
            this.SelectAll_Button.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SelectAll_Button.Location = new System.Drawing.Point(5, 0);
            this.SelectAll_Button.Margin = new System.Windows.Forms.Padding(0);
            this.SelectAll_Button.Name = "SelectAll_Button";
            this.SelectAll_Button.Size = new System.Drawing.Size(113, 23);
            this.SelectAll_Button.TabIndex = 3;
            this.SelectAll_Button.Text = "Select All";
            this.SelectAll_Button.UseVisualStyleBackColor = true;
            this.SelectAll_Button.Click += new System.EventHandler(this.SelectAll_Button_Click);
            // 
            // Deselect_Button
            // 
            this.Deselect_Button.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Deselect_Button.Location = new System.Drawing.Point(130, 0);
            this.Deselect_Button.Margin = new System.Windows.Forms.Padding(0);
            this.Deselect_Button.Name = "Deselect_Button";
            this.Deselect_Button.Size = new System.Drawing.Size(113, 23);
            this.Deselect_Button.TabIndex = 2;
            this.Deselect_Button.Text = "Deselect All";
            this.Deselect_Button.UseVisualStyleBackColor = true;
            this.Deselect_Button.Click += new System.EventHandler(this.Deselect_Button_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.6129F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.3871F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.Close_Button, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(258, 544);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(473, 23);
            this.tableLayoutPanel3.TabIndex = 4;
            // 
            // Close_Button
            // 
            this.Close_Button.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Close_Button.Location = new System.Drawing.Point(348, 0);
            this.Close_Button.Margin = new System.Windows.Forms.Padding(0);
            this.Close_Button.Name = "Close_Button";
            this.Close_Button.Size = new System.Drawing.Size(125, 23);
            this.Close_Button.TabIndex = 4;
            this.Close_Button.Text = "Close";
            this.Close_Button.UseVisualStyleBackColor = true;
            this.Close_Button.Click += new System.EventHandler(this.Close_Button_Click);
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Controls.Add(this.tableLayoutPanel11, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.DisplayWindow, 0, 0);
            this.tableLayoutPanel10.Location = new System.Drawing.Point(258, 60);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 2;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 91.21339F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.786611F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(506, 478);
            this.tableLayoutPanel10.TabIndex = 8;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 72.4F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.6F));
            this.tableLayoutPanel11.Controls.Add(this.Clear_Button, 1, 0);
            this.tableLayoutPanel11.Controls.Add(this.Generate_Button, 0, 0);
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 438);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(470, 37);
            this.tableLayoutPanel11.TabIndex = 0;
            // 
            // Clear_Button
            // 
            this.Clear_Button.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Clear_Button.Location = new System.Drawing.Point(342, 7);
            this.Clear_Button.Margin = new System.Windows.Forms.Padding(0);
            this.Clear_Button.Name = "Clear_Button";
            this.Clear_Button.Size = new System.Drawing.Size(125, 23);
            this.Clear_Button.TabIndex = 5;
            this.Clear_Button.Text = "Clear";
            this.Clear_Button.UseVisualStyleBackColor = true;
            this.Clear_Button.Click += new System.EventHandler(this.Clear_Button_Click);
            // 
            // Generate_Button
            // 
            this.Generate_Button.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Generate_Button.Location = new System.Drawing.Point(215, 7);
            this.Generate_Button.Margin = new System.Windows.Forms.Padding(0);
            this.Generate_Button.Name = "Generate_Button";
            this.Generate_Button.Size = new System.Drawing.Size(125, 23);
            this.Generate_Button.TabIndex = 3;
            this.Generate_Button.Text = "Generate";
            this.Generate_Button.UseVisualStyleBackColor = true;
            this.Generate_Button.Click += new System.EventHandler(this.Generate_Button_Click);
            // 
            // DisplayWindow
            // 
            this.DisplayWindow.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.DisplayWindow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DisplayWindow.Font = new System.Drawing.Font("Imprint MT Shadow", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DisplayWindow.Location = new System.Drawing.Point(32, 26);
            this.DisplayWindow.MaximumSize = new System.Drawing.Size(1000, 1000);
            this.DisplayWindow.Multiline = true;
            this.DisplayWindow.Name = "DisplayWindow";
            this.DisplayWindow.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DisplayWindow.Size = new System.Drawing.Size(441, 383);
            this.DisplayWindow.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Treasure.UI.Properties.Resources.dungeon_3_light;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(792, 595);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "D&D3.5 Treasure Generator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.CheckBox LevelSelectBox_1;
        private System.Windows.Forms.CheckBox LevelSelectBox_10;
        private System.Windows.Forms.CheckBox LevelSelectBox_9;
        private System.Windows.Forms.CheckBox LevelSelectBox_8;
        private System.Windows.Forms.CheckBox LevelSelectBox_7;
        private System.Windows.Forms.CheckBox LevelSelectBox_6;
        private System.Windows.Forms.CheckBox LevelSelectBox_5;
        private System.Windows.Forms.CheckBox LevelSelectBox_4;
        private System.Windows.Forms.CheckBox LevelSelectBox_3;
        private System.Windows.Forms.CheckBox LevelSelectBox_2;
        private System.Windows.Forms.CheckBox LevelSelectBox_20;
        private System.Windows.Forms.CheckBox LevelSelectBox_19;
        private System.Windows.Forms.CheckBox LevelSelectBox_18;
        private System.Windows.Forms.CheckBox LevelSelectBox_17;
        private System.Windows.Forms.CheckBox LevelSelectBox_16;
        private System.Windows.Forms.CheckBox LevelSelectBox_15;
        private System.Windows.Forms.CheckBox LevelSelectBox_14;
        private System.Windows.Forms.CheckBox LevelSelectBox_13;
        private System.Windows.Forms.CheckBox LevelSelectBox_12;
        private System.Windows.Forms.CheckBox LevelSelectBox_11;
        private System.Windows.Forms.Label LevelBoxLabel_1;
        private System.Windows.Forms.Label LevelBoxLabel_20;
        private System.Windows.Forms.Label LevelBoxLabel_19;
        private System.Windows.Forms.Label LevelBoxLabel_18;
        private System.Windows.Forms.Label LevelBoxLabel_17;
        private System.Windows.Forms.Label LevelBoxLabel_16;
        private System.Windows.Forms.Label LevelBoxLabel_15;
        private System.Windows.Forms.Label LevelBoxLabel_14;
        private System.Windows.Forms.Label LevelBoxLabel_13;
        private System.Windows.Forms.Label LevelBoxLabel_12;
        private System.Windows.Forms.Label LevelBoxLabel_11;
        private System.Windows.Forms.Label LevelBoxLabel_10;
        private System.Windows.Forms.Label LevelBoxLabel_9;
        private System.Windows.Forms.Label LevelBoxLabel_8;
        private System.Windows.Forms.Label LevelBoxLabel_7;
        private System.Windows.Forms.Label LevelBoxLabel_6;
        private System.Windows.Forms.Label LevelBoxLabel_5;
        private System.Windows.Forms.Label LevelBoxLabel_4;
        private System.Windows.Forms.Label LevelBoxLabel_3;
        private System.Windows.Forms.Label LevelBoxLabel_2;
        private System.Windows.Forms.Label LevelTitleLabel;
        private System.Windows.Forms.Button Deselect_Button;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button Generate_Button;
        private System.Windows.Forms.TextBox DisplayWindow;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.CheckBox CoinsLoot_Checkbox;
        private System.Windows.Forms.CheckBox GoodsLoot_Checkbox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.CheckBox GoodsLoot_Gems_Checkbox;
        private System.Windows.Forms.CheckBox GoodsLoot_Art_Checkbox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.CheckBox ItemsLoot_Checkbox;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.CheckBox ItemsLoot_AlchemicalBox;
        private System.Windows.Forms.CheckBox ItemsLoot_ArmourShieldsBox;
        private System.Windows.Forms.CheckBox ItemsLoot_WeaponsBox;
        private System.Windows.Forms.CheckBox ItemsLoot_ToolsGearBox;
        private System.Windows.Forms.CheckBox ItemsLoot_PotionsBox;
        private System.Windows.Forms.CheckBox ItemsLoot_RingsBox;
        private System.Windows.Forms.CheckBox ItemsLoot_RodsBox;
        private System.Windows.Forms.CheckBox ItemsLoot_ScrollsBox;
        private System.Windows.Forms.CheckBox ItemsLoot_StaffsBox;
        private System.Windows.Forms.CheckBox ItemsLoot_WandsBox;
        private System.Windows.Forms.CheckBox ItemsLoot_WondrousBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Button SelectAll_Button;
        private System.Windows.Forms.Button Close_Button;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Button Clear_Button;
    }
}

