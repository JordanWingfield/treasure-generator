﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Treasure.Domain;

namespace Treasure.UI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            this.Hide();
            InitializeComponent();
            LevelSelectBoxes = SetupLevelSelectBoxes();
            ItemsBoxes = ItemSelectBoxes();
            GoodsBoxes = GoodsSelectBoxes();
            TreasureBoxes = TreasureSelectBoxes();
        }

        private List<CheckBox> SetupLevelSelectBoxes()
        {
            var boxes = new List<CheckBox>();
            boxes.Add(LevelSelectBox_1);
            boxes.Add(LevelSelectBox_2);
            boxes.Add(LevelSelectBox_3);
            boxes.Add(LevelSelectBox_4);
            boxes.Add(LevelSelectBox_5);
            boxes.Add(LevelSelectBox_6);
            boxes.Add(LevelSelectBox_7);
            boxes.Add(LevelSelectBox_8);
            boxes.Add(LevelSelectBox_9);
            boxes.Add(LevelSelectBox_10);
            boxes.Add(LevelSelectBox_11);
            boxes.Add(LevelSelectBox_12);
            boxes.Add(LevelSelectBox_13);
            boxes.Add(LevelSelectBox_14);
            boxes.Add(LevelSelectBox_15);
            boxes.Add(LevelSelectBox_16);
            boxes.Add(LevelSelectBox_17);
            boxes.Add(LevelSelectBox_18);
            boxes.Add(LevelSelectBox_19);
            boxes.Add(LevelSelectBox_20);
            return boxes;
        }

        private List<CheckBox> GoodsSelectBoxes()
        {
            var checklist = new List<CheckBox>();
            checklist.Add(GoodsLoot_Art_Checkbox);
            checklist.Add(GoodsLoot_Gems_Checkbox);
            return checklist;
        }

        private List<CheckBox> ItemSelectBoxes()
        {
            var checklist = new List<CheckBox>();
            checklist.Add(ItemsLoot_AlchemicalBox);
            checklist.Add(ItemsLoot_ArmourShieldsBox);
            checklist.Add(ItemsLoot_PotionsBox);
            checklist.Add(ItemsLoot_RingsBox);
            checklist.Add(ItemsLoot_RodsBox);
            checklist.Add(ItemsLoot_ScrollsBox);
            checklist.Add(ItemsLoot_StaffsBox);
            checklist.Add(ItemsLoot_ToolsGearBox);
            checklist.Add(ItemsLoot_WandsBox);
            checklist.Add(ItemsLoot_WeaponsBox);
            checklist.Add(ItemsLoot_WondrousBox);
            return checklist;
        }

        private List<CheckBox> TreasureSelectBoxes()
        {
            var checklist = new List<CheckBox>();
            checklist.Add(CoinsLoot_Checkbox);
            checklist.Add(GoodsLoot_Checkbox);
            checklist.Add(ItemsLoot_Checkbox);
            return checklist;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            this.Show();
        }

        private void DisplayTreasure(List<Item> items)
        {
            foreach (var item in items)
            {
                DisplayWindow.AppendText(item.Name + ", Cost: " + item.Value.Item1.ToString() + " " + item.Value.Item2.ToString() + Environment.NewLine + Environment.NewLine);
            }
        }

        private void ShowError(string error_message)
        {
            System.Windows.Forms.MessageBox.Show(error_message);
        }

        private int GetEncounterLevel()
        {
            int counter = 1;
            foreach(var box in LevelSelectBoxes)
            {
                if (box.Checked == true)
                {
                    return counter;
                }
                else
                {
                    counter += 1;
                }
            }
            return 0;
        }
        
        private List<Item> GenerateAll(int encounter_level)
        {
            var generateor = new Treasure_Generator(encounter_level);
            return generateor.Generate();
        }

        private List<Item> GenerateIndividualCoins(int encounter_level)
        {
            var generator = new Treasure_Generator(encounter_level);
            return generator.GenerateCoins();
        }

        private List<Item> GenerateIndividualGoods(int encounter_level)
        {
            var generator = new Treasure_Generator(encounter_level);
            return generator.GenerateGoods();
        }

        private List<Item> GenerateIndividualItems(int encounter_level)
        {
            var generator = new Treasure_Generator(encounter_level);
            return generator.GenerateItems();
        }

        private List<Item> GenerateIndividualGems()
        {
            var generator = Treasure_Factory.CreateGemList();
            return generator.GenerateItem();
        }

        private List<Item> GenerateIndividualArt()
        {
            if (comboBox1.SelectedItem.ToString() == "Mundane")
            {
                return Treasure_Factory.CreateArtList().GenerateItem();
            }
            else
            {
                ShowError("Please select type of item to generate.");
                return null;
            }
        }

        private List<Item> GenerateIndividualAlchemicalItem()
        {
            if (comboBox1.SelectedItem.ToString() == "Mundane")
            {
                return Treasure_Factory.CreateAlchemicItemsList().GenerateItem();
            }
            else
            {
                ShowError("Please select type of item to generate.");
                return null;
            }
        }

        private List<Item> GenerateIndividualToolItem()
        {
            var generator = Treasure_Factory.CreateToolItemsList();
            return generator.GenerateItem();
        }

        private List<Item> GenerateIndividualArmourShieldItem()
        {
            switch (comboBox1.SelectedItem.ToString())
            {
                case "Mundane":
                    return Treasure_Factory.CreateArmourItemsList().GenerateItem();     
                case "Minor":
                    return Treasure_Factory.CreateMinorArmourShieldList().GenerateItem();
                case "Medium":
                    return Treasure_Factory.CreateMediumArmourShieldList().GenerateItem();
                case "Major":
                    return Treasure_Factory.CreateMajorArmourShieldList().GenerateItem();
                default:
                    ShowError("Please select type of item to generate.");
                    break;
            }
            return null;
        }

        private List<Item> GenerateIndividualWeaponItem()
        {
            switch (comboBox1.SelectedItem.ToString())
            {
                case "Mundane":
                    return Treasure_Factory.CreateWeaponItemsList().GenerateItem();
                case "Minor":
                    return Treasure_Factory.CreateMinorWeaponsList().GenerateItem();
                case "Medium":
                    return Treasure_Factory.CreateMediumWeaponsList().GenerateItem();
                case "Major":
                    return Treasure_Factory.CreateMajorWeaponsList().GenerateItem();
                default:
                    ShowError("Please select type of item to generate.");
                    break;
            }
            return null;
        }

        private List<Item> GenerateIndividualPotionsItem()
        {
            switch (comboBox1.SelectedItem.ToString())
            {
                case "Minor":
                    return Treasure_Factory.CreateMinorPotionsList().GenerateItem();
                case "Medium":
                    return Treasure_Factory.CreateMediumPotionsList().GenerateItem();
                case "Major":
                    return Treasure_Factory.CreateMajorPotionsList().GenerateItem();
                default:
                    ShowError("Please select type of item to generate.");
                    break;
            }
            return null;
        }

        private List<Item> GenerateIndividualRingItem()
        {
            switch (comboBox1.SelectedItem.ToString())
            {
                case "Minor":
                    return Treasure_Factory.CreateMinorRingsList().GenerateItem();
                case "Medium":
                    return Treasure_Factory.CreateMediumRingsList().GenerateItem();
                case "Major":
                    return Treasure_Factory.CreateMajorRingsList().GenerateItem();
                default:
                    ShowError("Please select type of item to generate.");
                    break;
            }
            return null;
        }

        private List<Item> GenerateIndividualRodItem()
        {
            switch (comboBox1.SelectedItem.ToString())
            {
                case "Medium":
                    return Treasure_Factory.CreateMediumRodsList().GenerateItem();
                case "Major":
                    return Treasure_Factory.CreateMajorRodsList().GenerateItem();
                default:
                    ShowError("Please select type of item to generate.");
                    break;
            }
            return null;
        }

        private List<Item> GenerateIndividualScrollItem()
        {
            switch (comboBox1.SelectedItem.ToString())
            {
                case "Minor":
                    return Treasure_Factory.CreateMinorScrollList().GenerateItem();
                case "Medium":
                    return Treasure_Factory.CreateMediumScrollList().GenerateItem();
                case "Major":
                    return Treasure_Factory.CreateMajorScrollList().GenerateItem();
                default:
                    ShowError("Please select type of item to generate.");
                    break;
            }
            return null;
        }

        private List<Item> GenerateIndividualStaffItem()
        {
            switch (comboBox1.SelectedItem.ToString())
            {
                case "Medium":
                    return Treasure_Factory.CreateMediumStaffsList().GenerateItem();
                case "Major":
                    return Treasure_Factory.CreateMajorStaffsList().GenerateItem();
                default:
                    ShowError("Please select type of item to generate.");
                    break;
            }
            return null;
        }

        private List<Item> GenerateIndividualWandItem()
        {
            switch (comboBox1.SelectedItem.ToString())
            {
                case "Minor":
                    return Treasure_Factory.CreateMinorWandsList().GenerateItem();
                case "Medium":
                    return Treasure_Factory.CreateMediumWandsList().GenerateItem();
                case "Major":
                    return Treasure_Factory.CreateMajorWandsList().GenerateItem();
                default:
                    ShowError("Please select type of item to generate.");
                    break;
            }
            return null;
        }

        private List<Item> GenerateIndividualWondrousItem()
        {
            switch (comboBox1.SelectedItem.ToString())
            {
                case "Minor":
                    return Treasure_Factory.CreateMinorWondrousList().GenerateItem();
                case "Medium":
                    return Treasure_Factory.CreateMediumWondrousList().GenerateItem();
                case "Major":
                    return Treasure_Factory.CreateMajorWondrousList().GenerateItem();
                default:
                    ShowError("Please select type of item to generate.");
                    break;
            }
            return null;
        }

        /* Encounter Level Checkbox methods:
         */
        private void UpdateLevelSelectBoxes(CheckBox sender, List<CheckBox> allboxes)
        {
            if (sender.Checked == true)
            {
                foreach (var box in allboxes)
                {
                    if (box != sender)
                    {
                        box.Checked = false;
                    }
                }
            }
        }
        
        private void LevelSelectBox_1_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_1, LevelSelectBoxes);
        }

        private void LevelSelectBox_2_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_2, LevelSelectBoxes);
        }

        private void LevelSelectBox_3_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_3, LevelSelectBoxes);
        }

        private void LevelSelectBox_4_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_4, LevelSelectBoxes);
        }

        private void LevelSelectBox_5_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_5, LevelSelectBoxes);
        }

        private void LevelSelectBox_6_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_6, LevelSelectBoxes);
        }

        private void LevelSelectBox_7_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_7, LevelSelectBoxes);
        }

        private void LevelSelectBox_8_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_8, LevelSelectBoxes);
        }

        private void LevelSelectBox_9_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_9, LevelSelectBoxes);
        }

        private void LevelSelectBox_10_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_10, LevelSelectBoxes);
        }

        private void LevelSelectBox_11_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_11, LevelSelectBoxes);
        }

        private void LevelSelectBox_12_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_12, LevelSelectBoxes);
        }

        private void LevelSelectBox_13_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_13, LevelSelectBoxes);
        }

        private void LevelSelectBox_14_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_14, LevelSelectBoxes);
        }

        private void LevelSelectBox_15_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_15, LevelSelectBoxes);
        }

        private void LevelSelectBox_16_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_16, LevelSelectBoxes);
        }

        private void LevelSelectBox_17_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_17, LevelSelectBoxes);
        }

        private void LevelSelectBox_18_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_18, LevelSelectBoxes);
        }

        private void LevelSelectBox_19_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_19, LevelSelectBoxes);
        }

        private void LevelSelectBox_20_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevelSelectBoxes(LevelSelectBox_20, LevelSelectBoxes);
        }


        /* Option checkbox methods:
         */ 
        

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedItem.ToString())
            {
                case "None":
                    foreach (var box in ItemsBoxes)
                    {
                        box.Enabled = true;
                        box.Checked = false;
                    }
                    break;
                case "Mundane":
                    foreach (var box in ItemsBoxes)
                    {
                        box.Enabled = true;
                    }
                    ItemsLoot_Checkbox.Checked = false;
                    ItemsLoot_PotionsBox.Checked = false;
                    ItemsLoot_RingsBox.Checked = false;
                    ItemsLoot_RodsBox.Checked = false;
                    ItemsLoot_ScrollsBox.Checked = false;
                    ItemsLoot_StaffsBox.Checked = false;
                    ItemsLoot_WandsBox.Checked = false;
                    ItemsLoot_WondrousBox.Checked = false;

                    ItemsLoot_PotionsBox.Enabled = false;
                    ItemsLoot_RingsBox.Enabled = false;
                    ItemsLoot_RodsBox.Enabled = false;
                    ItemsLoot_ScrollsBox.Enabled = false;
                    ItemsLoot_StaffsBox.Enabled = false;
                    ItemsLoot_WandsBox.Enabled = false;
                    ItemsLoot_WondrousBox.Enabled = false;
                    break;
                case "Minor":
                    foreach (var box in ItemsBoxes)
                    {
                        box.Enabled = true;
                    }
                    ItemsLoot_Checkbox.Checked = false;
                    ItemsLoot_AlchemicalBox.Checked = false;
                    ItemsLoot_ToolsGearBox.Checked = false;
                    ItemsLoot_RodsBox.Checked = false;
                    ItemsLoot_StaffsBox.Checked = false;

                    ItemsLoot_AlchemicalBox.Enabled = false;
                    ItemsLoot_ToolsGearBox.Enabled = false;
                    ItemsLoot_RodsBox.Enabled = false;
                    ItemsLoot_StaffsBox.Enabled = false;
                    break;
                case "Medium":
                    foreach (var box in ItemsBoxes)
                    {
                        box.Enabled = true;
                    }
                    ItemsLoot_Checkbox.Checked = false;
                    ItemsLoot_AlchemicalBox.Checked = false;
                    ItemsLoot_ToolsGearBox.Checked = false;

                    ItemsLoot_AlchemicalBox.Enabled = false;
                    ItemsLoot_ToolsGearBox.Enabled = false;
                    break;
                case "Major":
                    foreach (var box in ItemsBoxes)
                    {
                        box.Enabled = true;
                    }
                    ItemsLoot_Checkbox.Checked = false;
                    ItemsLoot_AlchemicalBox.Checked = false;
                    ItemsLoot_ToolsGearBox.Checked = false;

                    ItemsLoot_AlchemicalBox.Enabled = false;
                    ItemsLoot_ToolsGearBox.Enabled = false;
                    break;
                default:
                    Console.Write("No match found in dropdown menu. Text selected: " + comboBox1.SelectedItem.ToString());
                    break;
            }
        }

        private void ItemsLoot_AlchemicalBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ItemsLoot_AlchemicalBox.Checked == true)
            {
                ItemsLoot_Checkbox.Checked = false;
            }
        }

        private void ItemsLoot_ToolsGearBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ItemsLoot_ToolsGearBox.Checked == true)
            {
                ItemsLoot_Checkbox.Checked = false;
            }
        }

        private void ItemsLoot_ArmourShieldsBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ItemsLoot_ArmourShieldsBox.Checked == true)
            {
                ItemsLoot_Checkbox.Checked = false;
            }
        }

        private void ItemsLoot_WeaponsBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ItemsLoot_WeaponsBox.Checked == true)
            {
                ItemsLoot_Checkbox.Checked = false;
            }
        }

        private void ItemsLoot_PotionsBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ItemsLoot_PotionsBox.Checked == true)
            {
                ItemsLoot_Checkbox.Checked = false;
            }
        }

        private void ItemsLoot_RingsBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ItemsLoot_RingsBox.Checked == true)
            {
                ItemsLoot_Checkbox.Checked = false;
            }
        }

        private void ItemsLoot_RodsBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ItemsLoot_RodsBox.Checked == true)
            {
                ItemsLoot_Checkbox.Checked = false;
            }
        }

        private void ItemsLoot_ScrollsBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ItemsLoot_ScrollsBox.Checked == true)
            {
                ItemsLoot_Checkbox.Checked = false;
            }
        }

        private void ItemsLoot_StaffsBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ItemsLoot_StaffsBox.Checked == true)
            {
                ItemsLoot_Checkbox.Checked = false;
            }
        }

        private void ItemsLoot_WandsBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ItemsLoot_WandsBox.Checked == true)
            {
                ItemsLoot_Checkbox.Checked = false;
            }
        }

        private void ItemsLoot_WondrousBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ItemsLoot_WondrousBox.Checked == true)
            {
                ItemsLoot_Checkbox.Checked = false;
            }
        }

        private void ItemsLoot_Checkbox_CheckedChanged(object sender, EventArgs e)
        {
            
            if (ItemsLoot_Checkbox.Checked == true)
            {
                comboBox1.SelectedIndex = 0;
                foreach (var box in ItemsBoxes)
                {
                    box.Checked = false;
                }
            }
        }

        private void GoodsLoot_Art_Checkbox_CheckedChanged(object sender, EventArgs e)
        {
            if (GoodsLoot_Art_Checkbox.Checked == true)
            {
                GoodsLoot_Checkbox.Checked = false;
            }
        }

        private void GoodsLoot_Gems_Checkbox_CheckedChanged(object sender, EventArgs e)
        {
            if (GoodsLoot_Gems_Checkbox.Checked == true)
            {
                GoodsLoot_Checkbox.Checked = false;
            }
        }

        private void GoodsLoot_Checkbox_CheckedChanged(object sender, EventArgs e)
        {
            if (GoodsLoot_Checkbox.Checked == true)
            {
                foreach (var box in GoodsBoxes)
                {
                    box.Checked = false;
                }
            }
        }

        /* Button Methods:
         */ 

        private void SelectAll_Button_Click(object sender, EventArgs e)
        {
            GoodsLoot_Checkbox.Checked = true;
            CoinsLoot_Checkbox.Checked = true;
            ItemsLoot_Checkbox.Checked = true;
        }

        private void Deselect_Button_Click(object sender, EventArgs e)
        {
            GoodsLoot_Checkbox.Checked = false;
            CoinsLoot_Checkbox.Checked = false;
            ItemsLoot_Checkbox.Checked = false;
            foreach (var box in ItemsBoxes)
            {
                box.Checked = false;
            }
            foreach (var box in GoodsBoxes)
            {
                box.Checked = false;
            }
        }

        private void Close_Button_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Clear_Button_Click(object sender, EventArgs e)
        {
            DisplayWindow.ResetText();
        }

        private void Generate_Button_Click(object sender, EventArgs e)
        {
            DisplayWindow.ResetText();
            var treasure_bag = new List<Item>();

            if (GoodsLoot_Checkbox.Checked == true &&
                CoinsLoot_Checkbox.Checked == true &&
                ItemsLoot_Checkbox.Checked == true)
            {
                treasure_bag.AddRange(GenerateAll(GetEncounterLevel()));
            }
            else
            {
                var items = new List<Item>();
                if (CoinsLoot_Checkbox.Checked == true)
                {
                    items = GenerateIndividualCoins(GetEncounterLevel());
                    if (items == null)
                    {
                        return;
                    }
                    else
                    {

                        treasure_bag.AddRange(items);
                    }
                }

                if (GoodsLoot_Checkbox.Checked == true)
                {
                    items = GenerateIndividualGoods(GetEncounterLevel());
                    if (items == null)
                    {
                        return;
                    }
                    else
                    {

                        treasure_bag.AddRange(items);
                    }
                }

                if (ItemsLoot_Checkbox.Checked == true)
                {
                    items = GenerateIndividualItems(GetEncounterLevel());
                    if (items == null)
                    {
                        return;
                    }
                    else
                    {

                        treasure_bag.AddRange(items);
                    }
                }

                if (GoodsLoot_Gems_Checkbox.Checked == true)
                {
                    items = GenerateIndividualGems();
                    if (items == null)
                    {
                        return;
                    }
                    else
                    {

                        treasure_bag.AddRange(items);
                    }
                }

                if (GoodsLoot_Art_Checkbox.Checked == true)
                {
                    items = GenerateIndividualArt();
                    if (items == null)
                    {
                        return;
                    }
                    else
                    {

                        treasure_bag.AddRange(items);
                    }
                }

                if (ItemsLoot_AlchemicalBox.Checked == true)
                {
                    items = GenerateIndividualAlchemicalItem();
                    if (items == null)
                    {
                        return;
                    }
                    else
                    {

                        treasure_bag.AddRange(items);
                    }
                }

                if (ItemsLoot_ToolsGearBox.Checked == true)
                {
                    items = GenerateIndividualToolItem();
                    if (items == null)
                    {
                        return;
                    }
                    else
                    {

                        treasure_bag.AddRange(items);
                    }
                }

                if (ItemsLoot_ArmourShieldsBox.Checked == true)
                {
                    items = GenerateIndividualArmourShieldItem();
                    if (items == null)
                    {
                        return;
                    }
                    else
                    {

                        treasure_bag.AddRange(items);
                    }
                }

                if (ItemsLoot_WeaponsBox.Checked == true)
                {
                    items = GenerateIndividualWeaponItem();
                    if (items == null)
                    {
                        return;
                    }
                    else
                    {

                        treasure_bag.AddRange(items);
                    }
                }

                if (ItemsLoot_PotionsBox.Checked == true)
                {
                    items = GenerateIndividualPotionsItem();
                    if (items == null)
                    {
                        return;
                    }
                    else
                    {

                        treasure_bag.AddRange(items);
                    }
                }

                if (ItemsLoot_RingsBox.Checked == true)
                {
                    items = GenerateIndividualRingItem();
                    if (items == null)
                    {
                        return;
                    }
                    else
                    {

                        treasure_bag.AddRange(items);
                    }
                }

                if (ItemsLoot_RodsBox.Checked == true)
                {
                    items = GenerateIndividualRodItem();
                    if (items == null)
                    {
                        return;
                    }
                    else
                    {

                        treasure_bag.AddRange(items);
                    }
                }

                if (ItemsLoot_ScrollsBox.Checked == true)
                {
                    items = GenerateIndividualScrollItem();
                    if (items == null)
                    {
                        return;
                    }
                    else
                    {

                        treasure_bag.AddRange(items);
                    }
                }

                if (ItemsLoot_StaffsBox.Checked == true)
                {
                    items = GenerateIndividualStaffItem();
                    if (items == null)
                    {
                        return;
                    }
                    else
                    {

                        treasure_bag.AddRange(items);
                    }
                }

                if (ItemsLoot_WandsBox.Checked == true)
                {
                    items = GenerateIndividualWandItem();
                    if (items == null)
                    {
                        return;
                    }
                    else
                    {

                        treasure_bag.AddRange(items);
                    }
                }

                if (ItemsLoot_WondrousBox.Checked == true)
                {
                    items = GenerateIndividualWondrousItem();
                    if (items == null)
                    {
                        return;
                    }
                    else
                    {

                        treasure_bag.AddRange(items);
                    }
                }
            }

            DisplayTreasure(treasure_bag);
        }

        /* Data members:
         */

        private List<CheckBox> LevelSelectBoxes
        { get; set; }

        private List<CheckBox> TreasureBoxes
        { get; set; }

        private List<CheckBox> ItemsBoxes
        { get; set; }

        private List<CheckBox> GoodsBoxes
        { get; set; }

        
    }

}
