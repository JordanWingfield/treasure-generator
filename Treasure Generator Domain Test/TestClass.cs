﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Treasure.Domain;

namespace Treasure.Domain.UI
{
    [TestFixture]
    public class TestClass
    {
        [Test]
        public void TestCreateItem()
        {
            // Create Item
            var item = new CommonItem(10, "gp", "Amathyst");
            Assert.IsNotNull(item);
            Assert.AreEqual(item.Name, "Amathyst");
            Assert.AreEqual(item.Value, Tuple.Create<int, string>(10, "gp"));
        }

        [Test]
        public void TestCreateItemTypes()
        {
            var items = new List<Item>();
            var enchantments = new List<EnchantmentType>();
            enchantments.Add(new EnchantmentType(0, 50, new Enchantment("+1", 1000, "gp")));
            enchantments.Add(new EnchantmentType(51, 70, new Enchantment("+2", 4000, "gp")));
            enchantments.Add(new EnchantmentType(71, 100, new Enchantment("+3", 9000, "gp")));
            items.Add(new CommonItem(0, "gp", "item1"));
            items.Add(new CommonItem(3, "gp", "item2"));
            items.Add(new MagicItem(5, "gp", "item3", enchantments));
            items.Add(new MagicItem(100, "gp", "item4", enchantments));
            items.Add(new CommonItem(98, "gp", "item5"));
            Assert.IsNotNull(items);
            foreach(var item in items)
            {
                Assert.IsNotNull(item);
                Assert.IsTrue(item.GetType() == typeof(CommonItem) || item.GetType() == typeof(MagicItem));
            }
        }

        [Test]
        public void TestCreateMagicItem()
        {
            var enchantments = new List<EnchantmentType>();
            enchantments.Add(new EnchantmentType(0, 100, new Enchantment("+1", 1000, "gp")));
            var item = new MagicItem(0, "sp", "Blade", enchantments);
            var generated_item = item.GenerateItem();
            Assert.IsNotNull(generated_item);
            Assert.AreEqual(generated_item.Name, "Blade +1");
        }

        [Test]
        public void TestConvertItemToMagicItem()
        {
            var item_list = Treasure_Factory.CreateCommonMeleeWeaponItemsList();
            var enchantment_list = Treasure_Factory.CreateMinorMeleeWeaponEnchantmentsList();
            item_list.GenerateItem();
            Assert.IsNotNull(item_list);
            Assert.IsNotNull(enchantment_list);
            var magic_items = new List<MagicItem>();
            foreach (var item in item_list.GeneratedItems)
            {
                magic_items.Add(new MagicItem(item, enchantment_list));
            }
            foreach (var magic_item in magic_items)
            {
                Assert.AreEqual(magic_item.GetType(), typeof(MagicItem));
            }
        }

        [Test]
        public void TestCopyItemType()
        {
            var common_item = new CommonItem(10, "gp", "Common");
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 100, new Enchantment("+1", 100, "gp")));

            var original_item = new ItemType("Original", 0, 100, common_item);
            var copy_item = original_item.CopyItem(new MagicItem(common_item, enchantment_list));

            Assert.IsNotNull(original_item);
            Assert.IsNotNull(copy_item);
            Assert.AreEqual(original_item.LowerProbability, copy_item.LowerProbability);
            Assert.AreEqual(original_item.Name, copy_item.Name);
            Assert.AreEqual(original_item.UpperProbability, copy_item.UpperProbability);
            Assert.AreEqual(original_item.Item.Name, copy_item.Item.Name);
            Assert.AreEqual(original_item.Item.Value, copy_item.Item.Value);
        }

        [Test]
        public void TestGenerateItem()
        {
            var items = new List<Item>();
            var enchantments = new List<EnchantmentType>();
            enchantments.Add(new EnchantmentType(0, 50, new Enchantment("+1", 1000, "gp")));
            enchantments.Add(new EnchantmentType(51, 70, new Enchantment("+2", 1000, "gp")));
            enchantments.Add(new EnchantmentType(71, 100, new Enchantment("+3", 9000, "gp")));
            items.Add(new CommonItem(0, "gp", "item1"));
            items.Add(new CommonItem(3, "gp", "item2"));
            items.Add(new MagicItem(5, "gp", "item3", enchantments));
            items.Add(new MagicItem(100, "gp", "item4", enchantments));
            items.Add(new CommonItem(98, "gp", "item5"));
            Assert.IsNotNull(items);
            foreach (var item in items)
            {
                Assert.IsNotNull(item);
                Assert.IsTrue(item.GenerateItem().GetType() == typeof(CommonItem) || item.GetType() == typeof(MagicItem));
            }
        }

        [Test]
        public void TestCreateTreasure()
        {
            // Create Treasure
            var treasure = new Treasure();
            Assert.IsNotNull(treasure);
        }

        [Test]
        public void TestCreateTreasureComposite()
        {
            // Create Composite Treasure
            var treasure = new Treasure_Composite();
            Assert.IsNotNull(treasure);
        }

        [Test]
        public void TestCreateTreasureLeaf()
        {
            // Create Leaf Node Treasure
            var treasure = new Treasure_Leaf();
            Assert.IsNotNull(treasure);
        }

        [Test]
        public void TestTreasureAddItem()
        {
            // Adds item to treasure object successfully
            var treasure = new Treasure_Leaf();
            var item = new CommonItem(100, "sp", "Expensive Beer");
            treasure.AddTreasure(new ItemType("Misc", 0, 100, item));
            Assert.IsNotNull(treasure.GenerateItem());
            foreach (var generated_treasure in treasure.GeneratedItems)
            {
                Assert.AreEqual(generated_treasure, item);
            }
        }

        [Test]
        public void TestTreasureAddTreasure()
        {
            // Adds treasure to treasure object successfully
            var treasure = new Treasure_Composite();
            var treasure_comp = new Treasure_Composite();
            var treasure_leaf = new Treasure_Leaf();
            var item = new CommonItem(100, "cp", "Less Expensive Beer");
            treasure_leaf.AddTreasure(new ItemType("Misc", 0, 100, item));
            treasure_comp.AddTreasure(new TreasureType("Mundane", 0, 100, treasure_leaf));
            treasure.AddTreasure(new TreasureType("Mundane", 0, 100, treasure_comp));
            Assert.IsNotNull(treasure.GenerateItem());
            foreach (var generated_treasure in treasure.GenerateItem())
            {
                Assert.AreEqual(generated_treasure, item);
            }
        }

        [Test]
        public void TestFactoryCreateCoinItem()
        {
            var coin_item = Treasure_Factory.CreateCoinItem(1, 6, 100, "gp");
            Assert.IsNotNull(coin_item);
            Assert.IsTrue(coin_item.GetType() == typeof(CommonItem));
            Assert.LessOrEqual(coin_item.Value.Item1, 600);
            Assert.AreEqual(coin_item.Value.Item2, "gp");

        }

        [Test]
        public void TestCreateCoins()
        {
            var coins_list = Treasure_Factory.CreateCoinsList();
            Assert.IsNotNull(coins_list);
            Assert.IsTrue(coins_list.GetType() == typeof(Treasure_Leaf));
            foreach (var generated_treasure in coins_list.GenerateItem())
            {
                Assert.IsTrue(generated_treasure.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateGems()
        {
            var gem_list = Treasure_Factory.CreateGemList();
            Assert.IsNotNull(gem_list);
            Assert.IsTrue(gem_list.GetType() == typeof(Treasure_Leaf));
            foreach (var generated_treasure in gem_list.GenerateItem())
            {
                Assert.IsTrue(generated_treasure.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateArt()
        {
            var art_list = Treasure_Factory.CreateArtList();
            Assert.IsNotNull(art_list);
        }

        [Test]
        public void TestCreateGoods()
        {
            var goods_list = Treasure_Factory.CreateGoodsList();
            Assert.IsNotNull(goods_list);
            Assert.IsTrue(goods_list.GetType() == typeof(Treasure_Composite));
            foreach (var generated_treasure in goods_list.GenerateItem())
            {
                Assert.IsTrue(generated_treasure.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateToolItems()
        {
            var items_list = Treasure_Factory.CreateToolItemsList();
            Assert.IsNotNull(items_list);
            Assert.IsTrue(items_list.GetType() == typeof(Treasure_Leaf));
            foreach (var generated_treasure in items_list.GenerateItem())
            {
                Assert.IsTrue(generated_treasure.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateCommonMeleeWeaponItems()
        {
            var items_list = Treasure_Factory.CreateCommonMeleeWeaponItemsList();
            Assert.IsNotNull(items_list);
            Assert.IsTrue(items_list.GetType() == typeof(Treasure_Leaf));
            foreach (var generated_treasure in items_list.GenerateItem())
            {
                Assert.IsTrue(generated_treasure.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateUncommonMeleeWeaponItems()
        {
            var items_list = Treasure_Factory.CreateUncommonMeleeWeaponItemsList();
            Assert.IsNotNull(items_list);
            Assert.IsTrue(items_list.GetType() == typeof(Treasure_Leaf));
            foreach (var generated_treasure in items_list.GenerateItem())
            {
                Assert.IsTrue(generated_treasure.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateRangedWeaponItems()
        {
            var items_list = Treasure_Factory.CreateRangedWeaponItemsList();
            Assert.IsNotNull(items_list);
            Assert.IsTrue(items_list.GetType() == typeof(Treasure_Leaf));
            foreach (var generated_treasure in items_list.GenerateItem())
            {
                Assert.IsTrue(generated_treasure.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateWeaponItems()
        {
            var items_list = Treasure_Factory.CreateWeaponItemsList();
            Assert.IsNotNull(items_list);
            Assert.IsTrue(items_list.GetType() == typeof(Treasure_Composite));
            foreach (var generated_treasure in items_list.GenerateItem())
            {
                Assert.IsTrue(generated_treasure.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateArmourItems()
        {
            var items_list = Treasure_Factory.CreateArmourItemsList();
            Assert.IsNotNull(items_list);
            Assert.IsTrue(items_list.GetType() == typeof(Treasure_Leaf));
            foreach (var generated_treasure in items_list.GenerateItem())
            {
                Assert.IsTrue(generated_treasure.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateAlechemicItems()
        {
            var items_list = Treasure_Factory.CreateAlchemicItemsList();
            Assert.IsNotNull(items_list);
            Assert.IsTrue(items_list.GetType() == typeof(Treasure_Leaf));
            foreach (var generated_treasure in items_list.GenerateItem())
            {
                Assert.IsTrue(generated_treasure.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMundaneItems()
        {
            var items_list = Treasure_Factory.CreateMundaneItemsList();
            Assert.IsNotNull(items_list);
            Assert.IsTrue(items_list.GetType() == typeof(Treasure_Composite));
            foreach (var generated_treasure in items_list.GenerateItem())
            {
                Assert.IsTrue(generated_treasure.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateItems()
        {
            var items_list = Treasure_Factory.CreateItemsList();
            Assert.IsNotNull(items_list);
            Assert.IsTrue(items_list.GetType() == typeof(Treasure_Composite));
            foreach (var generated_treasure in items_list.GenerateItem())
            {
                Assert.IsTrue(generated_treasure.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMultipleItems()
        {
            var treasure_composite = new Treasure_Composite();
            var treasure_leaf = Treasure_Factory.CreateArtList();
            treasure_composite.AddTreasure(new TreasureType("Art", 0, 100, treasure_leaf, 2, 6));
            Assert.IsNotNull(treasure_composite);
            Assert.IsTrue(treasure_composite.GenerateItem().Count >= 2);
        }

        /* Enchantment List Test Creation Methods
         * Test: Minor/Medium/Major enchantments on melee, ranged, armour, and shields.
         */ 

        [Test]
        public void TestCreateMinorMeleeWeaponEnchantments()
        {
            var enchantment_list = Treasure_Factory.CreateMinorMeleeWeaponEnchantmentsList();
            Assert.IsNotNull(enchantment_list);
            Assert.IsTrue(enchantment_list.Count >= 2);
            foreach(var enchantment in enchantment_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateMinorRangedMeleeWeaponEnchantments()
        {
            var enchantment_list = Treasure_Factory.CreateMinorRangedWeaponEnchantmentsList();
            Assert.IsNotNull(enchantment_list);
            Assert.IsTrue(enchantment_list.Count >= 2);
            foreach (var enchantment in enchantment_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateMinorArmourEnchantments()
        {
            var enchantment_list = Treasure_Factory.CreateMinorArmourEnchantmentsList();
            Assert.IsNotNull(enchantment_list);
            Assert.IsTrue(enchantment_list.Count >= 2);
            foreach (var enchantment in enchantment_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateMinorShieldEnchantments()
        {
            var enchantment_list = Treasure_Factory.CreateMinorShieldEnchantmentsList();
            Assert.IsNotNull(enchantment_list);
            Assert.IsTrue(enchantment_list.Count >= 2);
            foreach (var enchantment in enchantment_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateMediumMeleeWeaponEnchantments()
        {
            var enchantment_list = Treasure_Factory.CreateMediumMeleeWeaponEnchantmentsList();
            Assert.IsNotNull(enchantment_list);
            Assert.IsTrue(enchantment_list.Count >= 2);
            foreach (var enchantment in enchantment_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateMediumRangedMeleeWeaponEnchantments()
        {
            var enchantment_list = Treasure_Factory.CreateMediumRangedWeaponEnchantmentsList();
            Assert.IsNotNull(enchantment_list);
            Assert.IsTrue(enchantment_list.Count >= 2);
            foreach (var enchantment in enchantment_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateMediumArmourEnchantments()
        {
            var enchantment_list = Treasure_Factory.CreateMediumArmourEnchantmentsList();
            Assert.IsNotNull(enchantment_list);
            Assert.IsTrue(enchantment_list.Count >= 2);
            foreach (var enchantment in enchantment_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateMediumShieldEnchantments()
        {
            var enchantment_list = Treasure_Factory.CreateMediumShieldEnchantmentsList();
            Assert.IsNotNull(enchantment_list);
            Assert.IsTrue(enchantment_list.Count >= 2);
            foreach (var enchantment in enchantment_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateMajorMeleeWeaponEnchantments()
        {
            var enchantment_list = Treasure_Factory.CreateMajorMeleeWeaponEnchantmentsList();
            Assert.IsNotNull(enchantment_list);
            Assert.IsTrue(enchantment_list.Count >= 2);
            foreach (var enchantment in enchantment_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateMajorRangedMeleeWeaponEnchantments()
        {
            var enchantment_list = Treasure_Factory.CreateMajorRangedWeaponEnchantmentsList();
            Assert.IsNotNull(enchantment_list);
            Assert.IsTrue(enchantment_list.Count >= 2);
            foreach (var enchantment in enchantment_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateMajorArmourEnchantments()
        {
            var enchantment_list = Treasure_Factory.CreateMajorArmourEnchantmentsList();
            Assert.IsNotNull(enchantment_list);
            Assert.IsTrue(enchantment_list.Count >= 2);
            foreach (var enchantment in enchantment_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateMajorShieldEnchantments()
        {
            var enchantment_list = Treasure_Factory.CreateMajorShieldEnchantmentsList();
            Assert.IsNotNull(enchantment_list);
            Assert.IsTrue(enchantment_list.Count >= 2);
            foreach (var enchantment in enchantment_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        /* Minor MagicItem Test Creation Methods
         * Test Create: Weapons, Armour/Shields, Potions, Rings, Scrolls, Wands,
         * and Wondrous items.
         */

        [Test]
        public void TestCreateMinorWeaponsList()
        {
            var treasure_pile = Treasure_Factory.CreateMinorWeaponsList();
            Assert.IsNotNull(treasure_pile);
            treasure_pile.GenerateItem();
            foreach (var generated_item in treasure_pile.GeneratedItems)
            {
                Assert.IsTrue(generated_item.GetType() == typeof(MagicItem) || generated_item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMinorArmourShieldList()
        {
            var treasure_pile = Treasure_Factory.CreateMinorArmourShieldList();
            Assert.IsNotNull(treasure_pile);
            treasure_pile.GenerateItem();
            foreach (var generated_item in treasure_pile.GeneratedItems)
            {
                Assert.IsTrue(generated_item.GetType() == typeof(MagicItem) || generated_item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMinorRingsList()
        {
            var treasure_pile = Treasure_Factory.CreateMinorRingsList();
            Assert.IsNotNull(treasure_pile);
            treasure_pile.GenerateItem();
            foreach (var generated_item in treasure_pile.GeneratedItems)
            {
                Assert.IsTrue(generated_item.GetType() == typeof(MagicItem) || generated_item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMinorWandsList()
        {
            var treasure_pile = Treasure_Factory.CreateMinorWandsList();
            Assert.IsNotNull(treasure_pile);
            treasure_pile.GenerateItem();
            foreach (var generated_item in treasure_pile.GeneratedItems)
            {
                Assert.IsTrue(generated_item.GetType() == typeof(MagicItem) || generated_item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMinorWondrousList()
        {
            var treasure_pile = Treasure_Factory.CreateMinorWondrousList();
            Assert.IsNotNull(treasure_pile);
            treasure_pile.GenerateItem();
            foreach (var generated_item in treasure_pile.GeneratedItems)
            {
                Assert.IsTrue(generated_item.GetType() == typeof(MagicItem) || generated_item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMinorPotionList()
        {
            var potion_list = Treasure_Factory.CreateMinorPotionsList();
            Assert.IsNotNull(potion_list);
            potion_list.GenerateItem();
            foreach (var item in potion_list.GeneratedItems)
            {
                Assert.IsTrue(item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMinorScrollList()
        {
            var scroll_list = Treasure_Factory.CreateMinorScrollList();
            Assert.IsNotNull(scroll_list);
            scroll_list.GenerateItem();
            foreach (var item in scroll_list.GeneratedItems)
            {
                Assert.AreEqual(item.GetType(), typeof(MagicItem));
            }
        }

        [Test]
        public void TestCreateMinorItemsList()
        {
            var item_list = Treasure_Factory.CreateMinorItemList();
            Assert.IsNotNull(item_list);
            item_list.GenerateItem();
            foreach (var item in item_list.GeneratedItems)
            {
                Assert.IsTrue(item.GetType() == typeof(MagicItem) || item.GetType() == typeof(CommonItem));
            }
        }

        /* Medium MagicItem Test Creation Methods
         * Test Create: Weapons, Armour/Shields, Potions, Rings, Rods, Scrolls, Staffs, Wands
         * and Wondrous items.
         */ 

        [Test]
        public void TestCreateMediumWeaponsList()
        {
            var treasure_pile = Treasure_Factory.CreateMediumWeaponsList();
            Assert.IsNotNull(treasure_pile);
            treasure_pile.GenerateItem();
            foreach (var generated_item in treasure_pile.GeneratedItems)
            {
                Assert.IsTrue(generated_item.GetType() == typeof(MagicItem) || generated_item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMediumArmourShieldList()
        {
            var treasure_pile = Treasure_Factory.CreateMediumArmourShieldList();
            Assert.IsNotNull(treasure_pile);
            treasure_pile.GenerateItem();
            foreach (var generated_item in treasure_pile.GeneratedItems)
            {
                Assert.IsTrue(generated_item.GetType() == typeof(MagicItem) || generated_item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMediumRingsList()
        {
            var treasure_pile = Treasure_Factory.CreateMediumRingsList();
            Assert.IsNotNull(treasure_pile);
            treasure_pile.GenerateItem();
            foreach (var generated_item in treasure_pile.GeneratedItems)
            {
                Assert.IsTrue(generated_item.GetType() == typeof(MagicItem) || generated_item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMediumRodsList()
        {
            var treasure_pile = Treasure_Factory.CreateMediumRodsList();
            Assert.IsNotNull(treasure_pile);
            treasure_pile.GenerateItem();
            foreach (var generated_item in treasure_pile.GeneratedItems)
            {
                Assert.IsTrue(generated_item.GetType() == typeof(MagicItem) || generated_item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMediumWandsList()
        {
            var treasure_pile = Treasure_Factory.CreateMediumWandsList();
            Assert.IsNotNull(treasure_pile);
            treasure_pile.GenerateItem();
            foreach (var generated_item in treasure_pile.GeneratedItems)
            {
                Assert.IsTrue(generated_item.GetType() == typeof(MagicItem) || generated_item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMediumStaffsList()
        {
            var treasure_pile = Treasure_Factory.CreateMediumStaffsList();
            Assert.IsNotNull(treasure_pile);
            treasure_pile.GenerateItem();
            foreach (var generated_item in treasure_pile.GeneratedItems)
            {
                Assert.IsTrue(generated_item.GetType() == typeof(MagicItem) || generated_item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMediumWondrousList()
        {
            var treasure_pile = Treasure_Factory.CreateMediumWondrousList();
            Assert.IsNotNull(treasure_pile);
            treasure_pile.GenerateItem();
            foreach (var generated_item in treasure_pile.GeneratedItems)
            {
                Assert.IsTrue(generated_item.GetType() == typeof(MagicItem) || generated_item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMediumPotionList()
        {
            var potion_list = Treasure_Factory.CreateMediumPotionsList();
            Assert.IsNotNull(potion_list);
            potion_list.GenerateItem();
            foreach (var item in potion_list.GeneratedItems)
            {
                Assert.IsTrue(item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMediumScrollList()
        {
            var scroll_list = Treasure_Factory.CreateMediumScrollList();
            Assert.IsNotNull(scroll_list);
            scroll_list.GenerateItem();
            foreach (var item in scroll_list.GeneratedItems)
            {
                Assert.AreEqual(item.GetType(), typeof(MagicItem));
            }
        }

        [Test]
        public void TestCreateMediumItemsList()
        {
            var item_list = Treasure_Factory.CreateMediumItemList();
            Assert.IsNotNull(item_list);
            item_list.GenerateItem();
            foreach (var item in item_list.GeneratedItems)
            {
                Assert.IsTrue(item.GetType() == typeof(MagicItem) || item.GetType() == typeof(CommonItem));
            }
        }

        /* Major MagicItem Test Creation Methods
         * Test Create: Weapons, Armour/Shields, Potions, Rings, Rods, Scrolls, Staffs, Wands
         * and Wondrous items.
         */

        [Test]
        public void TestCreateMajorWeaponsList()
        {
            var treasure_pile = Treasure_Factory.CreateMajorWeaponsList();
            Assert.IsNotNull(treasure_pile);
            treasure_pile.GenerateItem();
            foreach (var generated_item in treasure_pile.GeneratedItems)
            {
                Assert.IsTrue(generated_item.GetType() == typeof(MagicItem) || generated_item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMajorArmourShieldList()
        {
            var treasure_pile = Treasure_Factory.CreateMajorArmourShieldList();
            Assert.IsNotNull(treasure_pile);
            treasure_pile.GenerateItem();
            foreach (var generated_item in treasure_pile.GeneratedItems)
            {
                Assert.IsTrue(generated_item.GetType() == typeof(MagicItem) || generated_item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMajorRingsList()
        {
            var treasure_pile = Treasure_Factory.CreateMajorRingsList();
            Assert.IsNotNull(treasure_pile);
            treasure_pile.GenerateItem();
            foreach (var generated_item in treasure_pile.GeneratedItems)
            {
                Assert.IsTrue(generated_item.GetType() == typeof(MagicItem) || generated_item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMajorRodsList()
        {
            var treasure_pile = Treasure_Factory.CreateMajorRodsList();
            Assert.IsNotNull(treasure_pile);
            treasure_pile.GenerateItem();
            foreach (var generated_item in treasure_pile.GeneratedItems)
            {
                Assert.IsTrue(generated_item.GetType() == typeof(MagicItem) || generated_item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMajorWandsList()
        {
            var treasure_pile = Treasure_Factory.CreateMajorWandsList();
            Assert.IsNotNull(treasure_pile);
            treasure_pile.GenerateItem();
            foreach (var generated_item in treasure_pile.GeneratedItems)
            {
                Assert.IsTrue(generated_item.GetType() == typeof(MagicItem) || generated_item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMajorStaffsList()
        {
            var treasure_pile = Treasure_Factory.CreateMajorStaffsList();
            Assert.IsNotNull(treasure_pile);
            treasure_pile.GenerateItem();
            foreach (var generated_item in treasure_pile.GeneratedItems)
            {
                Assert.IsTrue(generated_item.GetType() == typeof(MagicItem) || generated_item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMajorWondrousList()
        {
            var treasure_pile = Treasure_Factory.CreateMajorWondrousList();
            Assert.IsNotNull(treasure_pile);
            treasure_pile.GenerateItem();
            foreach (var generated_item in treasure_pile.GeneratedItems)
            {
                Assert.IsTrue(generated_item.GetType() == typeof(MagicItem) || generated_item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMajorPotionList()
        {
            var potion_list = Treasure_Factory.CreateMajorPotionsList();
            Assert.IsNotNull(potion_list);
            potion_list.GenerateItem();
            foreach (var item in potion_list.GeneratedItems)
            {
                Assert.IsTrue(item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateMajorScrollList()
        {
            var scroll_list = Treasure_Factory.CreateMajorScrollList();
            Assert.IsNotNull(scroll_list);
            scroll_list.GenerateItem();
            foreach (var item in scroll_list.GeneratedItems)
            {
                Assert.AreEqual(item.GetType(), typeof(MagicItem));
            }
        }

        [Test]
        public void TestCreateMajorItemsList()
        {
            var item_list = Treasure_Factory.CreateMajorItemList();
            Assert.IsNotNull(item_list);
            item_list.GenerateItem();
            foreach (var item in item_list.GeneratedItems)
            {
                Assert.IsTrue(item.GetType() == typeof(MagicItem) || item.GetType() == typeof(CommonItem));
            }
        }

        /* Spell List Test Creation Methods
         * Test Create: Arcane spells (level 0 - 9), and Divine spells (level 0 - 9).
         */

        [Test]
        public void TestCreateLevel0ArcaneSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel0ArcaneSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateLevel1ArcaneSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel1ArcaneSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateLevel2ArcaneSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel2ArcaneSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateLevel3ArcaneSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel3ArcaneSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateLevel4ArcaneSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel4ArcaneSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateLevel5ArcaneSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel5ArcaneSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateLevel6ArcaneSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel6ArcaneSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateLevel7ArcaneSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel7ArcaneSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateLevel8ArcaneSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel8ArcaneSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateLevel9ArcaneSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel9ArcaneSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateLevel0DivineSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel0DivineSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateLevel1DivineSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel1DivineSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateLevel2DivineSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel2DivineSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateLevel3DivineSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel3DivineSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateLevel4DivineSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel4DivineSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateLevel5DivineSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel5DivineSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateLevel6DivineSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel6DivineSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateLevel7DivineSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel7DivineSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateLevel8DivineSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel8DivineSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        [Test]
        public void TestCreateLevel9DivineSpellList()
        {
            var spell_list = Treasure_Factory.CreateLevel9DivineSpellList();
            Assert.IsNotNull(spell_list);
            Assert.IsTrue(spell_list.Count >= 2);
            foreach (var enchantment in spell_list)
            {
                Assert.IsTrue(enchantment.GetType() == typeof(EnchantmentType));
            }
        }

        /* Treasure Generater and Treasure Factory Encounter Treasure Creation Methods
         * Will test factory methods to ensure other modules are being created properly.
         * Will test that the generater
         */ 

        [Test]
        public void TestInstantiateGenerator()
        {
            var treasure_generator = new Treasure_Generator(1);
            Assert.IsNotNull(treasure_generator);
            var items = treasure_generator.Generate();
            Assert.IsNotNull(items);
            foreach (var item in items)
            {
                Assert.IsNotNull(item);
                Assert.IsTrue(item.GetType() == typeof(MagicItem) || item.GetType() == typeof(CommonItem));
            }
        }

        [Test]
        public void TestCreateEncounterCoins()
        {
            for (int i = 1; i <= 20; i++)
            {
                var treasure_list = Treasure_Factory.CreateEncounterCoins(i);
                Assert.IsNotNull(treasure_list);
                var items = treasure_list.GenerateItem();
                Assert.IsNotNull(items);
                foreach (var item in items)
                {
                    Assert.IsNotNull(item);
                    Assert.IsTrue(item.GetType() == typeof(MagicItem) || item.GetType() == typeof(CommonItem));
                }
            }
        }

        [Test]
        public void TestCreateEncounterGoods()
        {
            for (int i = 1; i <= 20; i++)
            {
                var treasure_list = Treasure_Factory.CreateEncounterGoods(i);
                Assert.IsNotNull(treasure_list);
                var items = treasure_list.GenerateItem();
                Assert.IsNotNull(items);
                foreach (var item in items)
                {
                    Assert.IsNotNull(item);
                    Assert.IsTrue(item.GetType() == typeof(MagicItem) || item.GetType() == typeof(CommonItem));
                }
            }
        }

        [Test]
        public void TestCreateEncounterItems()
        {
            for (int i = 1; i <= 20; i++)
            {
                var treasure_list = Treasure_Factory.CreateEncounterItems(i);
                Assert.IsNotNull(treasure_list);
                var items = treasure_list.GenerateItem();
                Assert.IsNotNull(items);
                foreach (var item in items)
                {
                    Assert.IsNotNull(item);
                    Assert.IsTrue(item.GetType() == typeof(MagicItem) || item.GetType() == typeof(CommonItem));
                }
            }
        }

    }
}
