﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace Treasure.Domain
{
    /*
     * Item Class:
     * Represents an item in Dungeons and Dragons. Item name is given by the name member, and value
     * represents its value as a Dictionary key-value pair object.
     * */
    public class Item
    { 
        // TODO: Make print method for item to display
        public virtual Item GenerateItem()
        {
            return null;
        }

        public Tuple<int, string> Value
        { get; set; }

        public string Name
        { get; set; }  
    }

    public class CommonItem : Item
    {
        public CommonItem (int amount, string coin_type, string name)
        {
            this.Value = Tuple.Create<int, string>(amount, coin_type);
            this.Name = name;
        }

        public override Item GenerateItem()
        {
            return this;
        }
    }

    public class MagicItem : Item
    {
        public MagicItem (int amount, string coin_type, string name, List<EnchantmentType> enchantments)
        {
            this.Value = Tuple.Create<int, string>(amount, coin_type);
            this.Name = name;
            this.Enchantments = enchantments;
            this.Die = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
        }

        public MagicItem (Item item, List<EnchantmentType> enchantments)
        {
            this.Value = item.Value;
            this.Name = item.Name;
            this.Enchantments = enchantments;
            this.Die = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
        }

        public override Item GenerateItem()
        {
            Enchant(0);
            return this;
        }

        private void Enchant(int past_roll)
        {
            var count = 0;
            var roll = Die.Next(101);
            if (roll == past_roll)
            {
                roll += 1;
            }

            if (roll > 100)
            {
                roll = Math.Abs(roll - 100);
            }

            foreach (var enchantment in Enchantments)
            {
                count += 1;
                if (roll <= enchantment.UpperProbability)
                {
                    Name += " " + enchantment.Enchantment.Name;
                    var cost_amount = Value.Item1;
                    var cost_type = Value.Item2;
                    cost_amount += enchantment.Enchantment.Cost.Item1;
                    Value = Tuple.Create<int, string>(cost_amount, cost_type);
                    return;
                }
            }
            Enchant(roll);
            Enchant(roll);
            return;
        }

        Random Die
        { get; set; }

        List<EnchantmentType> Enchantments
        { get; set; }
    }

    public class ItemType
    {
        public ItemType (string name, int lowerProbability, int upperProbability, Item item)
        {
            this.Name = name;
            this.LowerProbability = lowerProbability;
            this.UpperProbability = upperProbability;
            this.Item = item;
        }

        public void SetItem(Item item)
        {
            this.Item = item;
        }

        public ItemType CopyItem(Item item)
        {
            var itemType = new ItemType(Name, LowerProbability, UpperProbability, item);
            return itemType;
        }

        public string Name
        { get; set; }

        public int LowerProbability
        { get; set; }

        public int UpperProbability
        { get; set; }

        public Item Item
        { get; set; }
    }

    public class Enchantment
    {
        public Enchantment (string name, int cost_amount, string cost_coin_type)
        {
            Name = name;
            Cost = Tuple.Create<int, string>(cost_amount, cost_coin_type);
        }

        public string Name
        { get; set; }

        public Tuple<int, string> Cost
        { get; set; }
    }

    public class EnchantmentType
    {
        public EnchantmentType (int lowerProbability, int upperProbability, Enchantment enchantment)
        {
            LowerProbability = lowerProbability;
            UpperProbability = upperProbability;
            Enchantment = enchantment;
        }
        
        public int LowerProbability
        { get; set; }

        public int UpperProbability
        { get; set; }

        public Enchantment Enchantment
        { get; set; }
    }

    public class TreasureType
    {
        public TreasureType(string name, int lowerProbability, int upperProbability, Treasure treasure, int dice_quantity = 1, int dice_type = 1)
        {
            this.Name = name;
            this.LowerProbability = lowerProbability;
            this.UpperProbability = upperProbability;
            this.treasure = treasure;
            this.DiceQuantity = dice_quantity;
            this.DiceType = dice_type;
        }

        public string Name
        { get; set; }

        public int LowerProbability
        { get; set; }

        public int UpperProbability
        { get; set; }

        public Treasure treasure
        { get; set; }

        public int DiceQuantity
        { get; set; }

        public int DiceType
        { get; set; }
    }

    public class Treasure
    {
        public virtual List<Item> GenerateItem()
        {
            return null;
        }

        public List<Item> GeneratedItems
        { get; set; }
    }

    public class Treasure_Composite : Treasure
    {
        public Treasure_Composite()
        {
            treasures = new List<TreasureType>();
            die = new Random();
        }

        public override List<Item> GenerateItem()
        {
            //var die = new Random();
            var roll = die.Next(101);
            if (roll > 100)
            {
                roll = 100;
            }

            foreach (var treasure in treasures)
            {
                if (roll <= treasure.UpperProbability)
                {
                    var item_list = new List<Item>();

                    // Get number of treasures to generate
                    int number_treasures = 0;
                    for (int i = 0; i < treasure.DiceQuantity; i++)
                    {
                        roll = die.Next(treasure.DiceType) + 1;
                        if (roll > treasure.DiceType) { roll = treasure.DiceType; }
                        number_treasures += roll;
                    }

                    // Generate treasure and add to list to return
                    for (int i = 0; i < number_treasures; i++)
                    {
                        if (treasure.treasure.GenerateItem() != null)
                        {
                            item_list.AddRange(treasure.treasure.GeneratedItems);
                        }
                    }

                    GeneratedItems = item_list;
                    return item_list;
                }
            }

            return null;
        }

        public void AddTreasure(TreasureType treasure)
        {
            treasures.Add(treasure);
        }

        public void AddTreasure(List<TreasureType> treasure)
        {
            treasures.AddRange(treasure);
        }

        private Random die;
        private List<TreasureType> treasures;
    }

    public class Treasure_Leaf : Treasure
    {
        public Treasure_Leaf()
        {
            treasures = new List<ItemType>();
            die = new Random();
        }

        public override List<Item> GenerateItem()
        {
            //var die = new Random();
            var roll = die.Next(101);
            if (roll > 100)
            {
                roll = 100;
            }

            foreach (var treasure in treasures)
            {
                
                if (roll <= treasure.UpperProbability)
                {
                    var item_list = new List<Item>();
                    item_list.Add(treasure.Item.GenerateItem());
                    GeneratedItems = item_list;
                    return item_list;
                }
            }

            return null;
        }

        public void AddTreasure(ItemType treasure)
        {
            treasures.Add(treasure);
        }

        public void AddTreasure(List<ItemType> treasure)
        {
            treasures.AddRange(treasure);
        }

        private Random die;
        private List<ItemType> treasures;
    }

    public class Treasure_Generator
    {
        public Treasure_Generator (int encounter_level)
        {
            m_coin_pile = Treasure_Factory.CreateEncounterCoins(encounter_level);
            m_goods_pile = Treasure_Factory.CreateEncounterGoods(encounter_level);
            m_items_pile = Treasure_Factory.CreateEncounterItems(encounter_level);
        }

        public List<Item> Generate ()
        {
            List<Item> treasure_pile = new List<Item>();
            treasure_pile.AddRange(m_coin_pile.GenerateItem());
            treasure_pile.AddRange(m_goods_pile.GenerateItem());
            treasure_pile.AddRange(m_items_pile.GenerateItem());
            return treasure_pile;
        }    

        public List<Item> GenerateCoins()
        {
            var coin_pile = new List<Item>();
            coin_pile.AddRange(m_coin_pile.GenerateItem());
            return coin_pile;
        }
        
        public List<Item> GenerateGoods()
        {
            var goods_pile = new List<Item>();
            goods_pile.AddRange(m_goods_pile.GenerateItem());
            return goods_pile;
        }

        public List<Item> GenerateItems()
        {
            var items_pile = new List<Item>();
            items_pile.AddRange(m_items_pile.GenerateItem());
            return items_pile;
        }

        private Treasure_Leaf m_coin_pile
        { get; set; }

        private Treasure_Composite m_goods_pile
        { get; set; }

        private Treasure_Composite m_items_pile
        { get; set; }

    }

    public class Treasure_Factory
    {
        /* Encounter Treasure Creation Methods
         * Create Sets of Coins, Goods, and (Mundane, Minor, Medium, and Major) Items
         */ 
        
        private static Treasure_Leaf SetupNULLTreasure()
        {
            var treasure = new Treasure_Leaf();
            treasure.AddTreasure(new ItemType("NULL", 0, 100, new CommonItem(0, "cp", "Nothing Found")));
            return treasure;
        }

        private static Treasure_Leaf SetupEncounterCoins(int encounter_level)
        {
            var coin_list = new Treasure_Leaf();

            switch(encounter_level)
            {
                case 1:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 14, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 15, 29, CreateCoinItem(1, 6, 1000, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 30, 52, CreateCoinItem(1, 8, 100, "sp")));
                    coin_list.AddTreasure(new ItemType("Coins", 53, 95, CreateCoinItem(2, 8, 10, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 96, 100, CreateCoinItem(1, 4, 10, "pp")));
                    break;
                case 2:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 13, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 14, 23, CreateCoinItem(1, 10, 1000, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 24, 43, CreateCoinItem(2, 10, 100, "sp")));
                    coin_list.AddTreasure(new ItemType("Coins", 44, 95, CreateCoinItem(4, 10, 10, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 96, 100, CreateCoinItem(2, 8, 10, "pp")));
                    break;
                case 3:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 11, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 12, 21, CreateCoinItem(2, 10, 1000, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 22, 41, CreateCoinItem(4, 8, 100, "sp")));
                    coin_list.AddTreasure(new ItemType("Coins", 42, 95, CreateCoinItem(1, 4, 100, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 96, 100, CreateCoinItem(1, 10, 10, "pp")));
                    break;
                case 4:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 14, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 12, 21, CreateCoinItem(3, 10, 1000, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 22, 41, CreateCoinItem(4, 12, 1000, "sp")));
                    coin_list.AddTreasure(new ItemType("Coins", 42, 95, CreateCoinItem(1, 6, 100, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 96, 100, CreateCoinItem(1, 8, 10, "pp")));
                    break;
                case 5:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 10, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 11, 19, CreateCoinItem(1, 4, 10000, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 20, 38, CreateCoinItem(1, 6, 1000, "sp")));
                    coin_list.AddTreasure(new ItemType("Coins", 39, 95, CreateCoinItem(1, 8, 100, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 96, 100, CreateCoinItem(1, 10, 10, "pp")));
                    break;
                case 6:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 10, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 11, 18, CreateCoinItem(1, 6, 10000, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 19, 37, CreateCoinItem(1, 8, 1000, "sp")));
                    coin_list.AddTreasure(new ItemType("Coins", 38, 95, CreateCoinItem(1, 10, 100, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 96, 100, CreateCoinItem(1, 12, 10, "pp")));
                    break;
                case 7:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 11, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 12, 18, CreateCoinItem(1, 10, 10000, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 19, 35, CreateCoinItem(1, 12, 1000, "sp")));
                    coin_list.AddTreasure(new ItemType("Coins", 36, 93, CreateCoinItem(2, 6, 100, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 94, 100, CreateCoinItem(3, 4, 10, "pp")));
                    break;
                case 8:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 10, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 11, 15, CreateCoinItem(1, 12, 10000, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 16, 29, CreateCoinItem(2, 6, 1000, "sp")));
                    coin_list.AddTreasure(new ItemType("Coins", 30, 87, CreateCoinItem(2, 8, 100, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 88, 100, CreateCoinItem(3, 6, 10, "pp")));
                    break;
                case 9:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 10, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 11, 15, CreateCoinItem(2, 6, 10000, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 16, 29, CreateCoinItem(2, 8, 1000, "sp")));
                    coin_list.AddTreasure(new ItemType("Coins", 30, 85, CreateCoinItem(5, 4, 100, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 86, 100, CreateCoinItem(2, 12, 10, "pp")));
                    break;
                case 10:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 10, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 11, 24, CreateCoinItem(2, 10, 1000, "sp")));
                    coin_list.AddTreasure(new ItemType("Coins", 25, 79, CreateCoinItem(6, 4, 100, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 80, 100, CreateCoinItem(5, 6, 10, "pp")));
                    break;
                case 11:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 8, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 9, 14, CreateCoinItem(3, 10, 1000, "sp")));
                    coin_list.AddTreasure(new ItemType("Coins", 15, 75, CreateCoinItem(4, 8, 100, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 76, 100, CreateCoinItem(4, 10, 10, "pp")));
                    break;
                case 12:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 8, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 9, 14, CreateCoinItem(3, 12, 1000, "sp")));
                    coin_list.AddTreasure(new ItemType("Coins", 15, 75, CreateCoinItem(1, 4, 1000, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 76, 100, CreateCoinItem(1, 4, 100, "pp")));
                    break;
                case 13:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 8, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 9, 75, CreateCoinItem(1, 4, 1000, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 76, 100, CreateCoinItem(1, 10, 100, "pp")));
                    break;
                case 14:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 8, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 9, 75, CreateCoinItem(1, 6, 1000, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 76, 100, CreateCoinItem(1, 12, 100, "pp")));
                    break;
                case 15:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 3, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 4, 74, CreateCoinItem(1, 8, 1000, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 75, 100, CreateCoinItem(1, 12, 100, "pp")));
                    break;
                case 16:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 3, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 4, 74, CreateCoinItem(1, 12, 1000, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 75, 100, CreateCoinItem(3, 4, 100, "pp")));
                    break;
                case 17:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 3, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 4, 68, CreateCoinItem(3, 4, 1000, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 69, 100, CreateCoinItem(2, 10, 100, "pp")));
                    break;
                case 18:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 2, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 3, 65, CreateCoinItem(3, 6, 1000, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 66, 100, CreateCoinItem(5, 4, 10, "pp")));
                    break;
                case 19:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 2, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 3, 65, CreateCoinItem(3, 8, 1000, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 66, 100, CreateCoinItem(3, 10, 100, "pp")));
                    break;
                case 20:
                    coin_list.AddTreasure(new ItemType("Coins", 0, 2, CreateCoinItem(0, 0, 0, "cp")));
                    coin_list.AddTreasure(new ItemType("Coins", 3, 65, CreateCoinItem(4, 8, 1000, "gp")));
                    coin_list.AddTreasure(new ItemType("Coins", 66, 100, CreateCoinItem(4, 10, 100, "pp")));
                    break;
                default:
                    break;
            }

            return coin_list;
        }

        public static Treasure_Leaf CreateEncounterCoins(int encounter_level)
        {
            return SetupEncounterCoins(encounter_level);
        }

        private static Treasure_Composite SetupEncounterGoods(int encounter_level)
        {
            var goods_list = new Treasure_Composite();

            switch(encounter_level)
            {
                case 1:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 90, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 91, 95, CreateGemList()));
                    goods_list.AddTreasure(new TreasureType("Art", 96, 100, CreateArtList()));
                    break;
                case 2:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 81, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 82, 95, CreateGemList(), 1, 3));
                    goods_list.AddTreasure(new TreasureType("Art", 96, 100, CreateArtList(), 1, 3));
                    break;
                case 3:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 77, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 78, 95, CreateGemList(), 1, 3));
                    goods_list.AddTreasure(new TreasureType("Art", 96, 100, CreateArtList(), 1, 3));
                    break;
                case 4:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 70, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 71, 95, CreateGemList(), 1, 4));
                    goods_list.AddTreasure(new TreasureType("Art", 96, 100, CreateArtList(), 1, 3));
                    break;
                case 5:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 60, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 61, 95, CreateGemList(), 1, 4));
                    goods_list.AddTreasure(new TreasureType("Art", 96, 100, CreateArtList(), 1, 4));
                    break;
                case 6:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 56, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 57, 92, CreateGemList(), 1, 4));
                    goods_list.AddTreasure(new TreasureType("Art", 93, 100, CreateArtList(), 1, 4));
                    break;
                case 7:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 48, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 49, 88, CreateGemList(), 1, 4));
                    goods_list.AddTreasure(new TreasureType("Art", 89, 100, CreateArtList(), 1, 4));
                    break;
                case 8:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 45, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 46, 85, CreateGemList(), 1, 6));
                    goods_list.AddTreasure(new TreasureType("Art", 86, 100, CreateArtList(), 1, 4));
                    break;
                case 9:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 40, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 41, 80, CreateGemList(), 1, 8));
                    goods_list.AddTreasure(new TreasureType("Art", 81, 100, CreateArtList(), 1, 4));
                    break;
                case 10:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 35, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 36, 79, CreateGemList(), 1, 8));
                    goods_list.AddTreasure(new TreasureType("Art", 80, 100, CreateArtList(), 1, 6));
                    break;
                case 11:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 24, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 25, 74, CreateGemList(), 1, 10));
                    goods_list.AddTreasure(new TreasureType("Art", 75, 100, CreateArtList(), 1, 6));
                    break;
                case 12:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 17, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 18, 70, CreateGemList(), 1, 10));
                    goods_list.AddTreasure(new TreasureType("Art", 71, 100, CreateArtList(), 1, 8));
                    break;
                case 13:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 11, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 12, 66, CreateGemList(), 1, 12));
                    goods_list.AddTreasure(new TreasureType("Art", 67, 100, CreateArtList(), 1, 10));
                    break;
                case 14:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 11, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 12, 66, CreateGemList(), 2, 8));
                    goods_list.AddTreasure(new TreasureType("Art", 67, 100, CreateArtList(), 2, 6));
                    break;
                case 15:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 9, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 10, 65, CreateGemList(), 2, 10));
                    goods_list.AddTreasure(new TreasureType("Art", 66, 100, CreateArtList(), 2, 8));
                    break;
                case 16:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 7, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 8, 64, CreateGemList(), 4, 6));
                    goods_list.AddTreasure(new TreasureType("Art", 65, 100, CreateArtList(), 2, 10));
                    break;
                case 17:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 4, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 5, 63, CreateGemList(), 4, 8));
                    goods_list.AddTreasure(new TreasureType("Art", 64, 100, CreateArtList(), 3, 8));
                    break;
                case 18:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 4, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 5, 54, CreateGemList(), 3, 12));
                    goods_list.AddTreasure(new TreasureType("Art", 55, 100, CreateArtList(), 3, 10));
                    break;
                case 19:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 3, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 4, 50, CreateGemList(), 6, 6));
                    goods_list.AddTreasure(new TreasureType("Art", 51, 100, CreateArtList(), 6, 6));
                    break;
                case 20:
                    goods_list.AddTreasure(new TreasureType("NULL", 0, 2, SetupNULLTreasure()));
                    goods_list.AddTreasure(new TreasureType("Gems", 3, 38, CreateGemList(), 4, 10));
                    goods_list.AddTreasure(new TreasureType("Art", 39, 100, CreateArtList(), 7, 6));
                    break;
                default:
                    break;
            }

            return goods_list;
        }

        public static Treasure_Composite CreateEncounterGoods(int encounter_level)
        {
            return SetupEncounterGoods(encounter_level);
        }

        private static Treasure_Composite SetupEncounterItems(int encounter_level)
        {
            var item_list = new Treasure_Composite();

            switch(encounter_level)
            {
                case 1:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 71, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Mundane Items", 72, 95, CreateMundaneItemsList()));
                    item_list.AddTreasure(new TreasureType("Minor Items", 96, 100, CreateMinorItemList()));
                    break;
                case 2:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 49, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Mundane Items", 50, 85, CreateMundaneItemsList()));
                    item_list.AddTreasure(new TreasureType("Minor Items", 86, 100, CreateMinorItemList()));
                    break;
                case 3:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 49, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Mundane Items", 50, 79, CreateMundaneItemsList(), 1, 3));
                    item_list.AddTreasure(new TreasureType("Minor Items", 80, 100, CreateMinorItemList()));
                    break;
                case 4:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 42, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Mundane Items", 43, 62, CreateMundaneItemsList(), 1, 4));
                    item_list.AddTreasure(new TreasureType("Minor Items", 63, 100, CreateMinorItemList()));
                    break;
                case 5:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 57, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Mundane Items", 58, 67, CreateMundaneItemsList(), 1, 4));
                    item_list.AddTreasure(new TreasureType("Minor Items", 68, 100, CreateMinorItemList(), 1, 3));
                    break;
                case 6:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 54, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Mundane Items", 55, 59, CreateMundaneItemsList(), 1, 4));
                    item_list.AddTreasure(new TreasureType("Minor Items", 60, 99, CreateMinorItemList(), 1, 3));
                    item_list.AddTreasure(new TreasureType("Medium Items", 100, 100, CreateMediumItemList()));
                    break;
                case 7:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 51, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Minor Items", 52, 97, CreateMinorItemList(), 1, 3));
                    item_list.AddTreasure(new TreasureType("Medium Items", 98, 100, CreateMediumItemList()));
                    break;
                case 8:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 48, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Minor Items", 49, 96, CreateMinorItemList(), 1, 4));
                    item_list.AddTreasure(new TreasureType("Medium Items", 97, 100, CreateMediumItemList()));
                    break;
                case 9:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 43, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Minor Items", 44, 91, CreateMinorItemList(), 1, 4));
                    item_list.AddTreasure(new TreasureType("Medium Items", 92, 100, CreateMediumItemList()));
                    break;
                case 10:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 40, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Minor Items", 41, 88, CreateMinorItemList(), 1, 4));
                    item_list.AddTreasure(new TreasureType("Medium Items", 89, 99, CreateMediumItemList()));
                    item_list.AddTreasure(new TreasureType("Major Items", 100, 100, CreateMajorItemList()));
                    break;
                case 11:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 31, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Minor Items", 32, 84, CreateMinorItemList(), 1, 4));
                    item_list.AddTreasure(new TreasureType("Medium Items", 85, 98, CreateMediumItemList()));
                    item_list.AddTreasure(new TreasureType("Major Items", 99, 100, CreateMajorItemList()));
                    break;
                case 12:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 27, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Minor Items", 28, 82, CreateMinorItemList(), 1, 6));
                    item_list.AddTreasure(new TreasureType("Medium Items", 83, 97, CreateMediumItemList()));
                    item_list.AddTreasure(new TreasureType("Major Items", 98, 100, CreateMajorItemList()));
                    break;
                case 13:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 19, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Minor Items", 20, 73, CreateMinorItemList(), 1, 6));
                    item_list.AddTreasure(new TreasureType("Medium Items", 74, 95, CreateMediumItemList()));
                    item_list.AddTreasure(new TreasureType("Major Items", 96, 100, CreateMajorItemList()));
                    break;
                case 14:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 19, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Minor Items", 20, 58, CreateMinorItemList(), 1, 6));
                    item_list.AddTreasure(new TreasureType("Medium Items", 59, 92, CreateMediumItemList()));
                    item_list.AddTreasure(new TreasureType("Major Items", 93, 100, CreateMajorItemList()));
                    break;
                case 15:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 11, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Minor Items", 12, 46, CreateMinorItemList(), 1, 10));
                    item_list.AddTreasure(new TreasureType("Medium Items", 47, 90, CreateMediumItemList()));
                    item_list.AddTreasure(new TreasureType("Major Items", 91, 100, CreateMajorItemList()));
                    break;
                case 16:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 40, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Minor Items", 41, 46, CreateMinorItemList(), 1, 10));
                    item_list.AddTreasure(new TreasureType("Medium Items", 47, 90, CreateMediumItemList(), 1, 3));
                    item_list.AddTreasure(new TreasureType("Major Items", 91, 100, CreateMajorItemList()));
                    break;
                case 17:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 33, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Medium Items", 34, 83, CreateMediumItemList(), 1, 4));
                    item_list.AddTreasure(new TreasureType("Major Items", 84, 100, CreateMajorItemList()));
                    break;
                case 18:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 24, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Medium Items", 25, 80, CreateMediumItemList(), 1, 4));
                    item_list.AddTreasure(new TreasureType("Major Items", 81, 100, CreateMajorItemList()));
                    break;
                case 19:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 4, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Medium Items", 5, 70, CreateMediumItemList(), 1, 4));
                    item_list.AddTreasure(new TreasureType("Major Items", 71, 100, CreateMajorItemList()));
                    break;
                case 20:
                    item_list.AddTreasure(new TreasureType("NULL", 0, 25, SetupNULLTreasure()));
                    item_list.AddTreasure(new TreasureType("Medium Items", 26, 65, CreateMediumItemList(), 1, 4));
                    item_list.AddTreasure(new TreasureType("Major Items", 66, 100, CreateMajorItemList(), 1, 3));
                    break;
                default:
                    break;
            }

            return item_list;
        }

        public static Treasure_Composite CreateEncounterItems(int encounter_level)
        {
            return SetupEncounterItems(encounter_level);
        }


        /* Coins Creation Methods
         * Create: Coins
         */

        public static CommonItem CreateCoinItem(int numberOfDice, int diceType, int multiplier, string coinType)
        {
            var die = new Random();
            int roll = 0;

            for (int i = 0; i < numberOfDice; i++)
            {
                roll += die.Next(diceType+1) + 1;
                if(roll > diceType)
                {
                    roll = diceType;
                }
            }

            roll *= multiplier;

            return new CommonItem(roll, coinType, "Coins");
        }

        private static Treasure_Leaf SetupCoinTypes()
        {
            var treasure_leaf = new Treasure_Leaf();
            // Add coin items to the list...

            treasure_leaf.AddTreasure(new ItemType("Coin", 0, 100, CreateCoinItem(1, 6, 100, "gp")));
            return treasure_leaf;
        }

        public static Treasure_Leaf CreateCoinsList()
        {
            return SetupCoinTypes();
        }

        /* Goods Creation Methods
         * Create: Weapons, Armour/Shields, Potions, Rings, Rods, Scrolls, Staffs, Wands, and Wondrous Items
         */

        private static Treasure_Leaf SetupGemTypes ()
        {
            var treasure_leaf = new Treasure_Leaf();
            treasure_leaf.AddTreasure(new ItemType("Gem", 0, 25, new CommonItem(10, "gp", "Banded, eye, or moss agate; azurite; blue quartz; hematite; lapis lazuli; malachite; obsidian; rhodochrosite; tiger eye turquoise; freshwater (irregular) pearl")));
            treasure_leaf.AddTreasure(new ItemType("Gem", 26, 50, new CommonItem(50, "gp", "Bloodstone; carnelian; chalcedony; chrysoprase; citrine; iolite, jasper; moonstone; onyx; peridot; rock crystal (clear quartz); sard; sardonyx; rose, smoky, or star rose quartz; zircon")));
            treasure_leaf.AddTreasure(new ItemType("Gem", 51, 70, new CommonItem(100, "gp", "Amber; amethyst; chrysoberyl; coral; red or brown-green garnet; jade; jet; white, golden, pink, or silver pearl; red spinel, red-brown or deep green spinel; tourmaline")));
            treasure_leaf.AddTreasure(new ItemType("Gem", 71, 90, new CommonItem(10, "gp", "Alexandrite; aquamarine; violet garnet; black pearl; deep blue spinel; golden yellow topaz")));
            treasure_leaf.AddTreasure(new ItemType("Gem", 91, 99, new CommonItem(10, "gp", "Emerald; white, black, or fire opal; blue sapphire; fiery yellow or rich purple corundum; blue or black star sapphire; star ruby")));
            treasure_leaf.AddTreasure(new ItemType("Gem", 100, 100, new CommonItem(10, "gp", "Clearest bright green emerald; blue-white, canary, pink, brown, or blue diamond; jacinth")));
            return treasure_leaf;
        }

        public static Treasure_Leaf CreateGemList()
        {          
            return SetupGemTypes();
        }

        private static Treasure_Leaf SetupArtTypes()
        {
            var treasure_leaf = new Treasure_Leaf();
            treasure_leaf.AddTreasure(new ItemType("Art", 0, 10, new CommonItem(55, "gp", "Silver ewer; carved bone/ivory statue; finely wrought small gold braclet")));
            treasure_leaf.AddTreasure(new ItemType("Art", 11, 25, new CommonItem(105, "gp", "Cloth of gold vestments; black velvent mask with numerous citrines; silver chalice with lapis lazuli gems")));
            treasure_leaf.AddTreasure(new ItemType("Art", 26, 40, new CommonItem(305, "gp", "Large well-done wool tapestry; brass mug with jade inlays")));
            treasure_leaf.AddTreasure(new ItemType("Art", 41, 50, new CommonItem(550, "gp", "Silver comb with moonstones; silver-plated steel longsword with jet jewel in hilt")));
            treasure_leaf.AddTreasure(new ItemType("Art", 51, 60, new CommonItem(700, "gp", "Carved harp of exotic wood with ivory inlay and zircon gems; solid gold idol (10lb)")));
            treasure_leaf.AddTreasure(new ItemType("Art", 61, 70, new CommonItem(1050, "gp", "Gold dragon comb with red garnet eyes; gold and topax bottle stopper cork; ceremonial electrum dagger with a star ruby in the pommel")));
            treasure_leaf.AddTreasure(new ItemType("Art", 71, 80, new CommonItem(1400, "gp", "Eyepatch with mock eye of sapphire and moonstone; fire opal pendant on a fine gold chain; old masterpiece painting")));
            treasure_leaf.AddTreasure(new ItemType("Art", 81, 85, new CommonItem(1750, "gp", "Embroidered silk and velvet mantle with numerous moonstones; sapphire pendant on gold chain")));
            treasure_leaf.AddTreasure(new ItemType("Art", 86, 90, new CommonItem(2500, "gp", "Embroidered and bejeweled glove; jeweled anklet; gold music box")));
            treasure_leaf.AddTreasure(new ItemType("Art", 91, 95, new CommonItem(3500, "gp", "Golden circlet with four aquamarines; a string of small pink pearls (necklace)")));
            treasure_leaf.AddTreasure(new ItemType("Art", 96, 99, new CommonItem(5000, "gp", "Jeweled gold crown; jeweled electrum ring")));
            treasure_leaf.AddTreasure(new ItemType("Art", 100, 100, new CommonItem(7000, "gp", "Gold and ruby ring; gold cup set with emeralds")));
            return treasure_leaf;
        }

        public static Treasure_Leaf CreateArtList()
        {
            return SetupArtTypes();
        }

        private static Treasure_Composite SetupGoodsTypes()
        {
            var treasure_composite = new Treasure_Composite();
            treasure_composite.AddTreasure(new TreasureType("Gems", 0, 50, CreateGemList()));
            treasure_composite.AddTreasure(new TreasureType("Art", 51, 100, CreateArtList()));
            return treasure_composite;
        }

        public static Treasure_Composite CreateGoodsList()
        {
            
            return SetupGoodsTypes();
        }

        private static Treasure_Composite SetupItemTypes()
        {
            var treasure_composite = new Treasure_Composite();
            // Add composite treasure items here...
            treasure_composite.AddTreasure(new TreasureType("Coins", 0, 30, CreateCoinsList()));
            treasure_composite.AddTreasure(new TreasureType("Goods", 31, 100, CreateGoodsList()));

            return treasure_composite;
        }

        public static Treasure_Composite CreateItemsList()
        {
            return SetupItemTypes();
        }


        /* Spell List Methods
         * Create: Arcane (0-9), and Divine (0-9)
         */

        private static List<EnchantmentType> SetupLevel0ArcaneSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 4, new Enchantment("Acid Splash", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(5, 8, new Enchantment("Arcane Mark", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(9, 13, new Enchantment("Dancing Lights", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(14, 17, new Enchantment("Daze", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(18, 24, new Enchantment("Detect Magic", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(25, 28, new Enchantment("Detect Poison", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(29, 32, new Enchantment("Disrupt Undead", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(33, 37, new Enchantment("Flare", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(38, 42, new Enchantment("Ghost Sound", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(43, 44, new Enchantment("Know Direction", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(45, 50, new Enchantment("Light", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(51, 52, new Enchantment("Lullaby", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(53, 57, new Enchantment("Mage Hand", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(58, 62, new Enchantment("Mending", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(63, 67, new Enchantment("Message", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(68, 72, new Enchantment("Open/Close", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(73, 77, new Enchantment("Prestidigitation", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(78, 81, new Enchantment("Ray of Frost", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(82, 87, new Enchantment("Read Magic", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(88, 94, new Enchantment("Resistance", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(95, 96, new Enchantment("Summon Instrument", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(97, 100, new Enchantment("Touch of Fatigue", 12, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel0ArcaneSpellList()
        {
            return SetupLevel0ArcaneSpellList();
        }

        private static List<EnchantmentType> SetupLevel1ArcaneSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Alarm", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(4, 5, new Enchantment("Animate Rope", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(6, 7, new Enchantment("Burning Hands", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(8, 9, new Enchantment("Cause Fear", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(10, 12, new Enchantment("Charm Person", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(13, 14, new Enchantment("Chill Touch", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(15, 16, new Enchantment("Colour Spray", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(17, 19, new Enchantment("Comprehend Languages", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(20, 20, new Enchantment("Lesser Confusion", 50, "gp")));
            enchantment_list.Add(new EnchantmentType(21, 21, new Enchantment("Cure Light Wounds", 50, "gp")));
            enchantment_list.Add(new EnchantmentType(22, 24, new Enchantment("Detect Secret Doors", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(25, 26, new Enchantment("Detect Undead", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(27, 29, new Enchantment("Disguise Self", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(30, 32, new Enchantment("Endure Elements", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(33, 35, new Enchantment("Enlarge Person", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(36, 37, new Enchantment("Erase", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(38, 40, new Enchantment("Expeditious Retreat", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(41, 41, new Enchantment("Feather Fall", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(42, 43, new Enchantment("Grease", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(44, 45, new Enchantment("Hold Portal", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(46, 47, new Enchantment("Hypnotism", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(48, 49, new Enchantment("Identify", 125, "gp")));
            enchantment_list.Add(new EnchantmentType(50, 51, new Enchantment("Jump", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(52, 54, new Enchantment("Mage Armour", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(55, 56, new Enchantment("Magic Missile", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(57, 59, new Enchantment("Magic Weapon", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(60, 62, new Enchantment("Mount", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(63, 64, new Enchantment("Magic Aura", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(65, 66, new Enchantment("Obsuring Mist", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(67, 74, new Enchantment("Protection from (Chaos/Evil/Good/Law)", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(75, 76, new Enchantment("Ray of Enfeeblement", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(77, 78, new Enchantment("Reduce Person", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(79, 80, new Enchantment("Remove Fear", 50, "gp")));
            enchantment_list.Add(new EnchantmentType(81, 82, new Enchantment("Shield", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(83, 84, new Enchantment("Shocking Grasp", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(85, 86, new Enchantment("Silent Image", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(87, 88, new Enchantment("Sleep", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(89, 90, new Enchantment("Summon Monster I", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(91, 93, new Enchantment("Floating Disk", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(94, 95, new Enchantment("True Strike", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(96, 96, new Enchantment("Undetectable Alignment", 50, "gp")));
            enchantment_list.Add(new EnchantmentType(97, 98, new Enchantment("Unseen Servent", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(99, 100, new Enchantment("Ventriloquism", 25, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel1ArcaneSpellList()
        {
            return SetupLevel1ArcaneSpellList();
        }

        private static List<EnchantmentType> SetupLevel2ArcaneSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 1, new Enchantment("Animal Messenger", 200, "gp")));
            enchantment_list.Add(new EnchantmentType(2, 2, new Enchantment("Animal Trance", 200, "gp")));
            enchantment_list.Add(new EnchantmentType(3, 3, new Enchantment("Arcane Lock", 175, "gp")));
            enchantment_list.Add(new EnchantmentType(4, 6, new Enchantment("Bear's Endurance", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(7, 8, new Enchantment("Blindness/Deafness", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(9, 10, new Enchantment("Blur", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(11, 13, new Enchantment("Bull's Strength", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(14, 14, new Enchantment("Calm Emotions", 200, "gp")));
            enchantment_list.Add(new EnchantmentType(15, 17, new Enchantment("Cat's Grace", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(18, 19, new Enchantment("Command Undead", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(20, 20, new Enchantment("Continual Flame", 200, "gp")));
            enchantment_list.Add(new EnchantmentType(21, 21, new Enchantment("Cure Moderate Wounds", 200, "gp")));
            enchantment_list.Add(new EnchantmentType(22, 22, new Enchantment("Darkness", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(23, 25, new Enchantment("Darkvision", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(26, 26, new Enchantment("Daze Monster", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(27, 27, new Enchantment("Delay Poison", 200, "gp")));
            enchantment_list.Add(new EnchantmentType(28, 29, new Enchantment("Detect Thoughts", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(30, 31, new Enchantment("Disguise Self", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(32, 34, new Enchantment("Eagle's Splendor", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(35, 35, new Enchantment("Enthrall", 200, "gp")));
            enchantment_list.Add(new EnchantmentType(36, 37, new Enchantment("False Life", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(38, 39, new Enchantment("Flaming Sphere", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(40, 40, new Enchantment("Fog Cloud", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(41, 43, new Enchantment("Fox's Cunning", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(44, 44, new Enchantment("Ghoul Touch", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(45, 46, new Enchantment("Glitterdust", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(47, 47, new Enchantment("Gust of Wind", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(48, 49, new Enchantment("Hypnotic Pattern", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(50, 52, new Enchantment("Invisibility", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(53, 55, new Enchantment("Knock", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(56, 56, new Enchantment("Phantom Trap", 200, "gp")));
            enchantment_list.Add(new EnchantmentType(57, 58, new Enchantment("Levitate", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(59, 59, new Enchantment("Locate Object", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(60, 60, new Enchantment("Magic Mouth", 160, "gp")));
            enchantment_list.Add(new EnchantmentType(61, 62, new Enchantment("Acid Arrow", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(63, 63, new Enchantment("Minor Image", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(64, 65, new Enchantment("Mirror Image", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(66, 66, new Enchantment("Misdirection", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(67, 67, new Enchantment("Obscure Object", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(68, 70, new Enchantment("Owl's Wisdom", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(71, 73, new Enchantment("Protection from Arrows", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(74, 75, new Enchantment("Pyrotechnics", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(76, 78, new Enchantment("Resist Energy", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(79, 79, new Enchantment("Rope Trick", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(80, 80, new Enchantment("Scare", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(81, 82, new Enchantment("Scorching Ray", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(83, 85, new Enchantment("See Invisibility", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(86, 86, new Enchantment("Shatter", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(87, 87, new Enchantment("Silence", 200, "gp")));
            enchantment_list.Add(new EnchantmentType(88, 88, new Enchantment("Sound Burst", 200, "gp")));
            enchantment_list.Add(new EnchantmentType(89, 89, new Enchantment("Spectral Hand", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(90, 91, new Enchantment("Spider Climb", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(92, 93, new Enchantment("Summon Monster II", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(94, 95, new Enchantment("Summon Swarm", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(96, 96, new Enchantment("Hideous Laughter", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(97, 97, new Enchantment("Touch of Idiocy", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(98, 99, new Enchantment("Web", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(100, 100, new Enchantment("Whispering Wind", 150, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel2ArcaneSpellList()
        {
            return SetupLevel2ArcaneSpellList();
        }

        private static List<EnchantmentType> SetupLevel3ArcaneSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Arcane Sight", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(3, 4, new Enchantment("Blink", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(5, 6, new Enchantment("Clairaudience/Clairvoyance", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(7, 7, new Enchantment("Cure Serious Wounds", 525, "gp")));
            enchantment_list.Add(new EnchantmentType(8, 10, new Enchantment("Daylight", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(11, 12, new Enchantment("Deep Slumber", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(13, 15, new Enchantment("Dispel Magic", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(16, 17, new Enchantment("Displacement", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(18, 18, new Enchantment("Explosive Runes", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(19, 20, new Enchantment("Fireball", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(21, 22, new Enchantment("Flame Arrow", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(23, 25, new Enchantment("Fly", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(26, 27, new Enchantment("Gaseous Form", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(28, 29, new Enchantment("Gentle Repose", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(30, 30, new Enchantment("Glibness", 525, "gp")));
            enchantment_list.Add(new EnchantmentType(31, 31, new Enchantment("Good Hope", 525, "gp")));
            enchantment_list.Add(new EnchantmentType(32, 33, new Enchantment("Halt Undead", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(34, 36, new Enchantment("Haste", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(37, 38, new Enchantment("Heroism", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(39, 40, new Enchantment("Hold Person", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(41, 41, new Enchantment("Illusory Script", 425, "gp")));
            enchantment_list.Add(new EnchantmentType(42, 44, new Enchantment("Invisibility Sphere", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(45, 47, new Enchantment("Keen Edge", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(48, 49, new Enchantment("Tiny Hut", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(50, 51, new Enchantment("Lightning Bolt", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(52, 59, new Enchantment("Magic Circle Against Alignment", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(60, 62, new Enchantment("Greater Magic Weapon", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(63, 64, new Enchantment("Major Image", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(65, 66, new Enchantment("Nondetection", 425, "gp")));
            enchantment_list.Add(new EnchantmentType(67, 68, new Enchantment("Phantom Steed", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(69, 71, new Enchantment("Protection from Energy", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(72, 73, new Enchantment("Rage", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(74, 75, new Enchantment("Ray of Exhaustion", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(76, 76, new Enchantment("Sculpt Sound", 525, "gp")));
            enchantment_list.Add(new EnchantmentType(77, 77, new Enchantment("Secret Passage", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(78, 78, new Enchantment("Sepia Snake Sigil", 875, "gp")));
            enchantment_list.Add(new EnchantmentType(79, 79, new Enchantment("Shrink Item", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(80, 81, new Enchantment("Sleet Storm", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(82, 83, new Enchantment("Slow", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(84, 84, new Enchantment("Speak With Animals", 525, "gp")));
            enchantment_list.Add(new EnchantmentType(85, 86, new Enchantment("Stinking Cloud", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(87, 88, new Enchantment("Suggestion", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(89, 90, new Enchantment("Summon Monster III", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(91, 93, new Enchantment("Tongues", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(94, 95, new Enchantment("Vampiric Touch", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(96, 98, new Enchantment("Water Breathing", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(99, 100, new Enchantment("Wind Wall", 375, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel3ArcaneSpellList()
        {
            return SetupLevel3ArcaneSpellList();
        }

        private static List<EnchantmentType> SetupLevel4ArcaneSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Animate Dead", 1050, "gp")));
            enchantment_list.Add(new EnchantmentType(3, 5, new Enchantment("Arcane Eye", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(6, 7, new Enchantment("Bestow Curse", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(8, 10, new Enchantment("Charm Monster", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(11, 13, new Enchantment("Confusion", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(14, 15, new Enchantment("Contagion", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(16, 17, new Enchantment("Crushing Despair", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(18, 18, new Enchantment("Cure Critical Wounds", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(19, 19, new Enchantment("Detect Scrying", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(20, 23, new Enchantment("Dimension Door", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(24, 26, new Enchantment("Dimensional Anchor", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(27, 28, new Enchantment("Enervation", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(29, 30, new Enchantment("Mass Enlarge Person", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(31, 32, new Enchantment("Black Tentacles", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(33, 34, new Enchantment("Fear", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(35, 37, new Enchantment("Fire Shield", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(38, 39, new Enchantment("Fire Trap", 725, "gp")));
            enchantment_list.Add(new EnchantmentType(40, 42, new Enchantment("Freedom of Movement", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(43, 43, new Enchantment("Lesser Geas", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(44, 46, new Enchantment("Lesser Globe of Invulnerability", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(47, 48, new Enchantment("Hallucinatory Terrain", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(49, 50, new Enchantment("Ice Storm", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(51, 52, new Enchantment("Illusory Wall", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(53, 55, new Enchantment("Greater Invisibility", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(56, 57, new Enchantment("Secure Shelter", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(58, 58, new Enchantment("Locate Creature", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(59, 60, new Enchantment("Minor Creation", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(61, 61, new Enchantment("Modify Memory", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(62, 62, new Enchantment("Neutralize Poison", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(63, 64, new Enchantment("Resilient Sphere", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(65, 66, new Enchantment("Phantasmal Killer", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(67, 68, new Enchantment("Polymorph", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(69, 70, new Enchantment("Rainbow Pattern", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(71, 71, new Enchantment("Mnemonic Enhancer", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(72, 73, new Enchantment("Mass Reduce Person", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(74, 76, new Enchantment("Remove Curse", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(77, 77, new Enchantment("Repel Vermin", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(78, 79, new Enchantment("Scrying", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(80, 81, new Enchantment("Shadow Conjuration", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(82, 83, new Enchantment("Shout", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(84, 85, new Enchantment("Solid Fog", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(86, 86, new Enchantment("Speak with Plants", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(87, 88, new Enchantment("Stone Shape", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(89, 91, new Enchantment("Stoneskin", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(92, 93, new Enchantment("Summon Monster IV", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(94, 96, new Enchantment("Wall of Fire", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(97, 99, new Enchantment("Wall of Ice", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(100, 100, new Enchantment("Zone of Silence", 700, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel4ArcaneSpellList()
        {
            return SetupLevel4ArcaneSpellList();
        }

        private static List<EnchantmentType> SetupLevel5ArcaneSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Animal Growth", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(3, 5, new Enchantment("Baleful Polymorph", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(6, 7, new Enchantment("Interposing Hand", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(8, 9, new Enchantment("Blight", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(10, 12, new Enchantment("Break Enchantment", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(13, 14, new Enchantment("Cloudkill", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(15, 17, new Enchantment("Cone of Cold", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(18, 19, new Enchantment("Contact Other Plane", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(20, 20, new Enchantment("Mass Cure Light Wounds", 1625, "gp")));
            enchantment_list.Add(new EnchantmentType(21, 23, new Enchantment("Dismissal", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(24, 26, new Enchantment("Greater Dispel Magic", 1625, "gp")));
            enchantment_list.Add(new EnchantmentType(27, 28, new Enchantment("Dominate Person", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(29, 29, new Enchantment("Dream", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(30, 31, new Enchantment("Fabricate", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(32, 33, new Enchantment("False Vision", 1375, "gp")));
            enchantment_list.Add(new EnchantmentType(34, 35, new Enchantment("Feeblemind", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(36, 39, new Enchantment("Hold Monster", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(40, 40, new Enchantment("Secret Chest", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(41, 41, new Enchantment("Magic Jar", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(42, 43, new Enchantment("Major Creation", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(44, 45, new Enchantment("Mind Fog", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(46, 47, new Enchantment("Mirage Arcana", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(48, 49, new Enchantment("Mage's Faithful Hound", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(50, 51, new Enchantment("Mage's Private Sanctum", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(52, 53, new Enchantment("Nightmare", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(54, 57, new Enchantment("Overland Flight", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(58, 60, new Enchantment("Passwall", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(61, 61, new Enchantment("Permenancy", 10125, "gp")));
            enchantment_list.Add(new EnchantmentType(62, 63, new Enchantment("Persistent Image", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(64, 65, new Enchantment("Lesser Planar Binding", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(66, 67, new Enchantment("Prying Eyes", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(68, 69, new Enchantment("Telepathic Bond", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(70, 71, new Enchantment("Seeming", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(72, 74, new Enchantment("Sending", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(75, 76, new Enchantment("Shadow Evocation", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(77, 77, new Enchantment("Song of Discord", 1625, "gp")));
            enchantment_list.Add(new EnchantmentType(78, 79, new Enchantment("Summon Monster V", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(80, 80, new Enchantment("Symbol of Pain", 2125, "gp")));
            enchantment_list.Add(new EnchantmentType(81, 81, new Enchantment("Symbol of Sleep", 2125, "gp")));
            enchantment_list.Add(new EnchantmentType(82, 83, new Enchantment("Telekinesis", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(84, 88, new Enchantment("Teleport", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(89, 90, new Enchantment("Transmute Mud to Rock", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(91, 92, new Enchantment("Transmute Rock to Mud", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(93, 95, new Enchantment("Wall of Force", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(96, 98, new Enchantment("Wall of Stone", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(99, 100, new Enchantment("Waves of Fatigue", 1125, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel5ArcaneSpellList()
        {
            return SetupLevel5ArcaneSpellList();
        }

        private static List<EnchantmentType> SetupLevel6ArcaneSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Acid Fog", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(3, 5, new Enchantment("Analyze Dweomer", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(6, 6, new Enchantment("Animate Objects", 2400, "gp")));
            enchantment_list.Add(new EnchantmentType(7, 9, new Enchantment("Antimagic Field", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(10, 12, new Enchantment("Mass Bear's Endurance", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(13, 14, new Enchantment("Forceful Hand", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(15, 17, new Enchantment("Mass Bull's Strength", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(18, 20, new Enchantment("Mass Cat's Grace", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(21, 23, new Enchantment("Chain Lightning", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(24, 25, new Enchantment("Circle of Death", 2150, "gp")));
            enchantment_list.Add(new EnchantmentType(26, 26, new Enchantment("Contingency", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(27, 28, new Enchantment("Control Weather", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(29, 29, new Enchantment("Create Undead", 2350, "gp")));
            enchantment_list.Add(new EnchantmentType(30, 30, new Enchantment("Mass Cure Moderate Wounds", 2400, "gp")));
            enchantment_list.Add(new EnchantmentType(31, 33, new Enchantment("Disintegrate", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(34, 37, new Enchantment("Greater Dispel Magic", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(38, 40, new Enchantment("Mass Eagle's Splendor", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(41, 42, new Enchantment("Eyebite", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(43, 43, new Enchantment("Find the Path", 2400, "gp")));
            enchantment_list.Add(new EnchantmentType(44, 45, new Enchantment("Flesh to Stone", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(46, 48, new Enchantment("Mass Fox's Cunning", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(49, 49, new Enchantment("Geas/Quest", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(50, 52, new Enchantment("Globe of Invulnerability", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(53, 53, new Enchantment("Guards and Wards", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(54, 54, new Enchantment("Heroe's Feast", 2400, "gp")));
            enchantment_list.Add(new EnchantmentType(55, 56, new Enchantment("Greater Heroism", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(57, 57, new Enchantment("Legend Lore", 1900, "gp")));
            enchantment_list.Add(new EnchantmentType(58, 59, new Enchantment("Mislead", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(60, 60, new Enchantment("Mage's Lucubration", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(61, 62, new Enchantment("Move Earth", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(63, 64, new Enchantment("Freezing Sphere", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(65, 67, new Enchantment("Mass Owl's Wisdom", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(68, 69, new Enchantment("Permanent Image", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(70, 71, new Enchantment("Planar Binding", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(72, 73, new Enchantment("Programmed Image", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(74, 75, new Enchantment("Repulsion", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(76, 78, new Enchantment("Shadow Walk", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(79, 81, new Enchantment("Stone to Flesh", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(82, 83, new Enchantment("Mass Suggestion", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(84, 85, new Enchantment("Summon Monster VI", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(86, 86, new Enchantment("Symbol of Fear", 2650, "gp")));
            enchantment_list.Add(new EnchantmentType(87, 87, new Enchantment("Symbol of Persuasion", 6650, "gp")));
            enchantment_list.Add(new EnchantmentType(88, 88, new Enchantment("Sympathetic Vibration", 2400, "gp")));
            enchantment_list.Add(new EnchantmentType(89, 90, new Enchantment("Transformation", 1950, "gp")));
            enchantment_list.Add(new EnchantmentType(91, 93, new Enchantment("True Seeing", 1950, "gp")));
            enchantment_list.Add(new EnchantmentType(94, 95, new Enchantment("Undeath to Death", 2150, "gp")));
            enchantment_list.Add(new EnchantmentType(96, 97, new Enchantment("Veil", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(98, 100, new Enchantment("Wall of Iron", 1700, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel6ArcaneSpellList()
        {
            return SetupLevel6ArcaneSpellList();
        }

        private static List<EnchantmentType> SetupLevel7ArcaneSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Greater Arcane Sight", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(4, 7, new Enchantment("Banishment", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(8, 10, new Enchantment("Grasping Hand", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(11, 13, new Enchantment("Control Undead", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(14, 16, new Enchantment("Control Weather", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(17, 19, new Enchantment("Delayed Blast Fireball", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(20, 21, new Enchantment("Instant Summons", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(22, 25, new Enchantment("Ethereal Jaunt", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(26, 28, new Enchantment("Finger of Death", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(29, 31, new Enchantment("Forcecage", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(32, 35, new Enchantment("Mass Hold Person", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(36, 38, new Enchantment("Insanity", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(39, 42, new Enchantment("Mass Invisibility", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(43, 43, new Enchantment("Limited Wish", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(44, 45, new Enchantment("Mage's Magnificent Mansion", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(46, 48, new Enchantment("Mage's Sword", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(49, 51, new Enchantment("Phase Door", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(52, 54, new Enchantment("Plane Shift", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(55, 57, new Enchantment("Power Word Blind", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(58, 61, new Enchantment("Prismatic Spray", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(62, 64, new Enchantment("Project Image", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(65, 67, new Enchantment("Reverse Gravity", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(68, 70, new Enchantment("Greater Scrying", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(71, 73, new Enchantment("Sequester", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(74, 76, new Enchantment("Greater Shadow Conjuration", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(77, 77, new Enchantment("Simulacrum", 7275, "gp")));
            enchantment_list.Add(new EnchantmentType(78, 80, new Enchantment("Spell Tuning", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(81, 82, new Enchantment("Statue", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(83, 85, new Enchantment("Summon Monster VII", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(86, 86, new Enchantment("Symbol of Stunning", 7275, "gp")));
            enchantment_list.Add(new EnchantmentType(87, 87, new Enchantment("Symbol of Weakness", 7275, "gp")));
            enchantment_list.Add(new EnchantmentType(88, 90, new Enchantment("Teleport Object", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(91, 95, new Enchantment("Greater Teleport", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(96, 97, new Enchantment("Vision", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(98, 100, new Enchantment("Waves of Exhaustion", 2275, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel7ArcaneSpellList()
        {
            return SetupLevel7ArcaneSpellList();
        }

        private static List<EnchantmentType> SetupLevel8ArcaneSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Antipathy", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Clenched Fist", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Binding", 8500, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Mass Charm Monster", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Clone", 4000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Create Greater Undead", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Demand", 3600, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Dimensional Lock", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Discern Location", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Horrid Wilting", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Incendiary Cloud", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Iron Body", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Maze", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Mind Blank", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Moment of Prescience", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Telekinetic Sphere", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Irresistable Dance", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Greater Planar Binding", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Polar Ray", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Polymorph Any Object", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Power Word Stun", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Prismatic Wall", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Protection from Spells", 3500, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Greater Prying Eyes", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Scintillating Pattern", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Screen", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Greater Shadow Evocation", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Greater Shout", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Summon Monster VIII", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Sunburst", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Symbol of Death", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Symbol of Insanity", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Sympathy", 4500, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Temporal Stasis", 3500, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Trap the Soul", 13000, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel8ArcaneSpellList()
        {
            return SetupLevel8ArcaneSpellList();
        }

        private static List<EnchantmentType> SetupLevel9ArcaneSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Astral Projection", 4870, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Crushing Hand", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Dominate Monster", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Energy Drain", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Etherealness", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Foresight", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Freedom", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Gate", 8825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Mass Hold Monster", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Imprisonment", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Meteor Swarm", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Mage's Disjunction", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Power Word Kill", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Prismatic Sphere", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Refuge", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Shades", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Shapechange", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Soul Bind", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Summon Monster IX", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Teleportation Circle", 4825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Time Stop", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Wail of the Banshee", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Weird", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Wish", 28825, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel9ArcaneSpellList()
        {
            return SetupLevel9ArcaneSpellList();
        }

        private static List<EnchantmentType> SetupLevel0DivineSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 7, new Enchantment("Create Water", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(8, 14, new Enchantment("Cure Minor Wounds", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(15, 22, new Enchantment("Detect Magic", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(23, 29, new Enchantment("Detect Poison", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(30, 36, new Enchantment("Flare", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(37, 43, new Enchantment("Guidance", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(44, 50, new Enchantment("Inflict Minor Wounds", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(51, 57, new Enchantment("Know Direction", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(58, 65, new Enchantment("Light", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(66, 72, new Enchantment("Mending", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(73, 79, new Enchantment("Purify Food and Drink", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(80, 86, new Enchantment("Read Magic", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(87, 93, new Enchantment("Resistance", 12, "gp")));
            enchantment_list.Add(new EnchantmentType(94, 100, new Enchantment("Virtue", 12, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel0DivineSpellList()
        {
            return SetupLevel0DivineSpellList();
        }

        private static List<EnchantmentType> SetupLevel1DivineSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 1, new Enchantment("Alarm", 100, "gp")));
            enchantment_list.Add(new EnchantmentType(2, 3, new Enchantment("Bane", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(4, 6, new Enchantment("Bless", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(7, 9, new Enchantment("Bless Water", 50, "gp")));
            enchantment_list.Add(new EnchantmentType(10, 10, new Enchantment("Bless Weapon", 100, "gp")));
            enchantment_list.Add(new EnchantmentType(11, 12, new Enchantment("Calm Animals", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(13, 14, new Enchantment("Cause Fear", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(15, 16, new Enchantment("Charm Animal", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(17, 19, new Enchantment("Command", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(20, 21, new Enchantment("Comprehend Languages", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(22, 26, new Enchantment("Cure Light Wounds", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(27, 28, new Enchantment("Curse Water", 50, "gp")));
            enchantment_list.Add(new EnchantmentType(29, 30, new Enchantment("Deathwatch", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(31, 32, new Enchantment("Detect Animals or Plants", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(33, 35, new Enchantment("Detect Alignment", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(36, 37, new Enchantment("Detect Snares and Pits", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(38, 39, new Enchantment("Detect Undead", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(40, 41, new Enchantment("Divine Favour", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(42, 43, new Enchantment("Doom", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(44, 48, new Enchantment("Endure Elements", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(49, 50, new Enchantment("Entangle", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(51, 52, new Enchantment("Entropic Shield", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(53, 54, new Enchantment("Faerie Fire", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(55, 56, new Enchantment("Goodberry", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(57, 58, new Enchantment("Hide from Animals", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(59, 60, new Enchantment("Hide from Undead", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(61, 62, new Enchantment("Inflict Light Wounds", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(63, 64, new Enchantment("Jump", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(65, 66, new Enchantment("Longstrider", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(67, 68, new Enchantment("Magic Fang", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(69, 72, new Enchantment("Magic Stone", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(73, 74, new Enchantment("Magic Weapon", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(75, 78, new Enchantment("Obscuring Mist", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(79, 80, new Enchantment("Pass without Trace", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(81, 82, new Enchantment("Produce Flame", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(83, 86, new Enchantment("Protection from Alignment", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(87, 88, new Enchantment("Remove Fear", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(89, 90, new Enchantment("Sanctuary", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(91, 92, new Enchantment("Shield of Faith", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(93, 94, new Enchantment("Shillelagh", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(95, 96, new Enchantment("Speak with Animals", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(97, 98, new Enchantment("Summon Monster I", 25, "gp")));
            enchantment_list.Add(new EnchantmentType(99, 100, new Enchantment("Summon Nature's Ally I", 25, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel1DivineSpellList()
        {
            return SetupLevel1DivineSpellList();
        }

        private static List<EnchantmentType> SetupLevel2DivineSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 1, new Enchantment("Animal Messenger", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(2, 2, new Enchantment("Animal Trance", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(3, 4, new Enchantment("Augury", 175, "gp")));
            enchantment_list.Add(new EnchantmentType(5, 6, new Enchantment("Barkskin", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(7, 9, new Enchantment("Bear's Endurance", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(10, 12, new Enchantment("Bull's Strength", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(13, 14, new Enchantment("Calm Emotions", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(15, 17, new Enchantment("Cat's Grace", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(18, 18, new Enchantment("Chill Metal", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(19, 20, new Enchantment("Consecrate", 200, "gp")));
            enchantment_list.Add(new EnchantmentType(21, 24, new Enchantment("Cure Moderate Wounds", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(25, 26, new Enchantment("Darkness", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(27, 27, new Enchantment("Death Knell", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(28, 30, new Enchantment("Delay Poison", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(31, 32, new Enchantment("Desecrate", 200, "gp")));
            enchantment_list.Add(new EnchantmentType(33, 35, new Enchantment("Eagle's Splendor", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(36, 37, new Enchantment("Enthrall", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(38, 39, new Enchantment("Find Traps", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(40, 40, new Enchantment("Fire Trap", 175, "gp")));
            enchantment_list.Add(new EnchantmentType(41, 42, new Enchantment("Flame Blade", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(43, 44, new Enchantment("Flaming Sphere", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(45, 46, new Enchantment("Fog Cloud", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(47, 47, new Enchantment("Gentle Repose", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(48, 48, new Enchantment("Gust of Wind", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(49, 49, new Enchantment("Heat Metal", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(50, 51, new Enchantment("Hold Animal", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(52, 54, new Enchantment("Hold Person", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(55, 56, new Enchantment("Inflict Moderate Wounds", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(57, 58, new Enchantment("Make Whole", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(59, 61, new Enchantment("Owl's Wisdom", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(62, 62, new Enchantment("Reduce Animal", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(63, 64, new Enchantment("Remove Paralysis", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(65, 67, new Enchantment("Resist Energy", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(68, 70, new Enchantment("Lesser Restoration", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(71, 72, new Enchantment("Shatter", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(73, 74, new Enchantment("Shield Other", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(75, 76, new Enchantment("Silence", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(77, 77, new Enchantment("Snare", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(78, 78, new Enchantment("Soften Earth and Stone", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(79, 80, new Enchantment("Sound Burst", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(81, 81, new Enchantment("Speak with Plants", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(82, 83, new Enchantment("Spider Climb", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(84, 85, new Enchantment("Spiritual Weapon", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(86, 86, new Enchantment("Status", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(87, 88, new Enchantment("Summon Monster II", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(89, 90, new Enchantment("Summon Nature's Ally II", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(91, 92, new Enchantment("Summon Swarm", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(93, 93, new Enchantment("Tree Shape", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(94, 95, new Enchantment("Undetectable Alignment", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(96, 97, new Enchantment("Warp Wood", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(98, 98, new Enchantment("Wood Shape", 150, "gp")));
            enchantment_list.Add(new EnchantmentType(99, 100, new Enchantment("Zone of Truth", 150, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel2DivineSpellList()
        {
            return SetupLevel2DivineSpellList();
        }

        private static List<EnchantmentType> SetupLevel3DivineSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 2, new Enchantment("Animate Dead", 625, "gp")));
            enchantment_list.Add(new EnchantmentType(3, 4, new Enchantment("Bestow Curse", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(5, 6, new Enchantment("Blindness/Deafness", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(7, 8, new Enchantment("Call Lightning", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(9, 10, new Enchantment("Contagion", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(11, 12, new Enchantment("Continual Flame", 425, "gp")));
            enchantment_list.Add(new EnchantmentType(13, 14, new Enchantment("Create Food and Water", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(15, 18, new Enchantment("Cure Serious Wounds", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(19, 19, new Enchantment("Darkvision", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(20, 21, new Enchantment("Daylight", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(22, 23, new Enchantment("Deeper Darknes", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(24, 25, new Enchantment("Diminish Plants", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(26, 27, new Enchantment("Dispel Magic", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(28, 29, new Enchantment("Dominate Animal", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(30, 31, new Enchantment("Glyph of Warding", 575, "gp")));
            enchantment_list.Add(new EnchantmentType(32, 32, new Enchantment("Heal Mount", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(33, 34, new Enchantment("Helping Hand", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(35, 36, new Enchantment("Inflict Serious Wounds", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(37, 38, new Enchantment("Invisibility Purge", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(39, 40, new Enchantment("Locate Object", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(41, 46, new Enchantment("Magic Circle Against Alignment", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(47, 48, new Enchantment("Greater Magic Fang", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(49, 50, new Enchantment("Magic Vestment", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(51, 52, new Enchantment("Meld Into Stone", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(53, 55, new Enchantment("Neutralize Poison", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(56, 57, new Enchantment("Obscure Object", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(58, 59, new Enchantment("Plant Growth", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(60, 62, new Enchantment("Prayer", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(63, 64, new Enchantment("Protection from Energy", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(65, 66, new Enchantment("Quench", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(67, 69, new Enchantment("Remove Blindness/Deafness", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(70, 71, new Enchantment("Remove Curse", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(72, 73, new Enchantment("Remove Disease", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(74, 76, new Enchantment("Searing Light", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(77, 78, new Enchantment("Sleet Storm", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(79, 80, new Enchantment("Snare", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(81, 83, new Enchantment("Speak with Dead", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(84, 85, new Enchantment("Speak with Plants", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(86, 87, new Enchantment("Spike Growth", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(88, 89, new Enchantment("Stone Shape", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(90, 91, new Enchantment("Summon Monster III", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(92, 93, new Enchantment("Summon Nature's Ally III", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(94, 96, new Enchantment("Water Breathing", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(97, 98, new Enchantment("Water Walk", 375, "gp")));
            enchantment_list.Add(new EnchantmentType(99, 100, new Enchantment("Wind Wall", 375, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel3DivineSpellList()
        {
            return SetupLevel3DivineSpellList();
        }

        private static List<EnchantmentType> SetupLevel4DivineSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 5, new Enchantment("Air Walk", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(6, 7, new Enchantment("Antiplant Shell", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(8, 9, new Enchantment("Blight", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(10, 11, new Enchantment("Break Enchantment", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(12, 13, new Enchantment("Command Plants", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(14, 15, new Enchantment("Control Water", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(16, 21, new Enchantment("Cure Critical Wounds", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(22, 26, new Enchantment("Death Ward", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(27, 31, new Enchantment("Dimensional Anchor", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(32, 34, new Enchantment("Discern Lies", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(35, 37, new Enchantment("Dismissal", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(38, 39, new Enchantment("Divination", 725, "gp")));
            enchantment_list.Add(new EnchantmentType(40, 42, new Enchantment("Divine Power", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(43, 47, new Enchantment("Freedom of Movement", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(48, 49, new Enchantment("Giant Vermin", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(50, 51, new Enchantment("Holy Sword", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(52, 54, new Enchantment("Imbue with Spell Ability", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(55, 57, new Enchantment("Inflict Critical Wounds", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(58, 60, new Enchantment("Greater Magic Weapon", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(61, 62, new Enchantment("Nondetection", 750, "gp")));
            enchantment_list.Add(new EnchantmentType(63, 64, new Enchantment("Lesser Planar Ally", 1200, "gp")));
            enchantment_list.Add(new EnchantmentType(65, 67, new Enchantment("Poison", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(68, 69, new Enchantment("Reincarnate", 1700, "gp")));
            enchantment_list.Add(new EnchantmentType(70, 71, new Enchantment("Repel Vermin", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(72, 76, new Enchantment("Restoration", 800, "gp")));
            enchantment_list.Add(new EnchantmentType(77, 78, new Enchantment("Rusting Grasp", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(79, 81, new Enchantment("Sending", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(82, 85, new Enchantment("Spell Immunity", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(86, 87, new Enchantment("Spike Stones", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(88, 90, new Enchantment("Summon Monster IV", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(91, 93, new Enchantment("Summon Nature's Ally IV", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(94, 98, new Enchantment("Tongues", 700, "gp")));
            enchantment_list.Add(new EnchantmentType(99, 100, new Enchantment("Tree Stride", 700, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel4DivineSpellList()
        {
            return SetupLevel4DivineSpellList();
        }

        private static List<EnchantmentType> SetupLevel5DivineSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Animal Growth", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(4, 5, new Enchantment("Atonement", 3625, "gp")));
            enchantment_list.Add(new EnchantmentType(6, 6, new Enchantment("Awaken", 2375, "gp")));
            enchantment_list.Add(new EnchantmentType(7, 9, new Enchantment("Baleful Polymorph", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(10, 13, new Enchantment("Break Enchantment", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(14, 16, new Enchantment("Call Lightning Storm", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(17, 20, new Enchantment("Greater Command", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(21, 21, new Enchantment("Commune", 1625, "gp")));
            enchantment_list.Add(new EnchantmentType(22, 22, new Enchantment("Commune with Nature", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(23, 24, new Enchantment("Control Winds", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(25, 30, new Enchantment("Mass Cure Light Wounds", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(31, 34, new Enchantment("Dispel Alignment", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(35, 38, new Enchantment("Disrupting Weapon", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(39, 41, new Enchantment("Flame Strike", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(42, 43, new Enchantment("Hallow", 6125, "gp")));
            enchantment_list.Add(new EnchantmentType(44, 46, new Enchantment("Ice Storm", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(47, 49, new Enchantment("Mass Inflict Light Wounds", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(50, 52, new Enchantment("Insect Plague", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(53, 53, new Enchantment("Mark of Justice", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(54, 56, new Enchantment("Plane Shift", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(57, 58, new Enchantment("Raise Dead", 6125, "gp")));
            enchantment_list.Add(new EnchantmentType(59, 60, new Enchantment("Righteous Might", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(61, 63, new Enchantment("Scrying", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(64, 66, new Enchantment("Slay Living", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(67, 69, new Enchantment("Spell Resistance", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(70, 71, new Enchantment("Stoneskin", 1375, "gp")));
            enchantment_list.Add(new EnchantmentType(72, 74, new Enchantment("Summon Monster V", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(75, 77, new Enchantment("Summon Nature's Ally V", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(78, 78, new Enchantment("Symbol of Pain", 2125, "gp")));
            enchantment_list.Add(new EnchantmentType(79, 79, new Enchantment("Symbol of Sleep", 2125, "gp")));
            enchantment_list.Add(new EnchantmentType(80, 82, new Enchantment("Transmute Mud to Rock", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(83, 85, new Enchantment("Transmute Rock to Mud", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(86, 89, new Enchantment("True Seeing", 1375, "gp")));
            enchantment_list.Add(new EnchantmentType(90, 91, new Enchantment("Unhallow", 6125, "gp")));
            enchantment_list.Add(new EnchantmentType(92, 94, new Enchantment("Wall of Fire", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(95, 97, new Enchantment("Wall of Stone", 1125, "gp")));
            enchantment_list.Add(new EnchantmentType(98, 100, new Enchantment("Wall of Thorns", 1125, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel5DivineSpellList()
        {
            return SetupLevel5DivineSpellList();
        }

        private static List<EnchantmentType> SetupLevel6DivineSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Animate Objects", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(4, 6, new Enchantment("Antilife Shell", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(7, 9, new Enchantment("Banishment", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(10, 13, new Enchantment("Mass Bear's Endurance", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(14, 16, new Enchantment("Blade Barrier", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(17, 20, new Enchantment("Mass Bull's Strength", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(21, 24, new Enchantment("Mass Cat's Grace", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(25, 25, new Enchantment("Create Undead", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(26, 29, new Enchantment("Mass Cure Moderate Wounds", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(30, 33, new Enchantment("Greater Dispel Magic", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(34, 37, new Enchantment("Mass Eagle's Splendor", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(38, 40, new Enchantment("Find the Path", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(41, 43, new Enchantment("Fire Seeds", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(44, 44, new Enchantment("Forbiddance", 4650, "gp")));
            enchantment_list.Add(new EnchantmentType(45, 45, new Enchantment("Geas/Quest", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(46, 46, new Enchantment("Greater Glyph of Warding", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(47, 49, new Enchantment("Harm", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(50, 52, new Enchantment("Heal", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(53, 55, new Enchantment("Heroes' Feast", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(56, 58, new Enchantment("Mass Inflict Moderate Wounds", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(59, 61, new Enchantment("Ironwood", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(62, 62, new Enchantment("Liveoak", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(63, 65, new Enchantment("Move Earth", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(66, 69, new Enchantment("Mass Owl's Wisdom", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(70, 71, new Enchantment("Planar Ally", 2400, "gp")));
            enchantment_list.Add(new EnchantmentType(72, 74, new Enchantment("Repel Wood", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(75, 77, new Enchantment("Spellstaff", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(78, 80, new Enchantment("Stone Tell", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(81, 83, new Enchantment("Summon Monster VI", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(84, 86, new Enchantment("Summon Nature's Ally VI", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(87, 87, new Enchantment("Symbol of Fear", 2650, "gp")));
            enchantment_list.Add(new EnchantmentType(88, 88, new Enchantment("Symbol of Persuasion", 6650, "gp")));
            enchantment_list.Add(new EnchantmentType(89, 91, new Enchantment("Transport Via Plants", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(92, 94, new Enchantment("Undeath to Death", 2150, "gp")));
            enchantment_list.Add(new EnchantmentType(95, 97, new Enchantment("Wind Walk", 1650, "gp")));
            enchantment_list.Add(new EnchantmentType(98, 100, new Enchantment("Word of Recall", 1650, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel6DivineSpellList()
        {
            return SetupLevel6DivineSpellList();
        }

        private static List<EnchantmentType> SetupLevel7DivineSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 5, new Enchantment("Animate Plants", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(6, 9, new Enchantment("Blasphemy", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(10, 14, new Enchantment("Changestaff", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(15, 16, new Enchantment("Control Weather", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(17, 21, new Enchantment("Creeping Doom", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(22, 27, new Enchantment("Mass Cure Serious Wounds", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(28, 32, new Enchantment("Destruction", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(33, 36, new Enchantment("Dictum", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(37, 41, new Enchantment("Ethereal Jaunt", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(42, 45, new Enchantment("Holy Word", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(46, 50, new Enchantment("Mass Inflict Serious Wounds", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(51, 55, new Enchantment("Refuge", 3775, "gp")));
            enchantment_list.Add(new EnchantmentType(56, 60, new Enchantment("Regenerate", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(61, 65, new Enchantment("Repulsion", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(66, 69, new Enchantment("Greater Restoration", 4775, "gp")));
            enchantment_list.Add(new EnchantmentType(70, 71, new Enchantment("Resurrection", 12275, "gp")));
            enchantment_list.Add(new EnchantmentType(72, 76, new Enchantment("Greater Scrying", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(77, 81, new Enchantment("Summon Monster VII", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(82, 85, new Enchantment("Summon Nature's Ally VII", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(86, 90, new Enchantment("Sunbeam", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(91, 91, new Enchantment("Symbol of Stunning", 7275, "gp")));
            enchantment_list.Add(new EnchantmentType(92, 92, new Enchantment("Symbol of Weakness", 7275, "gp")));
            enchantment_list.Add(new EnchantmentType(93, 97, new Enchantment("Transmute Metal to Wood", 2275, "gp")));
            enchantment_list.Add(new EnchantmentType(98, 100, new Enchantment("Word of Chaos", 2275, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel7DivineSpellList()
        {
            return SetupLevel7DivineSpellList();
        }

        private static List<EnchantmentType> SetupLevel8DivineSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 4, new Enchantment("Animal Shapes", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(5, 10, new Enchantment("Antimagic Field", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(11, 13, new Enchantment("Cloak of Chaos", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(14, 17, new Enchantment("Control Plants", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(18, 20, new Enchantment("Create Greater Undead", 3600, "gp")));
            enchantment_list.Add(new EnchantmentType(21, 27, new Enchantment("Mass Cure Critical Wounds", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(28, 32, new Enchantment("Dimensional Lock", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(33, 36, new Enchantment("Discern Location", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(37, 41, new Enchantment("Earthquake", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(42, 45, new Enchantment("Finger of Death", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(46, 49, new Enchantment("Fire Storm", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(50, 52, new Enchantment("Holy Aura", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(53, 56, new Enchantment("Mass Inflict Critical Wounds", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(57, 60, new Enchantment("Greater Planar Ally", 5500, "gp")));
            enchantment_list.Add(new EnchantmentType(61, 65, new Enchantment("Repel Metal or Stone", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(66, 69, new Enchantment("Reverse Gravity", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(70, 72, new Enchantment("Shield of Law", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(73, 76, new Enchantment("Greater Spell Immunity", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(77, 80, new Enchantment("Summon Monster VIII", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(81, 84, new Enchantment("Summon Nature's Ally VIII", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(85, 89, new Enchantment("Sunburst", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(90, 91, new Enchantment("Symbol of Death", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(92, 93, new Enchantment("Symbol of Insanity", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(94, 96, new Enchantment("Unholy Aura", 3000, "gp")));
            enchantment_list.Add(new EnchantmentType(97, 100, new Enchantment("Whirlwind", 3000, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel8DivineSpellList()
        {
            return SetupLevel8DivineSpellList();
        }

        private static List<EnchantmentType> SetupLevel9DivineSpellList()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 4, new Enchantment("Antipathy", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(5, 7, new Enchantment("Astral Projection", 4870, "gp")));
            enchantment_list.Add(new EnchantmentType(8, 13, new Enchantment("Elemental Swarm", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(14, 19, new Enchantment("Energy Drain", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(20, 25, new Enchantment("Etherealness", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(26, 31, new Enchantment("Foresight", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(32, 37, new Enchantment("Gate", 8825, "gp")));
            enchantment_list.Add(new EnchantmentType(38, 46, new Enchantment("Mass Heal", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(47, 53, new Enchantment("Implosion", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(54, 55, new Enchantment("Miracle", 28825, "gp")));
            enchantment_list.Add(new EnchantmentType(56, 61, new Enchantment("Regenerate", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(62, 66, new Enchantment("Shambler", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(67, 72, new Enchantment("Shapechange", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(73, 77, new Enchantment("Soul Bind", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(78, 83, new Enchantment("Storm of Vengeance", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(84, 89, new Enchantment("Summon Monster IX", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(90, 95, new Enchantment("Summon Nature's Ally IX", 3825, "gp")));
            enchantment_list.Add(new EnchantmentType(96, 99, new Enchantment("Sympathy", 5325, "gp")));
            enchantment_list.Add(new EnchantmentType(100, 100, new Enchantment("True Resurrection", 28825, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateLevel9DivineSpellList()
        {
            return SetupLevel9DivineSpellList();
        }

        /* Mundane Items Creation Methods
         * Create: Alchemical, Armour, Shields, Tools, Common and Uncommon Melee, and Ranged Weapons
         */
         
        private static Treasure_Composite SetupMundaneItemTypes()
        {
            var treasure_composite = new Treasure_Composite();
            treasure_composite.AddTreasure(new TreasureType("Mundane Item", 0, 17, CreateAlchemicItemsList()));
            treasure_composite.AddTreasure(new TreasureType("Mundane Item", 18, 50, CreateArmourItemsList()));
            treasure_composite.AddTreasure(new TreasureType("Mundane Item", 51, 83, CreateWeaponItemsList()));
            treasure_composite.AddTreasure(new TreasureType("Mundane Item", 84, 100, CreateToolItemsList()));
            return treasure_composite;
        }

        public static Treasure_Composite CreateMundaneItemsList()
        {
            return SetupMundaneItemTypes();
        }

        private static Treasure_Leaf SetupAlchemicItemsTypes()
        {
            var treasure_leaf = new Treasure_Leaf();
            treasure_leaf.AddTreasure(new ItemType("Alchemical", 0, 12, new CommonItem(20, "gp", "Alchemist's Fire, 1d4 flasks")));
            treasure_leaf.AddTreasure(new ItemType("Alchemical", 13, 24, new CommonItem(10, "gp", "Acid, 2d4 flasks")));
            treasure_leaf.AddTreasure(new ItemType("Alchemical", 25, 36, new CommonItem(20, "gp", "Smokesticks, 1d4 sticks")));
            treasure_leaf.AddTreasure(new ItemType("Alchemical", 37, 48, new CommonItem(25, "gp", "Holy Water, 1d4 flasks")));
            treasure_leaf.AddTreasure(new ItemType("Alchemical", 49, 62, new CommonItem(50, "gp", "Antitoxin, 1d4 doses")));
            treasure_leaf.AddTreasure(new ItemType("Alchemical", 63, 74, new CommonItem(20, "gp", "Everburning Torch")));
            treasure_leaf.AddTreasure(new ItemType("Alchemical", 75, 88, new CommonItem(50, "gp", "Tanglefoot bags, 1d4 bags")));
            treasure_leaf.AddTreasure(new ItemType("Alchemical", 89, 100, new CommonItem(30, "gp", "Thunderstone, 1d4 stones")));
            return treasure_leaf;
        }
        
        public static Treasure_Leaf CreateAlchemicItemsList()
        {
            return SetupAlchemicItemsTypes();
        }

        private static List<ItemType> SetupArmourItemTypes()
        {
            var treasure_leaf = new List<ItemType> ();
            treasure_leaf.Add(new ItemType("Armour", 0, 12, new CommonItem(100, "gp", "Chain Shirt")));
            treasure_leaf.Add(new ItemType("Armour", 13, 18, new CommonItem(175, "gp", "Masterwork Studded Leather")));
            treasure_leaf.Add(new ItemType("Armour", 19, 26, new CommonItem(200, "gp", "Breastplate")));
            treasure_leaf.Add(new ItemType("Armour", 27, 34, new CommonItem(250, "gp", "Banded Mail")));
            treasure_leaf.Add(new ItemType("Armour", 35, 54, new CommonItem(600, "gp", "Half-plate")));
            treasure_leaf.Add(new ItemType("Armour", 55, 80, new CommonItem(1500, "gp", "Full plate")));
            treasure_leaf.Add(new ItemType("Armour", 81, 85, new CommonItem(205, "gp", "Darkwood Buckler")));
            treasure_leaf.Add(new ItemType("Armour", 86, 90, new CommonItem(257, "gp", "Darkwood Shield")));
            treasure_leaf.Add(new ItemType("Armour", 91, 92, new CommonItem(165, "gp", "Masterwork Buckler")));
            treasure_leaf.Add(new ItemType("Armour", 93, 94, new CommonItem(153, "gp", "Masterwork Light Wooden Shield")));
            treasure_leaf.Add(new ItemType("Armour", 95, 96, new CommonItem(159, "gp", "Masterwork Steel Shield")));
            treasure_leaf.Add(new ItemType("Armour", 97, 98, new CommonItem(157, "gp", "Masterwork Heavy Wooden Shield")));
            treasure_leaf.Add(new ItemType("Armour", 99, 100, new CommonItem(170, "gp", "Masterwork Heavy Steel Shield")));
            return treasure_leaf;
        }

        private static List<ItemType> SetupArmourTypeToEnchant()
        {
            var treasure_leaf = new List<ItemType>();
            treasure_leaf.Add(new ItemType("Armour", 0, 1, new CommonItem(155, "gp", "Padded")));
            treasure_leaf.Add(new ItemType("Armour", 2, 2, new CommonItem(160, "gp", "Leather")));
            treasure_leaf.Add(new ItemType("Armour", 3, 17, new CommonItem(175, "gp", "Studded Leather")));
            treasure_leaf.Add(new ItemType("Armour", 18, 32, new CommonItem(250, "gp", "Chain Shirt")));
            treasure_leaf.Add(new ItemType("Armour", 33, 42, new CommonItem(165, "gp", "Hide")));
            treasure_leaf.Add(new ItemType("Armour", 43, 43, new CommonItem(200, "gp", "Scale Mail")));
            treasure_leaf.Add(new ItemType("Armour", 44, 44, new CommonItem(300, "gp", "Chainmail")));
            treasure_leaf.Add(new ItemType("Armour", 45, 57, new CommonItem(350, "gp", "Breastplate")));
            treasure_leaf.Add(new ItemType("Armour", 58, 58, new CommonItem(350, "gp", "Splint Mail")));
            treasure_leaf.Add(new ItemType("Armour", 59, 59, new CommonItem(400, "gp", "Banded Mail")));
            treasure_leaf.Add(new ItemType("Armour", 60, 60, new CommonItem(750, "gp", "Half-Plate")));
            treasure_leaf.Add(new ItemType("Armour", 61, 100, new CommonItem(1650, "gp", "Full Plate")));
            return treasure_leaf;
        }

        private static List<ItemType> SetupShieldTypeToEnchant()
        {
            var treasure_leaf = new List<ItemType>();
            treasure_leaf.Add(new ItemType("Shield", 0, 10, new CommonItem(165, "gp", "Buckler")));
            treasure_leaf.Add(new ItemType("Shield", 11, 15, new CommonItem(153, "gp", "Light Wooden Shield")));
            treasure_leaf.Add(new ItemType("Shield", 16, 20, new CommonItem(159, "gp", "Light Steel Shield")));
            treasure_leaf.Add(new ItemType("Shield", 21, 30, new CommonItem(157, "gp", "Heavy Wooden Shield")));
            treasure_leaf.Add(new ItemType("Shield", 31, 95, new CommonItem(170, "gp", "Heavy Steel Shield")));
            treasure_leaf.Add(new ItemType("Shield", 96, 100, new CommonItem(180, "gp", "Tower Shield")));
            return treasure_leaf;
        }

        public static Treasure_Leaf CreateArmourItemsList()
        {
            var treasure_pile = new Treasure_Leaf();
            treasure_pile.AddTreasure(SetupArmourItemTypes());
            return treasure_pile;
        }

        private static Treasure_Leaf SetupToolItemTypes()
        {
            var treasure_leaf = new Treasure_Leaf();
            treasure_leaf.AddTreasure(new ItemType("Tool", 0, 3, new CommonItem(2, "gp", "Backpack, empty")));
            treasure_leaf.AddTreasure(new ItemType("Tool", 4, 6, new CommonItem(2, "gp", "Crowbar")));
            treasure_leaf.AddTreasure(new ItemType("Tool", 7, 11, new CommonItem(12, "gp", "Lantern, bullseye")));
            treasure_leaf.AddTreasure(new ItemType("Tool", 12, 16, new CommonItem(20, "gp", "Lock, simple")));
            treasure_leaf.AddTreasure(new ItemType("Tool", 17, 21, new CommonItem(40, "gp", "Lock, average")));
            treasure_leaf.AddTreasure(new ItemType("Tool", 22, 28, new CommonItem(80, "gp", "Lock, good")));
            treasure_leaf.AddTreasure(new ItemType("Tool", 29, 35, new CommonItem(150, "gp", "Lock, superior")));
            treasure_leaf.AddTreasure(new ItemType("Tool", 36, 40, new CommonItem(50, "gp", "Manacles, masterwork")));
            treasure_leaf.AddTreasure(new ItemType("Tool", 41, 43, new CommonItem(10, "gp", "Mirror, small steel")));
            treasure_leaf.AddTreasure(new ItemType("Tool", 44, 46, new CommonItem(10, "gp", "Rope, silk (50 ft.)")));
            treasure_leaf.AddTreasure(new ItemType("Tool", 47, 53, new CommonItem(1000, "gp", "Spyglass")));
            treasure_leaf.AddTreasure(new ItemType("Tool", 54, 58, new CommonItem(55, "gp", "Artisan's Tools, masterwork")));
            treasure_leaf.AddTreasure(new ItemType("Tool", 59, 63, new CommonItem(80, "gp", "Climber's Kit")));
            treasure_leaf.AddTreasure(new ItemType("Tool", 64, 68, new CommonItem(50, "gp", "Disguise Kit")));
            treasure_leaf.AddTreasure(new ItemType("Tool", 69, 73, new CommonItem(50, "gp", "Healer's Kit")));
            treasure_leaf.AddTreasure(new ItemType("Tool", 74, 77, new CommonItem(25, "gp", "Holy Symbol, silver")));
            treasure_leaf.AddTreasure(new ItemType("Tool", 78, 81, new CommonItem(25, "gp", "Hourglass")));
            treasure_leaf.AddTreasure(new ItemType("Tool", 82, 88, new CommonItem(100, "gp", "Magnifying Glass")));
            treasure_leaf.AddTreasure(new ItemType("Tool", 89, 95, new CommonItem(100, "gp", "Musical Instrument, masterwork")));
            treasure_leaf.AddTreasure(new ItemType("Tool", 96, 100, new CommonItem(50, "gp", "Thieves' Tools, masterwork")));
            return treasure_leaf;
        }

        public static Treasure_Leaf CreateToolItemsList()
        {
            return SetupToolItemTypes();
        }

        private static List<ItemType> SetupCommonMeleeWeaponItemTypes()
        {
            var weapon_pile = new List<ItemType>();
            weapon_pile.Add(new ItemType("Common Melee", 0, 4, new CommonItem(302, "gp", "Masterwork Dagger")));
            weapon_pile.Add(new ItemType("Common Melee", 5, 14, new CommonItem(320, "gp", "Masterwork Greataxe")));
            weapon_pile.Add(new ItemType("Common Melee", 15, 24, new CommonItem(350, "gp", "Masterwork Greatsword")));
            weapon_pile.Add(new ItemType("Common Melee", 25, 28, new CommonItem(302, "gp", "Masterwork Kama")));
            weapon_pile.Add(new ItemType("Common Melee", 29, 41, new CommonItem(315, "gp", "Masterwork Longsword")));
            weapon_pile.Add(new ItemType("Common Melee", 42, 45, new CommonItem(305, "gp", "Masterwork Mace, light")));
            weapon_pile.Add(new ItemType("Common Melee", 46, 50, new CommonItem(312, "gp", "Masterwork Mave, heavy")));
            weapon_pile.Add(new ItemType("Common Melee", 51, 54, new CommonItem(302, "gp", "Masterwork Nunchaku")));
            weapon_pile.Add(new ItemType("Common Melee", 55, 57, new CommonItem(600, "gp", "Masterwork Quarterstaff")));
            weapon_pile.Add(new ItemType("Common Melee", 58, 61, new CommonItem(320, "gp", "Masterwork Rapier")));
            weapon_pile.Add(new ItemType("Common Melee", 62, 66, new CommonItem(315, "gp", "Masterwork Scimitar")));
            weapon_pile.Add(new ItemType("Common Melee", 67, 70, new CommonItem(302, "gp", "Masterwork Shortspear")));
            weapon_pile.Add(new ItemType("Common Melee", 71, 74, new CommonItem(303, "gp", "Masterwork Siangham")));
            weapon_pile.Add(new ItemType("Common Melee", 75, 84, new CommonItem(335, "gp", "Masterwork Bastard Sword")));
            weapon_pile.Add(new ItemType("Common Melee", 85, 89, new CommonItem(310, "gp", "Masterwork Short Sword")));
            weapon_pile.Add(new ItemType("Common Melee", 90, 100, new CommonItem(330, "gp", "Masterwork Dwarven Waraxe")));
            return weapon_pile;
        }

        public static Treasure_Leaf CreateCommonMeleeWeaponItemsList()
        {
            var treasure_pile = new Treasure_Leaf();
            treasure_pile.AddTreasure(SetupCommonMeleeWeaponItemTypes());
            return treasure_pile;
        }

        private static List<ItemType> SetupUncommonMeleeWeaponItemTypes()
        {
            var weapon_pile = new List<ItemType> ();
            weapon_pile.Add(new ItemType("Uncommon Melee", 0, 3, new CommonItem(660, "gp", "Masterwork Orc Double-Axe")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 4, 7, new CommonItem(310, "gp", "Masterwork Battleaxe")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 8, 10, new CommonItem(325, "gp", "Masterwork Chain, spiked")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 11, 12, new CommonItem(300, "gp", "Masterwork Club")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 13, 16, new CommonItem(400, "gp", "Masterwork Crossbow, hand")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 17, 19, new CommonItem(550, "gp", "Masterwork Crossbow, repeating (heavy, or light)")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 20, 21, new CommonItem(302, "gp", "Masterwork Dagger, punching")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 22, 23, new CommonItem(375, "gp", "Masterwork Falchion")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 24, 26, new CommonItem(690, "gp", "Masterwork Dire Flail")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 27, 31, new CommonItem(315, "gp", "Masterwork Heavy Flail")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 32, 35, new CommonItem(308, "gp", "Masterwork Flail")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 36, 37, new CommonItem(302, "gp", "Masterwork Gauntlet")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 38, 39, new CommonItem(305, "gp", "Masterwork Spiked Gauntlet")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 40, 41, new CommonItem(308, "gp", "Masterwork Glaive")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 42, 43, new CommonItem(305, "gp", "Masterwork Greatclub")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 44, 45, new CommonItem(309, "gp", "Masterwork Guisarme")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 46, 48, new CommonItem(310, "gp", "Masterwork Halberd")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 49, 51, new CommonItem(301, "gp", "Masterwork Spear")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 52, 54, new CommonItem(620, "gp", "Masterwork Hooked Hammer, gnome")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 55, 56, new CommonItem(301, "gp", "Masterwork Light Hammer")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 57, 58, new CommonItem(306, "gp", "Masterwork Handaxe")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 59, 61, new CommonItem(308, "gp", "Masterwork Kukri")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 62, 64, new CommonItem(310, "gp", "Masterwork Lance")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 65, 67, new CommonItem(305, "gp", "Masterwork Longspear")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 68, 70, new CommonItem(308, "gp", "Masterwork Morningstar")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 71, 72, new CommonItem(320, "gp", "Masterwork Net")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 73, 74, new CommonItem(308, "gp", "Masterwork Heavy Pick")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 75, 76, new CommonItem(304, "gp", "Masterwork Light Pick")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 77, 78, new CommonItem(310, "gp", "Masterwork Ranseur")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 79, 80, new CommonItem(301, "gp", "Masterwork Sap")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 81, 82, new CommonItem(318, "gp", "Masterwork Scythe")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 83, 84, new CommonItem(301, "gp", "Masterwork Shuriken")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 85, 86, new CommonItem(306, "gp", "Masterwork Sickle")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 87, 89, new CommonItem(700, "gp", "Masterwork Two-bladed Sword")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 90, 91, new CommonItem(315, "gp", "Masterwork Trident")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 92, 94, new CommonItem(650, "gp", "Masterwork Dwarven Urgrosh")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 95, 97, new CommonItem(312, "gp", "Masterwork Warhammer")));
            weapon_pile.Add(new ItemType("Uncommon Melee", 98, 100, new CommonItem(301, "gp", "Masterwork Whip")));
            return weapon_pile;
        } 

        public static Treasure_Leaf CreateUncommonMeleeWeaponItemsList()
        {
            var treasure_pile = new Treasure_Leaf();
            treasure_pile.AddTreasure(SetupUncommonMeleeWeaponItemTypes());
            return treasure_pile;
        }
        
        private static List<ItemType> SetupRangedWeaponItemTypes()
        {
            var weapon_pile = new List<ItemType> ();
            weapon_pile.Add(new ItemType("Ranged Weapon", 0, 4, new CommonItem(350, "gp", "Masterwork Arrows, bundle of 50")));
            weapon_pile.Add(new ItemType("Ranged Weapon", 5, 7, new CommonItem(350, "gp", "Masterwork Bolts, bundle of 50")));
            weapon_pile.Add(new ItemType("Ranged Weapon", 8, 10, new CommonItem(350, "gp", "Masterwork Bullets, bag of 50")));
            weapon_pile.Add(new ItemType("Ranged Weapon", 11, 15, new CommonItem(308, "gp", "Masterwork Throwing Axe")));
            weapon_pile.Add(new ItemType("Ranged Weapon", 16, 25, new CommonItem(350, "gp", "Masterwork Heavy Crossbow")));
            weapon_pile.Add(new ItemType("Ranged Weapon", 26, 35, new CommonItem(335, "gp", "Masterwork Light Crossbow")));
            weapon_pile.Add(new ItemType("Ranged Weapon", 36, 39, new CommonItem(300, "gp", "Masterwork Dart")));
            weapon_pile.Add(new ItemType("Ranged Weapon", 40, 41, new CommonItem(301, "gp", "Masterwork Javelin")));
            weapon_pile.Add(new ItemType("Ranged Weapon", 42, 46, new CommonItem(330, "gp", "Masterwork Shortbow")));
            weapon_pile.Add(new ItemType("Ranged Weapon", 47, 51, new CommonItem(375, "gp", "Masterwork Composite Shortbow (+0 Str)")));
            weapon_pile.Add(new ItemType("Ranged Weapon", 52, 56, new CommonItem(450, "gp", "Masterwork Composite Shortbow (+1 Str)")));
            weapon_pile.Add(new ItemType("Ranged Weapon", 57, 61, new CommonItem(525, "gp", "Masterwork Composite Shortbow (+2 Str)")));
            weapon_pile.Add(new ItemType("Ranged Weapon", 62, 65, new CommonItem(300, "gp", "Masterwork Sling")));
            weapon_pile.Add(new ItemType("Ranged Weapon", 66, 75, new CommonItem(375, "gp", "Masterwork Longbow")));
            weapon_pile.Add(new ItemType("Ranged Weapon", 76, 80, new CommonItem(400, "gp", "Masterwork Composite Longbow (+0 Str)")));
            weapon_pile.Add(new ItemType("Ranged Weapon", 81, 85, new CommonItem(500, "gp", "Masterwork Composite Longbow (+1 Str)")));
            weapon_pile.Add(new ItemType("Ranged Weapon", 86, 90, new CommonItem(600, "gp", "Masterwork Composite Longbow (+2 Str)")));
            weapon_pile.Add(new ItemType("Ranged Weapon", 91, 95, new CommonItem(700, "gp", "Masterwork Composite Longbow (+3 Str)")));
            weapon_pile.Add(new ItemType("Ranged Weapon", 96, 100, new CommonItem(800, "gp", "Masterwork Composite Longbow (+4 Str)")));
            return weapon_pile;
        }

        public static Treasure_Leaf CreateRangedWeaponItemsList()
        {
            var treasure_pile = new Treasure_Leaf();
            treasure_pile.AddTreasure(SetupUncommonMeleeWeaponItemTypes());
            return treasure_pile;
        }

        private static Treasure_Composite SetupWeaponItemTypes()
        {
            var treasure_composite = new Treasure_Composite();
            treasure_composite.AddTreasure(new TreasureType("Weapons", 0, 50, CreateCommonMeleeWeaponItemsList()));
            treasure_composite.AddTreasure(new TreasureType("Weapons", 51, 70, CreateUncommonMeleeWeaponItemsList()));
            treasure_composite.AddTreasure(new TreasureType("Weapons", 71, 100, CreateRangedWeaponItemsList()));
            return treasure_composite;
        }

        public static Treasure_Composite CreateWeaponItemsList()
        {
            return SetupWeaponItemTypes();
        }

        private static List<EnchantmentType> CreateEnchantmentType(int bonus, bool is_weapon)
        {
            var enchantment_list = new List<EnchantmentType>();
            var cost = (bonus * bonus) * 1000;
            if (is_weapon == true)
            {
                cost *= 2;
            }

            enchantment_list.Add(new EnchantmentType(0, 100, new Enchantment("+" + bonus.ToString(), cost, "gp")));
            return enchantment_list;
        }

        /* Enchantment List Methods
         * Create: Melee, Ranged, Armour, and Shields enchantment lists
         */

        private static List<EnchantmentType> SetupMinorMeleeWeaponEnchantmentTypes()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 10, new Enchantment("Bane", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(11, 17, new Enchantment("Defending", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(18, 27, new Enchantment("Flaming", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(28, 37, new Enchantment("Frost", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(38, 47, new Enchantment("Shock", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(48, 56, new Enchantment("Ghost Touch", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(57, 67, new Enchantment("Keen", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(68, 71, new Enchantment("Ki Focus", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(72, 75, new Enchantment("Merciful", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(76, 82, new Enchantment("Mighty Cleaving", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(83, 87, new Enchantment("Spell Storing", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(88, 91, new Enchantment("Throwing", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(92, 95, new Enchantment("Thundering", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(96, 99, new Enchantment("Vicious", 2000, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateMinorMeleeWeaponEnchantmentsList()
        {
            return SetupMinorMeleeWeaponEnchantmentTypes();
        }

        private static List<EnchantmentType> SetupMediumMeleeWeaponEnchantmentTypes()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 6, new Enchantment("Bane", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(7, 12, new Enchantment("Defending", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(13, 19, new Enchantment("Flaming", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(20, 26, new Enchantment("Frost", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(27, 33, new Enchantment("Shock", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(34, 38, new Enchantment("Ghost Touch", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(39, 44, new Enchantment("Keen", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(45, 48, new Enchantment("Ki Focus", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(49, 50, new Enchantment("Merciful", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(51, 54, new Enchantment("Mighty Cleaving", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(55, 59, new Enchantment("Spell Storing", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(60, 63, new Enchantment("Throwing", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(64, 65, new Enchantment("Thundering", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(66, 69, new Enchantment("Vicious", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(70, 72, new Enchantment("Anarchic", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(73, 75, new Enchantment("Axiomatic", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(76, 78, new Enchantment("Disruption", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(79, 81, new Enchantment("Flaming Burst", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(82, 84, new Enchantment("Icy Burst", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(85, 87, new Enchantment("Holy", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(88, 90, new Enchantment("Shocking Burst", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(91, 93, new Enchantment("Unholy", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(94, 95, new Enchantment("Wounding", 8000, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateMediumMeleeWeaponEnchantmentsList()
        {
            return SetupMediumMeleeWeaponEnchantmentTypes();
        }

        private static List<EnchantmentType> SetupMajorMeleeWeaponEnchantmentTypes()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Bane", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(4, 6, new Enchantment("Flaming", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(7, 9, new Enchantment("Frost", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(10, 12, new Enchantment("Shock", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(13, 15, new Enchantment("Ghost Touch", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(16, 19, new Enchantment("Ki Focus", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(20, 21, new Enchantment("Mighty Cleaving", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(22, 24, new Enchantment("Spell Storing", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(25, 28, new Enchantment("Throwing", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(29, 32, new Enchantment("Thundering", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(33, 36, new Enchantment("Vicious", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(37, 41, new Enchantment("Anarchic", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(42, 46, new Enchantment("Axiomatic", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(47, 49, new Enchantment("Disruption", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(50, 54, new Enchantment("Flaming Burst", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(55, 59, new Enchantment("Icy Burst", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(60, 64, new Enchantment("Holy", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(65, 69, new Enchantment("Shocking Burst", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(70, 74, new Enchantment("Unholy", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(75, 78, new Enchantment("Wounding", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(79, 83, new Enchantment("Speed", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(84, 86, new Enchantment("Brilliant Energy", 32000, "gp")));
            enchantment_list.Add(new EnchantmentType(87, 88, new Enchantment("Dancing", 32000, "gp")));
            enchantment_list.Add(new EnchantmentType(89, 90, new Enchantment("Vorpal", 50000, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateMajorMeleeWeaponEnchantmentsList()
        {
            return SetupMajorMeleeWeaponEnchantmentTypes();
        }

        private static List<EnchantmentType> SetupMinorRangedWeaponEnchantmentTypes()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 12, new Enchantment("Bane", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(13, 25, new Enchantment("Distance", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(26, 40, new Enchantment("Flaming", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(41, 55, new Enchantment("Frost", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(56, 60, new Enchantment("Merciful", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(61, 68, new Enchantment("Returning", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(69, 83, new Enchantment("Shock", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(84, 93, new Enchantment("Seeking", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(94, 99, new Enchantment("Thundering", 2000, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateMinorRangedWeaponEnchantmentsList()
        {
            return SetupMinorRangedWeaponEnchantmentTypes();
        }

        private static List<EnchantmentType> SetupMediumRangedWeaponEnchantmentTypes()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 8, new Enchantment("Bane", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(9, 16, new Enchantment("Distance", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(17, 28, new Enchantment("Flaming", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(29, 40, new Enchantment("Frost", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(41, 42, new Enchantment("Merciful", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(43, 47, new Enchantment("Returning", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(48, 59, new Enchantment("Shock", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(60, 64, new Enchantment("Seeking", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(65, 68, new Enchantment("Thundering", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(69, 71, new Enchantment("Anarchic", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(72, 74, new Enchantment("Axiomatic", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(75, 79, new Enchantment("Flaming Burst", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(80, 82, new Enchantment("Holy", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(83, 87, new Enchantment("Icy Burst", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(88, 92, new Enchantment("Shocking Burst", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(93, 95, new Enchantment("Unholy", 8000, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateMediumRangedWeaponEnchantmentsList()
        {
            return SetupMediumRangedWeaponEnchantmentTypes();
        }

        private static List<EnchantmentType> SetupMajorRangedWeaponEnchantmentTypes()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 4, new Enchantment("Bane", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(5, 8, new Enchantment("Distance", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(9, 12, new Enchantment("Flaming", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(13, 16, new Enchantment("Frost", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(17, 21, new Enchantment("Returning", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(22, 25, new Enchantment("Shock", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(26, 27, new Enchantment("Seeking", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(28, 29, new Enchantment("Thundering", 2000, "gp")));
            enchantment_list.Add(new EnchantmentType(30, 34, new Enchantment("Anarchic", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(35, 39, new Enchantment("Axiomatic", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(40, 49, new Enchantment("Flaming Burst", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(50, 54, new Enchantment("Holy", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(55, 64, new Enchantment("Icy Burst", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(65, 74, new Enchantment("Shocking Burst", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(75, 79, new Enchantment("Unholy", 8000, "gp")));
            enchantment_list.Add(new EnchantmentType(80, 84, new Enchantment("Speed", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(85, 90, new Enchantment("Brilliant Energy", 32000, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateMajorRangedWeaponEnchantmentsList()
        {
            return SetupMajorRangedWeaponEnchantmentTypes();
        }

        private static List<EnchantmentType> SetupMinorArmourEnchantmentTypes()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 25, new Enchantment("Glamered", 2700, "gp")));
            enchantment_list.Add(new EnchantmentType(26, 32, new Enchantment("Fortification, light", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(33, 52, new Enchantment("Slick", 3750, "gp")));
            enchantment_list.Add(new EnchantmentType(53, 72, new Enchantment("Shadow", 3750, "gp")));
            enchantment_list.Add(new EnchantmentType(73, 92, new Enchantment("Silent Moves", 3750, "gp")));
            enchantment_list.Add(new EnchantmentType(93, 96, new Enchantment("Spell Resistance (13)", 4000, "gp")));
            enchantment_list.Add(new EnchantmentType(97, 97, new Enchantment("Improved Slick", 15000, "gp")));
            enchantment_list.Add(new EnchantmentType(98, 98, new Enchantment("Improved Shadow", 15000, "gp")));
            enchantment_list.Add(new EnchantmentType(99, 99, new Enchantment("Improved Silent Moves", 15000, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateMinorArmourEnchantmentsList()
        {
            return SetupMinorArmourEnchantmentTypes();
        }

        private static List<EnchantmentType> SetupMediumArmourEnchantmentTypes()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 5, new Enchantment("Glamered", 2700, "gp")));
            enchantment_list.Add(new EnchantmentType(6, 8, new Enchantment("Light Fortification", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(9, 11, new Enchantment("Slick", 3750, "gp")));
            enchantment_list.Add(new EnchantmentType(12, 14, new Enchantment("Shadow", 3750, "gp")));
            enchantment_list.Add(new EnchantmentType(15, 17, new Enchantment("Silent Moves", 3750, "gp")));
            enchantment_list.Add(new EnchantmentType(18, 19, new Enchantment("Spell Resistance (13)", 4000, "gp")));
            enchantment_list.Add(new EnchantmentType(20, 29, new Enchantment("Improved Slick", 15000, "gp")));
            enchantment_list.Add(new EnchantmentType(30, 39, new Enchantment("Improved Shadow", 15000, "gp")));
            enchantment_list.Add(new EnchantmentType(40, 49, new Enchantment("Improved Silent Moves", 15000, "gp")));
            enchantment_list.Add(new EnchantmentType(50, 54, new Enchantment("Acid Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(55, 59, new Enchantment("Cold Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(60, 64, new Enchantment("Electricity Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(65, 69, new Enchantment("Fire Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(70, 74, new Enchantment("Sonic Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(75, 79, new Enchantment("Ghost Touch", 9000, "gp")));
            enchantment_list.Add(new EnchantmentType(80, 84, new Enchantment("Invulnerability", 9000, "gp")));
            enchantment_list.Add(new EnchantmentType(85, 89, new Enchantment("Moderate Fortification", 9000, "gp")));
            enchantment_list.Add(new EnchantmentType(90, 94, new Enchantment("Spell Resistance (15)", 9000, "gp")));
            enchantment_list.Add(new EnchantmentType(95, 99, new Enchantment("Wild", 9000, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateMediumArmourEnchantmentsList()
        {
            return SetupMediumArmourEnchantmentTypes();
        }

        private static List<EnchantmentType> SetupMajorArmourEnchantmentTypes()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 3, new Enchantment("Glamered", 2700, "gp")));
            enchantment_list.Add(new EnchantmentType(4, 4, new Enchantment("Light Fortification", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(5, 7, new Enchantment("Improved Slick", 15000, "gp")));
            enchantment_list.Add(new EnchantmentType(8, 10, new Enchantment("Improved Shadow", 15000, "gp")));
            enchantment_list.Add(new EnchantmentType(11, 13, new Enchantment("Improved Silent Moves", 15000, "gp")));
            enchantment_list.Add(new EnchantmentType(14, 16, new Enchantment("Acid Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(17, 19, new Enchantment("Cold Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(20, 22, new Enchantment("Electricity Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(23, 25, new Enchantment("Fire Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(26, 28, new Enchantment("Sonic Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(29, 33, new Enchantment("Ghost Touch", 9000, "gp")));
            enchantment_list.Add(new EnchantmentType(34, 35, new Enchantment("Invulnerability", 9000, "gp")));
            enchantment_list.Add(new EnchantmentType(36, 40, new Enchantment("Moderate Fortification", 9000, "gp")));
            enchantment_list.Add(new EnchantmentType(41, 42, new Enchantment("Spell Resistance (15)", 9000, "gp")));
            enchantment_list.Add(new EnchantmentType(43, 43, new Enchantment("Wild", 9000, "gp")));
            enchantment_list.Add(new EnchantmentType(44, 48, new Enchantment("Greater Slick", 33750, "gp")));
            enchantment_list.Add(new EnchantmentType(49, 53, new Enchantment("Greater Shadow", 33750, "gp")));
            enchantment_list.Add(new EnchantmentType(54, 58, new Enchantment("Greater Silent Moves", 33750, "gp")));
            enchantment_list.Add(new EnchantmentType(59, 63, new Enchantment("Improved Acid Resistance", 42000, "gp")));
            enchantment_list.Add(new EnchantmentType(64, 68, new Enchantment("Improved Cold Resistance", 42000, "gp")));
            enchantment_list.Add(new EnchantmentType(69, 73, new Enchantment("Improved Electricity Resistance", 42000, "gp")));
            enchantment_list.Add(new EnchantmentType(74, 78, new Enchantment("Improved Fire Resistance", 42000, "gp")));
            enchantment_list.Add(new EnchantmentType(79, 83, new Enchantment("Improved Sonic Resistance", 42000, "gp")));
            enchantment_list.Add(new EnchantmentType(84, 88, new Enchantment("Spell Resistance (17)", 16000, "gp")));
            enchantment_list.Add(new EnchantmentType(89, 89, new Enchantment("Etherealness", 49000, "gp")));
            enchantment_list.Add(new EnchantmentType(90, 90, new Enchantment("Control Undead", 49000, "gp")));
            enchantment_list.Add(new EnchantmentType(91, 92, new Enchantment("Heavy Fortification", 25000, "gp")));
            enchantment_list.Add(new EnchantmentType(93, 94, new Enchantment("Spell Resistance (19)", 25000, "gp")));
            enchantment_list.Add(new EnchantmentType(95, 95, new Enchantment("Greater Acid Resistance", 66000, "gp")));
            enchantment_list.Add(new EnchantmentType(96, 96, new Enchantment("Greater Cold Resistance", 66000, "gp")));
            enchantment_list.Add(new EnchantmentType(97, 97, new Enchantment("Greater Electricity Resistance", 66000, "gp")));
            enchantment_list.Add(new EnchantmentType(98, 98, new Enchantment("Greater Fire Resistance", 66000, "gp")));
            enchantment_list.Add(new EnchantmentType(99, 99, new Enchantment("Greater Sonic Resistance", 66000, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateMajorArmourEnchantmentsList()
        {
            return SetupMajorArmourEnchantmentTypes();
        }

        private static List<EnchantmentType> SetupMinorShieldEnchantmentTypes()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 20, new Enchantment("Arrow Catching", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(21, 40, new Enchantment("Bashing", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(41, 50, new Enchantment("Blinding", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(51, 75, new Enchantment("Fortification, light", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(76, 92, new Enchantment("Arrow Deflection", 4000, "gp")));
            enchantment_list.Add(new EnchantmentType(93, 97, new Enchantment("Animated", 4000, "gp")));
            enchantment_list.Add(new EnchantmentType(98, 99, new Enchantment("Spell Resistance (13)", 4000, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateMinorShieldEnchantmentsList()
        {
            return SetupMinorShieldEnchantmentTypes();
        }

        private static List<EnchantmentType> SetupMediumShieldEnchantmentTypes()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 10, new Enchantment("Arrow Catching", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(11, 20, new Enchantment("Bashing", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(21, 25, new Enchantment("Blinding", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(26, 40, new Enchantment("Light Fortification", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(41, 50, new Enchantment("Arrow Deflection", 4000, "gp")));
            enchantment_list.Add(new EnchantmentType(51, 57, new Enchantment("Animated", 4000, "gp")));
            enchantment_list.Add(new EnchantmentType(58, 59, new Enchantment("Spell Resistance (13)", 4000, "gp")));
            enchantment_list.Add(new EnchantmentType(60, 63, new Enchantment("Acid Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(64, 67, new Enchantment("Cold Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(68, 71, new Enchantment("Electricity Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(72, 75, new Enchantment("Fire Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(76, 79, new Enchantment("Sonic Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(80, 85, new Enchantment("Ghost Touch", 9000, "gp")));
            enchantment_list.Add(new EnchantmentType(86, 95, new Enchantment("Moderate Fortification", 9000, "gp")));
            enchantment_list.Add(new EnchantmentType(96, 98, new Enchantment("Spell Resistance", 9000, "gp")));
            enchantment_list.Add(new EnchantmentType(99, 99, new Enchantment("Wild", 9000, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateMediumShieldEnchantmentsList()
        {
            return SetupMediumShieldEnchantmentTypes();
        }

        private static List<EnchantmentType> SetupMajorShieldEnchantmentTypes()
        {
            var enchantment_list = new List<EnchantmentType>();
            enchantment_list.Add(new EnchantmentType(0, 5, new Enchantment("Arrow Catching", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(6, 8, new Enchantment("Bashing", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(9, 10, new Enchantment("Blinding", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(11, 15, new Enchantment("Light Fortification", 1000, "gp")));
            enchantment_list.Add(new EnchantmentType(16, 20, new Enchantment("Arrow Deflection", 4000, "gp")));
            enchantment_list.Add(new EnchantmentType(21, 25, new Enchantment("Animated", 4000, "gp")));
            enchantment_list.Add(new EnchantmentType(26, 28, new Enchantment("Acid Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(29, 31, new Enchantment("Cold Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(32, 34, new Enchantment("Electricity Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(35, 37, new Enchantment("Fire Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(38, 40, new Enchantment("Sonic Resistance", 18000, "gp")));
            enchantment_list.Add(new EnchantmentType(41, 46, new Enchantment("Ghost Touch", 9000, "gp")));
            enchantment_list.Add(new EnchantmentType(47, 56, new Enchantment("Moderate Fortification", 9000, "gp")));
            enchantment_list.Add(new EnchantmentType(57, 58, new Enchantment("Spell Resistance", 9000, "gp")));
            enchantment_list.Add(new EnchantmentType(59, 59, new Enchantment("Wild", 9000, "gp")));
            enchantment_list.Add(new EnchantmentType(60, 64, new Enchantment("Improved Acid Resistance", 42000, "gp")));
            enchantment_list.Add(new EnchantmentType(65, 69, new Enchantment("Improved Cold Resistance", 42000, "gp")));
            enchantment_list.Add(new EnchantmentType(70, 74, new Enchantment("Improved Electricity Resistance", 42000, "gp")));
            enchantment_list.Add(new EnchantmentType(75, 79, new Enchantment("Improved Fire Resistance", 42000, "gp")));
            enchantment_list.Add(new EnchantmentType(80, 84, new Enchantment("Improved Sonic Resistance", 42000, "gp")));
            enchantment_list.Add(new EnchantmentType(85, 86, new Enchantment("Spell Resistance (17)", 16000, "gp")));
            enchantment_list.Add(new EnchantmentType(87, 87, new Enchantment("Undead Controlling", 49000, "gp")));
            enchantment_list.Add(new EnchantmentType(88, 91, new Enchantment("Heavy Fortification", 25000, "gp")));
            enchantment_list.Add(new EnchantmentType(92, 93, new Enchantment("Reflecting", 25000, "gp")));
            enchantment_list.Add(new EnchantmentType(94, 94, new Enchantment("Spell Resistance (19)", 25000, "gp")));
            enchantment_list.Add(new EnchantmentType(95, 95, new Enchantment("Greater Acid Resistance", 66000, "gp")));
            enchantment_list.Add(new EnchantmentType(96, 96, new Enchantment("Greater Cold Resistance", 66000, "gp")));
            enchantment_list.Add(new EnchantmentType(97, 97, new Enchantment("Greater Electricity Resistance", 66000, "gp")));
            enchantment_list.Add(new EnchantmentType(98, 98, new Enchantment("Greater Fire Resistance", 66000, "gp")));
            enchantment_list.Add(new EnchantmentType(99, 99, new Enchantment("Greater Sonic Resistance", 66000, "gp")));
            return enchantment_list;
        }

        public static List<EnchantmentType> CreateMajorShieldEnchantmentsList()
        {
            return SetupMajorShieldEnchantmentTypes();
        }


        private static Treasure_Leaf SetupEnchantedItems(List<ItemType> item_list, List<EnchantmentType> enchantment_list)
        {
            var treasure_pile = new Treasure_Leaf();
            foreach (var item in item_list)
            {
                treasure_pile.AddTreasure(item.CopyItem(new MagicItem(item.Item, enchantment_list)));
            }
            return treasure_pile;
        }


        /* Minor MagicItem Methods
         * Create: Weapons, Armour/Shields, Potions, Rings, Scrolls, Wands, and Wondrous Items
         */

        private static Treasure_Leaf SetupMinorPotionTypes()
        {
            var potion_list = new Treasure_Leaf();
            potion_list.AddTreasure(new ItemType("Minor Potion", 0, 10, new CommonItem(50, "gp", "Potion of Cure Light Wounds")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 11, 13, new CommonItem(50, "gp", "Potion of Endure Elements")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 14, 15, new CommonItem(50, "gp", "Potion of Hide from Animals")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 16, 17, new CommonItem(50, "gp", "Potion of Hide from Undead")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 18, 19, new CommonItem(50, "gp", "Potion of Jump")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 20, 22, new CommonItem(50, "gp", "Potion of Mage Armour")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 23, 25, new CommonItem(50, "gp", "Potion of Magic Fang")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 26, 26, new CommonItem(50, "gp", "Potion of Magic Stone")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 27, 29, new CommonItem(50, "gp", "Potion of Magic Weapon")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 30, 30, new CommonItem(50, "gp", "Potion of Pass without Trace")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 31, 32, new CommonItem(50, "gp", "Potion of Protection from Alignment")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 33, 34, new CommonItem(50, "gp", "Potion of Remove Fear")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 35, 35, new CommonItem(50, "gp", "Potion of Sanctuary")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 36, 38, new CommonItem(50, "gp", "Potion of Shield of Faith +2")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 39, 39, new CommonItem(50, "gp", "Potion of Shillelagh")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 40, 41, new CommonItem(100, "gp", "Potion of Bless Weapon")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 42, 44, new CommonItem(250, "gp", "Potion of Enlarge Person")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 45, 45, new CommonItem(250, "gp", "Potion of Reduce Person")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 46, 47, new CommonItem(300, "gp", "Potion of Aid")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 48, 50, new CommonItem(300, "gp", "Potion of Barkskin +2")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 51, 53, new CommonItem(300, "gp", "Potion of Bear's Endurance")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 54, 56, new CommonItem(300, "gp", "Potion of Blur")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 57, 59, new CommonItem(300, "gp", "Potion of Bull's Strength")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 60, 62, new CommonItem(300, "gp", "Potion of Cat's Grace")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 63, 67, new CommonItem(300, "gp", "Potion of Cure Moderate Wounds")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 68, 68, new CommonItem(300, "gp", "Potion of Darkness")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 69, 71, new CommonItem(300, "gp", "Potion of Darkvision")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 72, 74, new CommonItem(300, "gp", "Potion of Delay Poison")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 75, 76, new CommonItem(300, "gp", "Potion of Eagle's Splendor")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 77, 78, new CommonItem(300, "gp", "Potion of Fox's Cunning")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 79, 81, new CommonItem(300, "gp", "Potion of Invisibility")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 82, 84, new CommonItem(300, "gp", "Potion of Lesser Restoration")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 85, 86, new CommonItem(300, "gp", "Potion of Levitate")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 87, 87, new CommonItem(300, "gp", "Potion of Misdirection")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 88, 89, new CommonItem(300, "gp", "Potion of Owl's Wisdom")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 90, 91, new CommonItem(300, "gp", "Potion of Protection from Arrows (10/magic)")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 92, 93, new CommonItem(300, "gp", "Potion of Remove Paralysis")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 94, 96, new CommonItem(300, "gp", "Potion of Resist Energy (10)")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 97, 97, new CommonItem(300, "gp", "Potion of Shield of Faith +3")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 98, 99, new CommonItem(300, "gp", "Potion of Spider Climb")));
            potion_list.AddTreasure(new ItemType("Minor Potion", 100, 100, new CommonItem(300, "gp", "Potion of Undetectable Alignment")));
            return potion_list;
        }

        public static Treasure_Leaf CreateMinorPotionsList()
        {
            return SetupMinorPotionTypes();
        }

        private static Treasure_Composite SetupMinorScrollTypes()
        {
            var treasure_pile = new Treasure_Composite();

            var arcane_scrolls = new Treasure_Leaf();
            arcane_scrolls.AddTreasure(new ItemType("Minor Arcane Scroll", 0, 5, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel0ArcaneSpellList())));
            arcane_scrolls.AddTreasure(new ItemType("Minor Arcane Scroll", 6, 50, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel1ArcaneSpellList())));
            arcane_scrolls.AddTreasure(new ItemType("Minor Arcane Scroll", 51, 95, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel2ArcaneSpellList())));
            arcane_scrolls.AddTreasure(new ItemType("Minor Arcane Scroll", 96, 100, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel3ArcaneSpellList())));

            var divine_scrolls = new Treasure_Leaf();
            divine_scrolls.AddTreasure(new ItemType("Minor Divine Scroll", 0, 5, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel0DivineSpellList())));
            divine_scrolls.AddTreasure(new ItemType("Minor Divine Scroll", 6, 50, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel1DivineSpellList())));
            divine_scrolls.AddTreasure(new ItemType("Minor Divine Scroll", 51, 95, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel2DivineSpellList())));
            divine_scrolls.AddTreasure(new ItemType("Minor Divine Scroll", 96, 100, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel3DivineSpellList())));


            treasure_pile.AddTreasure(new TreasureType("Minor Arcane Scroll", 0, 70, arcane_scrolls));
            treasure_pile.AddTreasure(new TreasureType("Minor Divine Scroll", 71, 100, divine_scrolls));
            return treasure_pile;
        }

        public static Treasure_Composite CreateMinorScrollList()
        {
            return SetupMinorScrollTypes();
        }

        private static Treasure_Composite SetupMinorWeaponsTypes()
        {
            var treasure_pile = new Treasure_Composite();

            var enchantment_1_wep_list = CreateEnchantmentType(1, true);
            var enchantment_2_wep_list = CreateEnchantmentType(2, true);
            
            var common_melee_list = SetupCommonMeleeWeaponItemTypes();
            var uncommon_melee_list = SetupUncommonMeleeWeaponItemTypes();
            var ranged_list = SetupRangedWeaponItemTypes();
            

            var special_list = new Treasure_Composite();
            special_list.AddTreasure(new TreasureType("Common Melee", 0, 70, SetupEnchantedItems(common_melee_list, SetupMinorMeleeWeaponEnchantmentTypes())));
            special_list.AddTreasure(new TreasureType("Uncommon Melee", 71, 80, SetupEnchantedItems(uncommon_melee_list, SetupMinorMeleeWeaponEnchantmentTypes())));
            special_list.AddTreasure(new TreasureType("Ranged", 81, 100, SetupEnchantedItems(ranged_list, SetupMinorMeleeWeaponEnchantmentTypes())));
            
            var bonus_1_list = new Treasure_Composite();
            bonus_1_list.AddTreasure(new TreasureType("Common Melee", 0, 70, SetupEnchantedItems(common_melee_list, enchantment_1_wep_list)));
            bonus_1_list.AddTreasure(new TreasureType("Uncommon Melee", 71, 80, SetupEnchantedItems(uncommon_melee_list, enchantment_1_wep_list)));
            bonus_1_list.AddTreasure(new TreasureType("Ranged", 81, 100, SetupEnchantedItems(ranged_list, enchantment_1_wep_list)));

            var bonus_2_list = new Treasure_Composite();
            bonus_2_list.AddTreasure(new TreasureType("Common Melee", 0, 70, SetupEnchantedItems(common_melee_list, enchantment_2_wep_list)));
            bonus_2_list.AddTreasure(new TreasureType("Uncommon Melee", 71, 80, SetupEnchantedItems(uncommon_melee_list, enchantment_2_wep_list)));
            bonus_2_list.AddTreasure(new TreasureType("Ranged", 81, 100, SetupEnchantedItems(ranged_list, enchantment_2_wep_list)));
            
            var specific_list = new Treasure_Leaf();
            specific_list.AddTreasure(new ItemType("Specific", 0, 15, new CommonItem(132, "gp", "Sleep Arrow")));
            specific_list.AddTreasure(new ItemType("Specific", 16, 25, new CommonItem(267, "gp", "Screaming Bolt")));
            specific_list.AddTreasure(new ItemType("Specific", 26, 45, new CommonItem(322, "gp", "Masterwork Silver Dagger")));
            specific_list.AddTreasure(new ItemType("Specific", 46, 65, new CommonItem(320, "gp", "Masterwork Cold Iron Longsword")));
            specific_list.AddTreasure(new ItemType("Specific", 66, 75, new CommonItem(1500, "gp", "Javelin of Lightning")));
            specific_list.AddTreasure(new ItemType("Specific", 76, 80, new CommonItem(2282, "gp", "Slaying Arrow")));
            specific_list.AddTreasure(new ItemType("Specific", 81, 90, new CommonItem(3002, "gp", "Adamantine Dagger")));
            specific_list.AddTreasure(new ItemType("Specific", 91, 100, new CommonItem(3010, "gp", "Adamantine Battleaxe")));

            treasure_pile.AddTreasure(new TreasureType("+1 Bonus", 0, 70, bonus_1_list));
            treasure_pile.AddTreasure(new TreasureType("+2 Bonus", 71, 85, bonus_2_list));
            treasure_pile.AddTreasure(new TreasureType("Specific Weapon", 86, 90, specific_list));
            treasure_pile.AddTreasure(new TreasureType("Special Ability", 91, 100, special_list));

            return treasure_pile;
        }

        public static Treasure_Composite CreateMinorWeaponsList()
        {
            return SetupMinorWeaponsTypes();
        }

        private static Treasure_Composite SetupMinorArmourShieldTypes()
        {
            var treasure_pile = new Treasure_Composite();
            
            // Specific Armour
            var specific_armour_list = new Treasure_Leaf();
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 0, 50, new CommonItem(1100, "gp", "Mithral Shirt")));
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 51, 80, new CommonItem(3300, "gp", "Dragonhide Plate")));
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 81, 100, new CommonItem(4150, "gp", "Elven Chain")));

            // Specific Shield
            var specific_shield_list = new Treasure_Leaf();
            specific_shield_list.AddTreasure(new ItemType("Specific Shield", 0, 30, new CommonItem(205, "gp", "Darkwood Buckler")));
            specific_shield_list.AddTreasure(new ItemType("Specific Shield", 31, 80, new CommonItem(257, "gp", "Darkwood Shield")));
            specific_shield_list.AddTreasure(new ItemType("Specific Shield", 81, 95, new CommonItem(1020, "gp", "Mithral Heavy Shield")));
            specific_shield_list.AddTreasure(new ItemType("Specific Shield", 96, 100, new CommonItem(3153, "gp", "Caster's Shield")));

            // Special ability compound
            var special_item_list = new Treasure_Composite();
            special_item_list.AddTreasure(new TreasureType("Special Armour", 0, 50, SetupEnchantedItems(SetupArmourTypeToEnchant(), CreateMinorArmourEnchantmentsList())));
            special_item_list.AddTreasure(new TreasureType("Special Shield", 51, 100, SetupEnchantedItems(SetupShieldTypeToEnchant(), CreateMinorShieldEnchantmentsList())));
            
            treasure_pile.AddTreasure(new TreasureType("+1 Shield", 0, 60, SetupEnchantedItems(SetupShieldTypeToEnchant(), CreateEnchantmentType(1, false))));
            treasure_pile.AddTreasure(new TreasureType("+1 Armour", 61, 80, SetupEnchantedItems(SetupArmourTypeToEnchant(), CreateEnchantmentType(1, false))));
            treasure_pile.AddTreasure(new TreasureType("+2 Shield", 81, 85, SetupEnchantedItems(SetupShieldTypeToEnchant(), CreateEnchantmentType(2, false))));
            treasure_pile.AddTreasure(new TreasureType("+1 Armour", 86, 87, SetupEnchantedItems(SetupArmourTypeToEnchant(), CreateEnchantmentType(2, false))));
            treasure_pile.AddTreasure(new TreasureType("Specific Armour", 88, 89, specific_armour_list));
            treasure_pile.AddTreasure(new TreasureType("Specific Shield", 90, 91, specific_shield_list));
            treasure_pile.AddTreasure(new TreasureType("Special Ability", 92, 100, special_item_list));

            return treasure_pile;
        }

        public static Treasure_Composite CreateMinorArmourShieldList()
        {
            return SetupMinorArmourShieldTypes();
        }

        private static Treasure_Leaf SetupMinorRingTypes()
        {
            var treasure_pile = new Treasure_Leaf();
            treasure_pile.AddTreasure(new ItemType("Minor Ring", 0, 18, new CommonItem(2000, "gp", "Ring of Protection +1")));
            treasure_pile.AddTreasure(new ItemType("Minor Ring", 19, 28, new CommonItem(2200, "gp", "Ring of Feather Falling")));
            treasure_pile.AddTreasure(new ItemType("Minor Ring", 29, 36, new CommonItem(2500, "gp", "Ring of Sustenance")));
            treasure_pile.AddTreasure(new ItemType("Minor Ring", 37, 44, new CommonItem(2500, "gp", "Ring of Climbing")));
            treasure_pile.AddTreasure(new ItemType("Minor Ring", 45, 52, new CommonItem(2500, "gp", "Ring of Jumping")));
            treasure_pile.AddTreasure(new ItemType("Minor Ring", 53, 60, new CommonItem(2500, "gp", "Ring of Swimming")));
            treasure_pile.AddTreasure(new ItemType("Minor Ring", 61, 70, new CommonItem(4000, "gp", "Ring of Counterspells")));
            treasure_pile.AddTreasure(new ItemType("Minor Ring", 71, 75, new CommonItem(8000, "gp", "Ring of Mind Shielding")));
            treasure_pile.AddTreasure(new ItemType("Minor Ring", 76, 80, new CommonItem(8000, "gp", "Ring of Protection +1")));
            treasure_pile.AddTreasure(new ItemType("Minor Ring", 81, 85, new CommonItem(8500, "gp", "Ring of Force Shield")));
            treasure_pile.AddTreasure(new ItemType("Minor Ring", 86, 90, new CommonItem(8600, "gp", "Ring of Ram")));
            treasure_pile.AddTreasure(new ItemType("Minor Ring", 91, 93, new CommonItem(12000, "gp", "Ring of Animal Friendship")));
            treasure_pile.AddTreasure(new ItemType("Minor Ring", 94, 96, new CommonItem(12000, "gp", "Ring of Minor Energy Resistance")));
            treasure_pile.AddTreasure(new ItemType("Minor Ring", 97, 98, new CommonItem(12700, "gp", "Ring of Chameleon Power")));
            treasure_pile.AddTreasure(new ItemType("Minor Ring", 99, 100, new CommonItem(15000, "gp", "Ring of Water Walking")));
            return treasure_pile;
        }

        public static Treasure_Leaf CreateMinorRingsList()
        {
            return SetupMinorRingTypes();
        }

        private static Treasure_Leaf SetupMinorWandTypes()
        {
            var wand_list = new Treasure_Leaf();
            wand_list.AddTreasure(new ItemType("Minor Wand", 0, 2, new CommonItem(375, "gp", "Wand of Detect Magic")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 3, 4, new CommonItem(375, "gp", "Wand of Light")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 5, 7, new CommonItem(750, "gp", "Wand of Burnig Hands")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 8, 10, new CommonItem(750, "gp", "Wand of Charm Animal")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 11, 13, new CommonItem(750, "gp", "Wand of Charm Person")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 14, 16, new CommonItem(750, "gp", "Wand of Colour Spray")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 17, 19, new CommonItem(750, "gp", "Wand of Cure Light Wounds")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 20, 22, new CommonItem(750, "gp", "Wand of Detect Secret Doors")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 23, 25, new CommonItem(750, "gp", "Wand of Enlarge Person")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 26, 28, new CommonItem(750, "gp", "Wand of Magic Missile (1st level)")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 29, 31, new CommonItem(750, "gp", "Wand of Shocking Grasp")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 32, 34, new CommonItem(750, "gp", "Wand of Summon Monster I")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 35, 36, new CommonItem(2250, "gp", "Wand of Magic Missile (3rd level)")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 37, 37, new CommonItem(3750, "gp", "Wand of Magic Missile (5th level)")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 38, 40, new CommonItem(4500, "gp", "Wand of Bear's Endurance")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 41, 43, new CommonItem(4500, "gp", "Wand of Bull's Strength")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 44, 46, new CommonItem(4500, "gp", "Wand of Cat's Grace")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 47, 49, new CommonItem(4500, "gp", "Wand of Cure Moderate Wounds")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 50, 51, new CommonItem(4500, "gp", "Wand of Darkness")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 52, 54, new CommonItem(4500, "gp", "Wand of Daze Monster")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 55, 57, new CommonItem(4500, "gp", "Wand of Delay Poison")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 58, 60, new CommonItem(4500, "gp", "Wand of Eagle's Splendor")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 61, 63, new CommonItem(4500, "gp", "Wand of False Life")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 64, 66, new CommonItem(4500, "gp", "Wand of Fox's Cunning")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 67, 68, new CommonItem(4500, "gp", "Wand of Ghoul Touch")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 69, 70, new CommonItem(4500, "gp", "Wand of Hold Person")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 72, 74, new CommonItem(4500, "gp", "Wand of Invisibility")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 75, 77, new CommonItem(4500, "gp", "Wand of Knock")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 78, 80, new CommonItem(4500, "gp", "Wand of Levitate")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 81, 83, new CommonItem(4500, "gp", "Wand of Acid Arrow")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 84, 86, new CommonItem(4500, "gp", "Wand of Mirror Image")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 87, 89, new CommonItem(4500, "gp", "Wand of Owl's Wisdom")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 90, 91, new CommonItem(4500, "gp", "Wand of Shatter")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 92, 94, new CommonItem(4500, "gp", "Wand of Silence")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 95, 97, new CommonItem(4500, "gp", "Wand of Summon Monster II")));
            wand_list.AddTreasure(new ItemType("Minor Wand", 98, 100, new CommonItem(4500, "gp", "Wand of Web")));
            return wand_list;
        }

        public static Treasure_Leaf CreateMinorWandsList()
        {
            return SetupMinorWandTypes();
        }

        private static Treasure_Leaf SetupMinorWondrousTypes()
        {
            var item_list = new Treasure_Leaf();
            item_list.AddTreasure(new ItemType("Minor Wondrous", 0, 1, new CommonItem(50, "gp", "Anchor Feather Token")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 2, 2, new CommonItem(50, "gp", "Universal Solvent")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 3, 3, new CommonItem(150, "gp", "Elixer of Love")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 4, 4, new CommonItem(150, "gp", "Unguent of Timelessness")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 5, 5, new CommonItem(200, "gp", "Fan Feather Token")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 6, 6, new CommonItem(250, "gp", "Dust of Tracelessness")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 7, 7, new CommonItem(250, "gp", "Elixir of Hiding")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 8, 8, new CommonItem(250, "gp", "Elixir of Sneaking")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 9, 9, new CommonItem(250, "gp", "Elixir of Swimming")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 10, 10, new CommonItem(250, "gp", "Elixir of Vision")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 11, 11, new CommonItem(250, "gp", "Silversheen")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 12, 12, new CommonItem(300, "gp", "Bird Feather Token")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 13, 13, new CommonItem(400, "gp", "Tree Feather Token")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 14, 14, new CommonItem(450, "gp", "Swan Boat Feather Token")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 15, 15, new CommonItem(500, "gp", "Elixir of Truth")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 16, 16, new CommonItem(500, "gp", "Whip Feather Token")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 17, 17, new CommonItem(850, "gp", "Dust of Dryness")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 18, 18, new CommonItem(900, "gp", "Gray Bag of Tricks")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 19, 19, new CommonItem(900, "gp", "Hand of the Mage")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 20, 20, new CommonItem(1000, "gp", "Bracers of Armour +1")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 21, 21, new CommonItem(1000, "gp", "Cloak of Resistance +1")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 22, 22, new CommonItem(1000, "gp", "Pearl of Power (1st level)")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 23, 23, new CommonItem(1000, "gp", "Phylactery of Faithfulness")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 24, 24, new CommonItem(1000, "gp", "Salve of Slipperiness")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 25, 25, new CommonItem(1100, "gp", "Elixir of Fire Breath")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 26, 26, new CommonItem(1150, "gp", "Pipes of the Sewers")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 27, 27, new CommonItem(1200, "gp", "Dust of Illusion")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 28, 28, new CommonItem(1250, "gp", "Goggles of Minute Seeing")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 29, 29, new CommonItem(1500, "gp", "Brooch of Shielding")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 30, 30, new CommonItem(1650, "gp", "Necklace of Fireballs (Type I)")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 31, 31, new CommonItem(1800, "gp", "Dust of Appearance")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 32, 32, new CommonItem(1800, "gp", "Hat of Disguise")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 33, 33, new CommonItem(1800, "gp", "Pipes of Sounding")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 34, 34, new CommonItem(1800, "gp", "Efficient Quiver")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 35, 35, new CommonItem(2000, "gp", "Amulet of Natural Armour +1")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 36, 36, new CommonItem(2000, "gp", "Handy Haversack")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 37, 37, new CommonItem(2000, "gp", "Horn of Fog")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 38, 38, new CommonItem(2250, "gp", "Elemental Gem")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 39, 39, new CommonItem(2400, "gp", "Robe of Bones")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 40, 40, new CommonItem(2400, "gp", "Sovereign Glue")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 41, 41, new CommonItem(2500, "gp", "Bag of Holding (Type I)")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 42, 42, new CommonItem(2500, "gp", "Boots of Elvenkind")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 43, 43, new CommonItem(2500, "gp", "Boots of the Winterlands")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 44, 44, new CommonItem(2500, "gp", "Candle of Truth")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 45, 45, new CommonItem(2500, "gp", "Cloak of Elvenkind")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 46, 46, new CommonItem(2500, "gp", "Eyes of the Eagle")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 47, 47, new CommonItem(2500, "gp", "Golembane Scarab")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 48, 48, new CommonItem(2700, "gp", "Necklace of Fireballs (Type II)")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 49, 49, new CommonItem(2700, "gp", "Stone of Alarm")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 50, 50, new CommonItem(3000, "gp", "Rust Bag of Tricks")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 51, 51, new CommonItem(3000, "gp", "Bead of Force")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 52, 52, new CommonItem(3000, "gp", "Chime of Opening")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 53, 53, new CommonItem(3000, "gp", "Horseshoes of Speed")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 54, 54, new CommonItem(3000, "gp", "Rope of Climbing")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 55, 55, new CommonItem(3500, "gp", "Dust of Disappearance")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 56, 56, new CommonItem(3500, "gp", "Lens of Detection")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 57, 57, new CommonItem(3800, "gp", "Silver Raven Figurine of Wondrous Power")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 58, 58, new CommonItem(4000, "gp", "Amulet of Health +2")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 59, 59, new CommonItem(4000, "gp", "Bracers of Armour +2")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 60, 60, new CommonItem(4000, "gp", "Cloak of Charisma +2")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 61, 61, new CommonItem(4000, "gp", "Cloak of Resistance +2")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 62, 62, new CommonItem(4000, "gp", "Gauntlets of Ogre Power")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 63, 63, new CommonItem(4000, "gp", "Gloves of Arrow Snaring")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 64, 64, new CommonItem(4000, "gp", "Gloves of Dexterity +2")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 65, 65, new CommonItem(4000, "gp", "Headband of Intellect +2")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 66, 66, new CommonItem(4000, "gp", "Ioun Stone, Clear Spindle")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 67, 67, new CommonItem(4000, "gp", "Restorative Ointment")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 68, 68, new CommonItem(4000, "gp", "Marvelous Pigments")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 69, 69, new CommonItem(4000, "gp", "Pearl of Power (2nd level)")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 70, 70, new CommonItem(4000, "gp", "Periapt of Wisdom +2")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 71, 71, new CommonItem(4000, "gp", "Stone Salve")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 72, 72, new CommonItem(4350, "gp", "Necklace of Fireballs (Type III)")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 73, 73, new CommonItem(4500, "gp", "Circlet of Persuasion")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 74, 74, new CommonItem(4800, "gp", "Slippers of Spider Climbing")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 75, 75, new CommonItem(4900, "gp", "Incense of Meditation")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 76, 76, new CommonItem(5000, "gp", "Bag of Holding (Type II)")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 77, 77, new CommonItem(5000, "gp", "Lesser Bracers of Archery")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 78, 78, new CommonItem(5000, "gp", "Ioun Stone, Dusty Rose Prism")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 79, 79, new CommonItem(5200, "gp", "Helm of Comprehend Languages and Read Magic")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 80, 80, new CommonItem(5200, "gp", "Vest of Escape")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 81, 81, new CommonItem(5400, "gp", "Eversmoking Bottle")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 82, 82, new CommonItem(5400, "gp", "Sustaining Spoon")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 83, 83, new CommonItem(5400, "gp", "Necklace of Fireballs (Type IV)")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 84, 84, new CommonItem(5500, "gp", "Boots of Striding and Springing")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 85, 85, new CommonItem(5500, "gp", "Wind Fan")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 86, 86, new CommonItem(5400, "gp", "Necklace of Fireballs (Type V)")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 87, 87, new CommonItem(6000, "gp", "Amulet of Mighty Fists +1")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 88, 88, new CommonItem(6000, "gp", "Horseshoes of a Zephyr")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 89, 89, new CommonItem(6000, "gp", "Pipes of Haunting")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 90, 90, new CommonItem(6250, "gp", "Gloves of Swimming and Climbing")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 91, 91, new CommonItem(6300, "gp", "Tan Bag of Tricks")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 92, 92, new CommonItem(6480, "gp", "Minor Circlet of Blasting")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 93, 93, new CommonItem(6500, "gp", "Horn of Goodness/Evil")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 94, 94, new CommonItem(6600, "gp", "Shrouds of disintegration")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 95, 95, new CommonItem(7000, "gp", "Robe of Useful Items")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 96, 96, new CommonItem(7200, "gp", "Folding Boat")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 97, 97, new CommonItem(7200, "gp", "Cloak of the Manta Ray")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 98, 98, new CommonItem(7250, "gp", "Bottle of Air")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 99, 99, new CommonItem(7400, "gp", "Bag of Holding (Type III)")));
            item_list.AddTreasure(new ItemType("Minor Wondrous", 100, 100, new CommonItem(7400, "gp", "Periapt of Health")));
            return item_list;
        }

        public static Treasure_Leaf CreateMinorWondrousList()
        {
            return SetupMinorWondrousTypes();
        }

        private static Treasure_Composite SetupMinorItemTypes()
        {
            var treasure_pile = new Treasure_Composite();
            treasure_pile.AddTreasure(new TreasureType("Minor Armour and Shields", 0, 4, CreateMinorArmourShieldList()));
            treasure_pile.AddTreasure(new TreasureType("Minor Weapons", 5, 9, CreateMinorWeaponsList()));
            treasure_pile.AddTreasure(new TreasureType("Minor Potions", 10, 44, CreateMinorPotionsList()));
            treasure_pile.AddTreasure(new TreasureType("Minor Rings", 45, 46, CreateMinorRingsList()));
            treasure_pile.AddTreasure(new TreasureType("Minor Scrolls", 47, 81, CreateMinorScrollList()));
            treasure_pile.AddTreasure(new TreasureType("Minor Wands", 82, 91, CreateMinorWandsList()));
            treasure_pile.AddTreasure(new TreasureType("Minor Wondrous", 92, 100, CreateMinorWondrousList()));
            return treasure_pile;
        }

        public static Treasure_Composite CreateMinorItemList()
        {
            return SetupMinorItemTypes();
        }

        /* Medium MagicItem Methods
         * Create: Weapons, Armour/Shields, Potions, Rings, Rods, Scrolls, Staffs, Wands, and Wondrous Items
         */

        private static Treasure_Leaf SetupMediumPotionTypes()
        {
            var potion_list = new Treasure_Leaf();
            potion_list.AddTreasure(new ItemType("Medium Potion", 0, 2, new CommonItem(100, "gp", "Potion of Bless Weapon")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 3, 4, new CommonItem(250, "gp", "Potion of Enlarge Person")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 5, 5, new CommonItem(250, "gp", "Potion of Reduce Person")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 6, 6, new CommonItem(300, "gp", "Potion of Aid")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 7, 7, new CommonItem(300, "gp", "Potion of Barkskin +2")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 8, 10, new CommonItem(300, "gp", "Potion of Bear's Endurance")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 11, 13, new CommonItem(300, "gp", "Potion of Blur")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 14, 16, new CommonItem(300, "gp", "Potion of Bull's Strength")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 17, 19, new CommonItem(300, "gp", "Potion of Cat's Grace")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 20, 27, new CommonItem(300, "gp", "Potion of Cure Moderate Wounds")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 28, 28, new CommonItem(300, "gp", "Potion of Darkness")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 29, 30, new CommonItem(300, "gp", "Potion of Darkvision")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 31, 31, new CommonItem(300, "gp", "Potion of Delay Poison")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 32, 33, new CommonItem(300, "gp", "Potion of Eagle's Splendor")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 34, 35, new CommonItem(300, "gp", "Potion of Fox's Cunning")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 36, 37, new CommonItem(300, "gp", "Potion of Invisibility")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 38, 38, new CommonItem(300, "gp", "Potion of Lesser Restoration")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 39, 39, new CommonItem(300, "gp", "Potion of Levitate")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 40, 40, new CommonItem(300, "gp", "Potion of Misdirection")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 41, 42, new CommonItem(300, "gp", "Potion of Owl's Wisdom")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 43, 43, new CommonItem(300, "gp", "Potion of Protection from Arrows (10/magic)")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 44, 44, new CommonItem(300, "gp", "Potion of Remove Paralysis")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 45, 46, new CommonItem(300, "gp", "Potion of Resist Energy (10)")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 47, 48, new CommonItem(300, "gp", "Potion of Shield of Faith +3")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 49, 49, new CommonItem(300, "gp", "Potion of Spider Climb")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 50, 50, new CommonItem(300, "gp", "Potion of Undetectable Alignment")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 51, 51, new CommonItem(600, "gp", "Potion of Barkskin +3")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 52, 52, new CommonItem(600, "gp", "Potion of Shield of Faith +4")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 53, 55, new CommonItem(700, "gp", "Potion of Resist Energy")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 56, 60, new CommonItem(750, "gp", "Potion of Cure Serious Wounds")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 61, 61, new CommonItem(750, "gp", "Potion of Daylight")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 62, 64, new CommonItem(750, "gp", "Potion of Displacement")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 65, 65, new CommonItem(750, "gp", "Potion of Flame Arrow")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 66, 68, new CommonItem(750, "gp", "Potion of Fly")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 69, 69, new CommonItem(750, "gp", "Potion of Gaseous Form")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 70, 71, new CommonItem(750, "gp", "Potion of Greater Magic Fang +1")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 72, 73, new CommonItem(750, "gp", "Potion of Greater Magic Weapon +1")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 74, 75, new CommonItem(750, "gp", "Potion of Haste")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 76, 78, new CommonItem(750, "gp", "Potion of Heroism")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 79, 80, new CommonItem(750, "gp", "Potion of Keen Edge")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 81, 81, new CommonItem(750, "gp", "Potion of Magic Circle against Alignment")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 82, 83, new CommonItem(750, "gp", "Potion of Magic Vestment +1")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 84, 86, new CommonItem(750, "gp", "Potion of Neutralize Poison")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 87, 88, new CommonItem(750, "gp", "Potion of Nondetection")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 89, 91, new CommonItem(750, "gp", "Potion of Protection from Energy")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 92, 93, new CommonItem(750, "gp", "Potion of Rage")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 94, 94, new CommonItem(750, "gp", "Potion of Remove Blindness/Deafness")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 95, 95, new CommonItem(750, "gp", "Potion of Remove Curse")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 96, 96, new CommonItem(750, "gp", "Potion of Remove Disease")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 97, 97, new CommonItem(750, "gp", "Potion of Tongues")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 98, 99, new CommonItem(750, "gp", "Potion of Water Breathing")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 99, 100, new CommonItem(750, "gp", "Potion of Water Walk")));
            return potion_list;
        }

        public static Treasure_Leaf CreateMediumPotionsList()
        {
            return SetupMediumPotionTypes();
        }

        private static Treasure_Composite SetupMediumScrollTypes()
        {
            var treasure_pile = new Treasure_Composite();

            var arcane_scrolls = new Treasure_Leaf();
            arcane_scrolls.AddTreasure(new ItemType("Medium Arcane Scroll", 0, 5, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel2ArcaneSpellList())));
            arcane_scrolls.AddTreasure(new ItemType("Medium Arcane Scroll", 6, 65, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel3ArcaneSpellList())));
            arcane_scrolls.AddTreasure(new ItemType("Medium Arcane Scroll", 66, 95, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel4ArcaneSpellList())));
            arcane_scrolls.AddTreasure(new ItemType("Medium Arcane Scroll", 96, 100, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel5ArcaneSpellList())));

            var divine_scrolls = new Treasure_Leaf();
            divine_scrolls.AddTreasure(new ItemType("Medium Divine Scroll", 0, 5, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel2DivineSpellList())));
            divine_scrolls.AddTreasure(new ItemType("Medium Divine Scroll", 6, 65, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel3DivineSpellList())));
            divine_scrolls.AddTreasure(new ItemType("Medium Divine Scroll", 66, 95, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel4DivineSpellList())));
            divine_scrolls.AddTreasure(new ItemType("Medium Divine Scroll", 96, 100, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel5DivineSpellList())));


            treasure_pile.AddTreasure(new TreasureType("Medium Arcane Scroll", 0, 70, arcane_scrolls));
            treasure_pile.AddTreasure(new TreasureType("Medium Divine Scroll", 71, 100, divine_scrolls));
            return treasure_pile;
        }

        public static Treasure_Composite CreateMediumScrollList()
        {
            return SetupMediumScrollTypes();
        }

        private static Treasure_Leaf SetupMediumRodTypes()
        {
            var rod_list = new Treasure_Leaf();
            rod_list.AddTreasure(new ItemType("Medium Rod", 0, 4, new CommonItem(3000, "gp", "Rod of Lesser Metamagic (Enlarge)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 5, 14, new CommonItem(3000, "gp", "Rod of Lesser Metamagic (Extend)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 15, 21, new CommonItem(3000, "gp", "Rod of Lesser Metamagic (Silent)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 22, 28, new CommonItem(5000, "gp", "Immovable Rod")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 29, 35, new CommonItem(9000, "gp", "Rod of Lesser Metamagic (Empower)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 36, 42, new CommonItem(10500, "gp", "Rod of Metal and Mineral Detection")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 43, 53, new CommonItem(11000, "gp", "Rod of Cancellation")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 54, 57, new CommonItem(11000, "gp", "Rod of Metamagic (Enlarge)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 58, 61, new CommonItem(11000, "gp", "Rod of Metamagic (Extend)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 62, 65, new CommonItem(11000, "gp", "Rod of Lesser Metamagic (Silent)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 66, 71, new CommonItem(12000, "gp", "Rod of Wonder")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 72, 79, new CommonItem(13000, "gp", "Rod of Python")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 80, 83, new CommonItem(14000, "gp", "Rod of Lesser Metamagic (Maximize)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 84, 89, new CommonItem(15000, "gp", "Rod of Flame Extinguishing")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 90, 97, new CommonItem(19000, "gp", "Rod of Viper")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 98, 99, new CommonItem(32500, "gp", "Rod of Metamagic (Empower)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 100, 100, new CommonItem(35000, "gp", "Rod of Lesser Metamagic (Quicken)")));
            return rod_list;
        }

        public static Treasure_Leaf CreateMediumRodsList()
        {
            return SetupMediumRodTypes();
        }

        private static Treasure_Composite SetupMediumWeaponsTypes()
        {
            var treasure_pile = new Treasure_Composite();

            var enchantment_1_wep_list = CreateEnchantmentType(1, true);
            var enchantment_2_wep_list = CreateEnchantmentType(2, true);
            var enchantment_3_wep_list = CreateEnchantmentType(3, true);
            var enchantment_4_wep_list = CreateEnchantmentType(4, true);

            var common_melee_list = SetupCommonMeleeWeaponItemTypes();
            var uncommon_melee_list = SetupUncommonMeleeWeaponItemTypes();
            var ranged_list = SetupRangedWeaponItemTypes();


            var special_list = new Treasure_Composite();
            special_list.AddTreasure(new TreasureType("Common Melee", 0, 70, SetupEnchantedItems(common_melee_list, SetupMediumMeleeWeaponEnchantmentTypes())));
            special_list.AddTreasure(new TreasureType("Uncommon Melee", 71, 80, SetupEnchantedItems(uncommon_melee_list, SetupMediumMeleeWeaponEnchantmentTypes())));
            special_list.AddTreasure(new TreasureType("Ranged", 81, 100, SetupEnchantedItems(ranged_list, SetupMediumMeleeWeaponEnchantmentTypes())));

            var bonus_1_list = new Treasure_Composite();
            bonus_1_list.AddTreasure(new TreasureType("Common Melee", 0, 70, SetupEnchantedItems(common_melee_list, enchantment_1_wep_list)));
            bonus_1_list.AddTreasure(new TreasureType("Uncommon Melee", 71, 80, SetupEnchantedItems(uncommon_melee_list, enchantment_1_wep_list)));
            bonus_1_list.AddTreasure(new TreasureType("Ranged", 81, 100, SetupEnchantedItems(ranged_list, enchantment_1_wep_list)));

            var bonus_2_list = new Treasure_Composite();
            bonus_2_list.AddTreasure(new TreasureType("Common Melee", 0, 70, SetupEnchantedItems(common_melee_list, enchantment_2_wep_list)));
            bonus_2_list.AddTreasure(new TreasureType("Uncommon Melee", 71, 80, SetupEnchantedItems(uncommon_melee_list, enchantment_2_wep_list)));
            bonus_2_list.AddTreasure(new TreasureType("Ranged", 81, 100, SetupEnchantedItems(ranged_list, enchantment_2_wep_list)));

            var bonus_3_list = new Treasure_Composite();
            bonus_2_list.AddTreasure(new TreasureType("Common Melee", 0, 70, SetupEnchantedItems(common_melee_list, enchantment_3_wep_list)));
            bonus_2_list.AddTreasure(new TreasureType("Uncommon Melee", 71, 80, SetupEnchantedItems(uncommon_melee_list, enchantment_3_wep_list)));
            bonus_2_list.AddTreasure(new TreasureType("Ranged", 81, 100, SetupEnchantedItems(ranged_list, enchantment_3_wep_list)));

            var bonus_4_list = new Treasure_Composite();
            bonus_2_list.AddTreasure(new TreasureType("Common Melee", 0, 70, SetupEnchantedItems(common_melee_list, enchantment_4_wep_list)));
            bonus_2_list.AddTreasure(new TreasureType("Uncommon Melee", 71, 80, SetupEnchantedItems(uncommon_melee_list, enchantment_4_wep_list)));
            bonus_2_list.AddTreasure(new TreasureType("Ranged", 81, 100, SetupEnchantedItems(ranged_list, enchantment_4_wep_list)));


            var specific_list = new Treasure_Leaf();
            specific_list.AddTreasure(new ItemType("Specific", 0, 9, new CommonItem(1500, "gp", "Javelin of Lightning")));
            specific_list.AddTreasure(new ItemType("Specific", 10, 15, new CommonItem(2282, "gp", "Slaying Arrow")));
            specific_list.AddTreasure(new ItemType("Specific", 16, 24, new CommonItem(3002, "gp", "Adamantine Dagger")));
            specific_list.AddTreasure(new ItemType("Specific", 25, 33, new CommonItem(3010, "gp", "Adamantine Battleaxe")));
            specific_list.AddTreasure(new ItemType("Specific", 34, 37, new CommonItem(4057, "gp", "Greater Slaying Arrow")));
            specific_list.AddTreasure(new ItemType("Specific", 38, 40, new CommonItem(4315, "gp", "Shatterspike")));
            specific_list.AddTreasure(new ItemType("Specific", 41, 46, new CommonItem(8302, "gp", "Dagger of Venom")));
            specific_list.AddTreasure(new ItemType("Specific", 47, 51, new CommonItem(10115, "gp", "Trident of Warning")));
            specific_list.AddTreasure(new ItemType("Specific", 52, 57, new CommonItem(10302, "gp", "Assassin's Dagger")));
            specific_list.AddTreasure(new ItemType("Specific", 58, 62, new CommonItem(12780, "gp", "Shifter's Sorrow")));
            specific_list.AddTreasure(new ItemType("Specific", 63, 66, new CommonItem(18650, "gp", "Trident of Fish Command")));
            specific_list.AddTreasure(new ItemType("Specific", 67, 74, new CommonItem(20715, "gp", "Flame Tongue")));
            specific_list.AddTreasure(new ItemType("Specific", 75, 79, new CommonItem(22060, "gp", "Luck Blade (0 wishes)")));
            specific_list.AddTreasure(new ItemType("Specific", 80, 86, new CommonItem(22310, "gp", "Sword of Subtlety")));
            specific_list.AddTreasure(new ItemType("Specific", 87, 91, new CommonItem(22315, "gp", "Sword of the Planes")));
            specific_list.AddTreasure(new ItemType("Specific", 92, 95, new CommonItem(23057, "gp", "Nine Lives Stealer")));
            specific_list.AddTreasure(new ItemType("Specific", 96, 98, new CommonItem(25715, "gp", "Sword of Life Stealing")));
            specific_list.AddTreasure(new ItemType("Specific", 99, 100, new CommonItem(25600, "gp", "Oathbow")));

            treasure_pile.AddTreasure(new TreasureType("+1 Bonus", 0, 10, bonus_1_list));
            treasure_pile.AddTreasure(new TreasureType("+2 Bonus", 11, 29, bonus_2_list));
            treasure_pile.AddTreasure(new TreasureType("+3 Bonus", 30, 58, bonus_3_list));
            treasure_pile.AddTreasure(new TreasureType("+4 Bonus", 59, 62, bonus_4_list));
            treasure_pile.AddTreasure(new TreasureType("Specific Weapon", 63, 68, specific_list));
            treasure_pile.AddTreasure(new TreasureType("Special Ability", 69, 100, special_list));

            return treasure_pile;
        }

        public static Treasure_Composite CreateMediumWeaponsList()
        {
            return SetupMediumWeaponsTypes();
        }

        private static Treasure_Composite SetupMediumArmourShieldTypes()
        {
            var treasure_pile = new Treasure_Composite();

            // Specific Armour
            var specific_armour_list = new Treasure_Leaf();
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 0, 25, new CommonItem(1100, "gp", "Mithral Shirt")));
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 26, 45, new CommonItem(3300, "gp", "Dragonhide Plate")));
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 46, 57, new CommonItem(4150, "gp", "Elven Chain")));
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 58, 67, new CommonItem(5165, "gp", "Rhino Hide")));
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 68, 82, new CommonItem(10200, "gp", "Adamantine Breastplate")));
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 83, 97, new CommonItem(16500, "gp", "Dwarven Plate")));
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 98, 100, new CommonItem(18900, "gp", "Banded Mail of Luck")));

            // Specific Shield
            var specific_shield_list = new Treasure_Leaf();
            specific_shield_list.AddTreasure(new ItemType("Specific Shield", 0, 20, new CommonItem(205, "gp", "Darkwood Buckler")));
            specific_shield_list.AddTreasure(new ItemType("Specific Shield", 21, 45, new CommonItem(257, "gp", "Darkwood Shield")));
            specific_shield_list.AddTreasure(new ItemType("Specific Shield", 46, 70, new CommonItem(1020, "gp", "Mithral Heavy Shield")));
            specific_shield_list.AddTreasure(new ItemType("Specific Shield", 71, 85, new CommonItem(3153, "gp", "Caster's Shield")));
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 86, 90, new CommonItem(5580, "gp", "Spined Shield")));
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 91, 95, new CommonItem(9170, "gp", "Lion's Shield")));
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 96, 100, new CommonItem(17257, "gp", "Winged Shield")));

            // Special ability compound
            var special_item_list = new Treasure_Composite();
            special_item_list.AddTreasure(new TreasureType("Special Armour", 0, 50, SetupEnchantedItems(SetupArmourTypeToEnchant(), CreateMediumArmourEnchantmentsList())));
            special_item_list.AddTreasure(new TreasureType("Special Shield", 51, 100, SetupEnchantedItems(SetupShieldTypeToEnchant(), CreateMediumShieldEnchantmentsList())));

            treasure_pile.AddTreasure(new TreasureType("+1 Shield", 0, 5, SetupEnchantedItems(SetupShieldTypeToEnchant(), CreateEnchantmentType(1, false))));
            treasure_pile.AddTreasure(new TreasureType("+1 Armour", 6, 10, SetupEnchantedItems(SetupArmourTypeToEnchant(), CreateEnchantmentType(1, false))));
            treasure_pile.AddTreasure(new TreasureType("+2 Shield", 11, 20, SetupEnchantedItems(SetupShieldTypeToEnchant(), CreateEnchantmentType(2, false))));
            treasure_pile.AddTreasure(new TreasureType("+2 Armour", 21, 30, SetupEnchantedItems(SetupArmourTypeToEnchant(), CreateEnchantmentType(2, false))));
            treasure_pile.AddTreasure(new TreasureType("+3 Shield", 31, 40, SetupEnchantedItems(SetupShieldTypeToEnchant(), CreateEnchantmentType(3, false))));
            treasure_pile.AddTreasure(new TreasureType("+3 Armour", 41, 50, SetupEnchantedItems(SetupArmourTypeToEnchant(), CreateEnchantmentType(3, false))));
            treasure_pile.AddTreasure(new TreasureType("+4 Shield", 51, 55, SetupEnchantedItems(SetupShieldTypeToEnchant(), CreateEnchantmentType(4, false))));
            treasure_pile.AddTreasure(new TreasureType("+4 Armour", 56, 57, SetupEnchantedItems(SetupArmourTypeToEnchant(), CreateEnchantmentType(4, false))));
            treasure_pile.AddTreasure(new TreasureType("Specific Armour", 58, 60, specific_armour_list));
            treasure_pile.AddTreasure(new TreasureType("Specific Shield", 61, 63, specific_shield_list));
            treasure_pile.AddTreasure(new TreasureType("Special Ability", 64, 100, special_item_list));

            return treasure_pile;
        }

        public static Treasure_Composite CreateMediumArmourShieldList()
        {
            return SetupMediumArmourShieldTypes();
        }

        private static Treasure_Leaf SetupMediumRingTypes()
        {
            var treasure_pile = new Treasure_Leaf();
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 0, 5, new CommonItem(4000, "gp", "Ring of Counterspells")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 6, 8, new CommonItem(8000, "gp", "Ring of Mind Shielding")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 9, 18, new CommonItem(8000, "gp", "Ring of Protection +2")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 19, 23, new CommonItem(8500, "gp", "Ring of Force Shield")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 24, 28, new CommonItem(8600, "gp", "Ring of Ram")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 29, 34, new CommonItem(10000, "gp", "Ring of Improved Climbing")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 35, 40, new CommonItem(10000, "gp", "Ring of Improved Jumping")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 41, 46, new CommonItem(10000, "gp", "Ring of Improved Swimming")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 47, 51, new CommonItem(10800, "gp", "Ring of Animal Friendship")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 52, 56, new CommonItem(12000, "gp", "Ring of Minor Energy Resistance")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 57, 61, new CommonItem(12700, "gp", "Ring of Chameleon Power")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 62, 66, new CommonItem(15000, "gp", "Ring of Water Walking")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 67, 71, new CommonItem(18000, "gp", "Ring of Protection +3")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 72, 76, new CommonItem(18000, "gp", "Ring of Minor Spell Storing")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 77, 81, new CommonItem(20000, "gp", "Ring of Invisibility")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 82, 85, new CommonItem(20000, "gp", "Ring of Wizardry (I)")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 86, 89, new CommonItem(25000, "gp", "Ring of Evasion")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 90, 92, new CommonItem(25000, "gp", "Ring of X-Ray Vision")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 93, 95, new CommonItem(27000, "gp", "Ring of Blinking")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 96, 98, new CommonItem(27000, "gp", "Ring of Meld into Stone")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 99, 100, new CommonItem(28000, "gp", "Ring of Major Energy Resistance")));
            return treasure_pile;
        }

        public static Treasure_Leaf CreateMediumRingsList()
        {
            return SetupMediumRingTypes();
        }

        private static Treasure_Leaf SetupMediumWandTypes()
        {
            var wand_list = new Treasure_Leaf();
            wand_list.AddTreasure(new ItemType("Medium Wand", 0, 3, new CommonItem(3750, "gp", "Wand of Magic Missile (5th level)")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 4, 7, new CommonItem(4500, "gp", "Wand of Bear's Endurance")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 8, 11, new CommonItem(4500, "gp", "Wand of Bull's Strength")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 12, 15, new CommonItem(4500, "gp", "Wand of Cat's Grace")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 16, 20, new CommonItem(4500, "gp", "Wand of Cure Moderate Wounds")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 21, 22, new CommonItem(4500, "gp", "Wand of Darkness")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 23, 24, new CommonItem(4500, "gp", "Wand of Daze Monster")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 25, 27, new CommonItem(4500, "gp", "Wand of Delay Poison")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 28, 31, new CommonItem(4500, "gp", "Wand of Eagle's Splendor")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 32, 33, new CommonItem(4500, "gp", "Wand of False Life")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 34, 37, new CommonItem(4500, "gp", "Wand of Fox's Cunning")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 38, 38, new CommonItem(4500, "gp", "Wand of Ghoul Touch")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 39, 39, new CommonItem(4500, "gp", "Wand of Hold Person")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 40, 42, new CommonItem(4500, "gp", "Wand of Invisibility")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 43, 44, new CommonItem(4500, "gp", "Wand of Knock")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 45, 45, new CommonItem(4500, "gp", "Wand of Levitate")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 46, 47, new CommonItem(4500, "gp", "Wand of Acid Arrow")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 48, 49, new CommonItem(4500, "gp", "Wand of Mirror Image")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 50, 53, new CommonItem(4500, "gp", "Wand of Owl's Wisdom")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 54, 54, new CommonItem(4500, "gp", "Wand of Shatter")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 55, 56, new CommonItem(4500, "gp", "Wand of Silence")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 57, 57, new CommonItem(4500, "gp", "Wand of Summon Monster II")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 58, 59, new CommonItem(4500, "gp", "Wand of Web")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 60, 62, new CommonItem(5250, "gp", "Wand of Magic Missile (7th level)")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 63, 64, new CommonItem(6750, "gp", "Wand of Magic Missile (9th level)")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 65, 67, new CommonItem(11250, "gp", "Wand of Call Lightning (5th level)")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 68, 68, new CommonItem(11250, "gp", "Wand of Charm Person, Heightened (3rd level)")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 69, 70, new CommonItem(11250, "gp", "Wand of Contagion")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 71, 74, new CommonItem(11250, "gp", "Wand of Cure Serious Wounds")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 75, 77, new CommonItem(11250, "gp", "Wand of Dispel Magic")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 78, 81, new CommonItem(11250, "gp", "Wand of Fireball (5th level)")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 82, 83, new CommonItem(11250, "gp", "Wand of Keen Edge")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 84, 87, new CommonItem(11250, "gp", "Wand of Lightning Bolt (5th level)")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 88, 89, new CommonItem(11250, "gp", "Wand of Major Image")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 90, 91, new CommonItem(11250, "gp", "Wand of Slow")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 92, 94, new CommonItem(11250, "gp", "Wand of Suggestion")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 95, 97, new CommonItem(11250, "gp", "Wand of Summon Monster III")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 98, 98, new CommonItem(13500, "gp", "Wand of Fireball (6th level)")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 99, 99, new CommonItem(13500, "gp", "Wand of Lightning Bolt (6th)")));
            wand_list.AddTreasure(new ItemType("Medium Wand", 100, 100, new CommonItem(13500, "gp", "Wand of Searing Light (6th Level)")));
            return wand_list;
        }

        public static Treasure_Leaf CreateMediumWandsList()
        {
            return SetupMediumWandTypes();
        }

        private static Treasure_Leaf SetupMediumStaffTypes()
        {
            var staff_list = new Treasure_Leaf();
            staff_list.AddTreasure(new ItemType("Medium Staff", 0, 15, new CommonItem(16500, "gp", "Staff of Charming")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 16, 30, new CommonItem(28500, "gp", "Staff of Fire")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 31, 40, new CommonItem(24750, "gp", "Staff of Swarming Insects")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 41, 60, new CommonItem(27750, "gp", "Staff of Healing")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 61, 75, new CommonItem(29000, "gp", "Staff of Size Alteration")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 76, 90, new CommonItem(48250, "gp", "Staff of Illumination")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 91, 95, new CommonItem(56250, "gp", "Staff of Frost")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 96, 100, new CommonItem(58250, "gp", "Staff of Defense")));
            return staff_list;
        }

        public static Treasure_Leaf CreateMediumStaffsList()
        {
            return SetupMediumStaffTypes();
        }

        private static Treasure_Leaf SetupMediumWondrousTypes()
        {
            var item_list = new Treasure_Leaf();
            item_list.AddTreasure(new ItemType("Medium Wondrous", 0, 1, new CommonItem(7500, "gp", "Boots of Levitation")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 2, 2, new CommonItem(7500, "gp", "Harp of Charming")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 3, 3, new CommonItem(8000, "gp", "Amulet of Natural Armour +2")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 4, 4, new CommonItem(8000, "gp", "Flesh Golem Manual")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 5, 5, new CommonItem(8000, "gp", "Hand of Glory")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 6, 6, new CommonItem(8000, "gp", "Ioun Stone, Deep Red Sphere")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 7, 7, new CommonItem(8000, "gp", "Ioun Stone, Incandescent Blue Sphere")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 8, 8, new CommonItem(8000, "gp", "Ioun Stone, Pale Blue Rhomboid")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 9, 9, new CommonItem(8000, "gp", "Ioun Stone, Pink and Green Sphere")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 10, 10, new CommonItem(8000, "gp", "Ioun Stone, Pink Rhomboid")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 11, 11, new CommonItem(8000, "gp", "Ioun Stone, Scarlet and Blue Sphere")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 12, 12, new CommonItem(8100, "gp", "Deck of Illusions")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 13, 13, new CommonItem(8100, "gp", "Necklace of Fireballs (Type VI)")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 14, 14, new CommonItem(8400, "gp", "Candle of Invocation")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 15, 15, new CommonItem(8700, "gp", "Necklace of Fireballs (Type VII)")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 16, 16, new CommonItem(9000, "gp", "Bracers of Armour +3")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 17, 17, new CommonItem(9000, "gp", "Cloak of Resistance +3")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 18, 18, new CommonItem(9000, "gp", "Decanter of Endless Water")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 19, 19, new CommonItem(9000, "gp", "Necklace of Adaptation")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 20, 20, new CommonItem(9000, "gp", "Pearl of Power (3rd level)")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 21, 21, new CommonItem(9100, "gp", "Serpentine Owl Figurine of Wondrous Power")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 22, 22, new CommonItem(9600, "gp", "Lesser Strand of Prayer Beads")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 23, 23, new CommonItem(10000, "gp", "Bag of Holding (Type IV)")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 24, 24, new CommonItem(10000, "gp", "Bronze Griffon Figurine of Wondrous Power")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 25, 25, new CommonItem(10000, "gp", "Ebony Fly Figurine of Wondrous Power")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 26, 26, new CommonItem(10000, "gp", "Glove of Storing")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 27, 27, new CommonItem(10000, "gp", "Ioun Stone, Dark Blue Rhomboid")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 28, 28, new CommonItem(10000, "gp", "Courser Stone Horse")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 29, 29, new CommonItem(10000, "gp", "Druid's Vestment")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 30, 30, new CommonItem(10080, "gp", "Cape of the Mountebank")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 31, 31, new CommonItem(11000, "gp", "Phylactery of Undead Turning")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 32, 32, new CommonItem(11500, "gp", "Gauntlet of Rust")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 33, 33, new CommonItem(12000, "gp", "Boots of Speed")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 34, 34, new CommonItem(12000, "gp", "Goggles of Night")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 35, 35, new CommonItem(12000, "gp", "Clay Golem Manual")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 36, 36, new CommonItem(12000, "gp", "Medallion of Thoughts")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 37, 37, new CommonItem(12000, "gp", "Pipes of Pain")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 38, 38, new CommonItem(12500, "gp", "Blessed Book")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 39, 39, new CommonItem(13000, "gp", "Monk's Belt")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 40, 40, new CommonItem(13000, "gp", "Gem of Brightness")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 41, 41, new CommonItem(13000, "gp", "Lyre of Building")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 42, 42, new CommonItem(14000, "gp", "Cloak of Arachnida")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 43, 43, new CommonItem(14800, "gp", "Destrier Stone Horse")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 44, 44, new CommonItem(14900, "gp", "Belt of Dwarvenkind")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 45, 45, new CommonItem(15000, "gp", "Periapt of Wound Closure")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 46, 46, new CommonItem(15100, "gp", "Horn of the Tritons")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 47, 47, new CommonItem(15300, "gp", "Pearl of the Sirines")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 48, 48, new CommonItem(15500, "gp", "Onyx Dog Figurine of Wondrous Power")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 49, 49, new CommonItem(16000, "gp", "Amulet of Health +4")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 50, 50, new CommonItem(16000, "gp", "Belt of Giant's Strength +4")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 51, 51, new CommonItem(16000, "gp", "Winged Boots")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 52, 52, new CommonItem(16000, "gp", "Bracers of Armour +4")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 53, 53, new CommonItem(16000, "gp", "Cloak of Charisma +4")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 54, 54, new CommonItem(16000, "gp", "Cloak of Resistance +4")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 55, 55, new CommonItem(16000, "gp", "Gloves of Dexterity +4")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 56, 56, new CommonItem(16000, "gp", "Headband of Intellect +4")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 57, 57, new CommonItem(16000, "gp", "Pearl of Power (4th level)")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 58, 58, new CommonItem(16000, "gp", "Periapt of Wisdom +4")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 59, 59, new CommonItem(16000, "gp", "Scabbard of Keen Edges")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 60, 60, new CommonItem(16500, "gp", "Golden Lions Figurine of Wondrous Power")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 61, 61, new CommonItem(16800, "gp", "Chime of Interruption")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 62, 62, new CommonItem(17000, "gp", "Broom of Flying")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 63, 63, new CommonItem(17000, "gp", "Marble Elephant Figurine of Wondrous Power")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 64, 64, new CommonItem(18000, "gp", "Amulet of Natural Armour +3")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 65, 65, new CommonItem(18000, "gp", "Ioun Stone, Iridescent Spindle")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 66, 66, new CommonItem(19000, "gp", "Bracelet of Friends")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 67, 67, new CommonItem(20000, "gp", "Carpet of Flying (5ft.x5ft.)")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 68, 68, new CommonItem(20000, "gp", "Horn of Blasting")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 69, 69, new CommonItem(20000, "gp", "Ioun Stone, Pale Lavender Ellipsoid")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 70, 70, new CommonItem(20000, "gp", "Ioun Stone, Pearly White Spindle")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 71, 71, new CommonItem(20000, "gp", "Portable Hole")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 72, 72, new CommonItem(20000, "gp", "Stone of Good Luck")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 73, 73, new CommonItem(21000, "gp", "Ivory Goats Figurine of Wondrous Power")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 74, 74, new CommonItem(21000, "gp", "Rope of Entanglement")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 75, 75, new CommonItem(22000, "gp", "Stone Golem Manual")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 76, 76, new CommonItem(22000, "gp", "Mask of the Skull")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 77, 77, new CommonItem(23348, "gp", "Mattock of the Titans")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 78, 78, new CommonItem(23760, "gp", "Major Circlet of Blasting")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 79, 79, new CommonItem(24000, "gp", "Amulet of Mighty Fists +2")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 80, 80, new CommonItem(24000, "gp", "Minor Cloak of Displacement")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 81, 81, new CommonItem(24000, "gp", "Helm of Underwater Action")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 82, 82, new CommonItem(25000, "gp", "Greater Bracers of Archery")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 83, 83, new CommonItem(25000, "gp", "Bracers of Armour +5")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 84, 84, new CommonItem(25000, "gp", "Cloak of Resistance +5")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 85, 85, new CommonItem(25000, "gp", "Eyes of Doom")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 86, 86, new CommonItem(25000, "gp", "Pearl of Power (5th level)")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 87, 87, new CommonItem(25305, "gp", "Maul of the Titans")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 88, 88, new CommonItem(45800, "gp", "Strand of Prayer Beads")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 89, 89, new CommonItem(26000, "gp", "Cloak of the Bat")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 90, 90, new CommonItem(26000, "gp", "Iron Bands of Binding")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 91, 91, new CommonItem(27000, "gp", "Cube of Frost Resistance")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 92, 92, new CommonItem(27000, "gp", "Helm of Telepathy")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 93, 93, new CommonItem(27000, "gp", "Periapt of Proof Against Poison")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 94, 94, new CommonItem(27000, "gp", "Robe of Scantillating Colours")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 95, 95, new CommonItem(27500, "gp", "Manual of Bodily Health +1")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 96, 96, new CommonItem(27500, "gp", "Manual of Gainful Exercise +1")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 97, 97, new CommonItem(27500, "gp", "Manual of Quickness of Action +1")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 98, 98, new CommonItem(27500, "gp", "Tome of Clear Thought +1")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 99, 99, new CommonItem(27500, "gp", "Tome of Leadership and Influence +1")));
            item_list.AddTreasure(new ItemType("Medium Wondrous", 100, 100, new CommonItem(27500, "gp", "Tome of Understanding +1")));
            return item_list;
        }

        public static Treasure_Leaf CreateMediumWondrousList()
        {
            return SetupMediumWondrousTypes();
        }

        private static Treasure_Composite SetupMediumItemTypes()
        {
            var treasure_pile = new Treasure_Composite();
            treasure_pile.AddTreasure(new TreasureType("Medium Armour and Shields", 0, 10, CreateMediumArmourShieldList()));
            treasure_pile.AddTreasure(new TreasureType("Medium Weapons", 11, 20, CreateMediumWeaponsList()));
            treasure_pile.AddTreasure(new TreasureType("Medium Potions", 21, 30, CreateMediumPotionsList()));
            treasure_pile.AddTreasure(new TreasureType("Medium Rings", 31, 40, CreateMediumRingsList()));
            treasure_pile.AddTreasure(new TreasureType("Medium Rods", 41, 50, CreateMediumRodsList()));
            treasure_pile.AddTreasure(new TreasureType("Medium Scrolls", 51, 65, CreateMediumScrollList()));
            treasure_pile.AddTreasure(new TreasureType("Medium Staffs", 66, 68, CreateMediumStaffsList()));
            treasure_pile.AddTreasure(new TreasureType("Medium Wands", 69, 83, CreateMediumWandsList()));
            treasure_pile.AddTreasure(new TreasureType("Medium Wondrous", 84, 100, CreateMediumWondrousList()));
            return treasure_pile;
        }

        public static Treasure_Composite CreateMediumItemList()
        {
            return SetupMediumItemTypes();
        }

        /* Medium MagicItem Methods
         * Create: Weapons, Armour/Shields, Potions, Rings, Rods, Scrolls, Staffs, Wands, and Wondrous Items
         */

        private static Treasure_Leaf SetupMajorPotionTypes()
        {
            var potion_list = new Treasure_Leaf();
            potion_list.AddTreasure(new ItemType("Medium Potion", 0, 2, new CommonItem(300, "gp", "Potion of Blur")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 3, 7, new CommonItem(300, "gp", "Potion of Cure Moderate Wounds")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 8, 9, new CommonItem(300, "gp", "Potion of Darkvision")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 10, 11, new CommonItem(300, "gp", "Potion of Invisibility")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 12, 12, new CommonItem(300, "gp", "Potion of Lesser Restoration")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 13, 13, new CommonItem(300, "gp", "Potion of Remove Paralysis")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 14, 14, new CommonItem(300, "gp", "Potion of Shield of Faith +3")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 15, 15, new CommonItem(300, "gp", "Potion of Undetectable Alignment")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 16, 16, new CommonItem(600, "gp", "Potion of Barkskin +3")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 17, 18, new CommonItem(600, "gp", "Potion of Shield of Faith +4")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 19, 20, new CommonItem(700, "gp", "Potion of Resist Energy")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 21, 28, new CommonItem(750, "gp", "Potion of Cure Serious Wounds")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 29, 29, new CommonItem(750, "gp", "Potion of Daylight")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 30, 32, new CommonItem(750, "gp", "Potion of Displacement")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 33, 33, new CommonItem(750, "gp", "Potion of Flame Arrow")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 34, 38, new CommonItem(750, "gp", "Potion of Fly")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 39, 39, new CommonItem(750, "gp", "Potion of Gaseous Form")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 40, 41, new CommonItem(750, "gp", "Potion of Haste")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 42, 44, new CommonItem(750, "gp", "Potion of Heroism")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 45, 46, new CommonItem(750, "gp", "Potion of Keen Edge")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 47, 47, new CommonItem(750, "gp", "Potion of Magic Circle against Alignment")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 48, 50, new CommonItem(750, "gp", "Potion of Neutralize Poison")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 51, 52, new CommonItem(750, "gp", "Potion of Nondetection")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 53, 54, new CommonItem(750, "gp", "Potion of Protection from Energy")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 55, 55, new CommonItem(750, "gp", "Potion of Rage")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 56, 56, new CommonItem(750, "gp", "Potion of Remove Blindness/Deafness")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 57, 57, new CommonItem(750, "gp", "Potion of Remove Curse")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 58, 58, new CommonItem(750, "gp", "Potion of Remove Disease")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 59, 59, new CommonItem(700, "gp", "Potion of Tongues")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 60, 60, new CommonItem(750, "gp", "Potion of Water Breathing")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 61, 61, new CommonItem(750, "gp", "Potion of Water Walk")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 62, 63, new CommonItem(900, "gp", "Potion of Barkskin +4")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 64, 64, new CommonItem(900, "gp", "Potion of Shield of Faith +5")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 65, 65, new CommonItem(1050, "gp", "Potion of Good Hope")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 66, 68, new CommonItem(1100, "gp", "Potion of Resist Energy (30)")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 69, 69, new CommonItem(1200, "gp", "Potion of Barkskin +5")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 70, 73, new CommonItem(1200, "gp", "Potion of Greater Magic Fang +2")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 74, 77, new CommonItem(1200, "gp", "Potion of Greater Magic Weapon +2")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 78, 81, new CommonItem(1200, "gp", "Potion of Magic Vestment +2")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 82, 82, new CommonItem(1500, "gp", "Potion of Protection from Arrows (15/magic)")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 83, 85, new CommonItem(1800, "gp", "Potion of Greater Magic Fang +3")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 86, 88, new CommonItem(1800, "gp", "Potion of Greater Magic Weapon +3")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 89, 91, new CommonItem(1800, "gp", "Potion of Magic Vestment +3")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 92, 93, new CommonItem(2400, "gp", "Potion of Greater Magic Fang +4")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 94, 95, new CommonItem(2400, "gp", "Potion of Greater Magic Weapon +4")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 96, 97, new CommonItem(2400, "gp", "Potion of Magic Vestment +4")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 98, 98, new CommonItem(3000, "gp", "Potion of Greater Magic Fang +5")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 99, 99, new CommonItem(3000, "gp", "Potion of Greater Magic Weapon +5")));
            potion_list.AddTreasure(new ItemType("Medium Potion", 100, 100, new CommonItem(3000, "gp", "Potion of Magic Vestment +5")));
            return potion_list;
        }

        public static Treasure_Leaf CreateMajorPotionsList()
        {
            return SetupMajorPotionTypes();
        }

        private static Treasure_Composite SetupMajorScrollTypes()
        {
            var treasure_pile = new Treasure_Composite();

            var arcane_scrolls = new Treasure_Leaf();
            arcane_scrolls.AddTreasure(new ItemType("Medium Arcane Scroll", 0, 5, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel4ArcaneSpellList())));
            arcane_scrolls.AddTreasure(new ItemType("Medium Arcane Scroll", 6, 50, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel5ArcaneSpellList())));
            arcane_scrolls.AddTreasure(new ItemType("Medium Arcane Scroll", 51, 70, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel6ArcaneSpellList())));
            arcane_scrolls.AddTreasure(new ItemType("Medium Arcane Scroll", 71, 85, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel7ArcaneSpellList())));
            arcane_scrolls.AddTreasure(new ItemType("Medium Arcane Scroll", 86, 95, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel8ArcaneSpellList())));
            arcane_scrolls.AddTreasure(new ItemType("Medium Arcane Scroll", 96, 100, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel8ArcaneSpellList())));

            var divine_scrolls = new Treasure_Leaf();
            divine_scrolls.AddTreasure(new ItemType("Medium Divine Scroll", 0, 5, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel4DivineSpellList())));
            divine_scrolls.AddTreasure(new ItemType("Medium Divine Scroll", 6, 50, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel5DivineSpellList())));
            divine_scrolls.AddTreasure(new ItemType("Medium Divine Scroll", 51, 70, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel6DivineSpellList())));
            divine_scrolls.AddTreasure(new ItemType("Medium Divine Scroll", 71, 85, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel7DivineSpellList())));
            divine_scrolls.AddTreasure(new ItemType("Medium Divine Scroll", 86, 95, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel8DivineSpellList())));
            divine_scrolls.AddTreasure(new ItemType("Medium Divine Scroll", 96, 100, new MagicItem(0, "gp", "Scroll of ", Treasure_Factory.SetupLevel9DivineSpellList())));


            treasure_pile.AddTreasure(new TreasureType("Medium Arcane Scroll", 0, 70, arcane_scrolls));
            treasure_pile.AddTreasure(new TreasureType("Medium Divine Scroll", 71, 100, divine_scrolls));
            return treasure_pile;
        }

        public static Treasure_Composite CreateMajorScrollList()
        {
            return SetupMajorScrollTypes();
        }

        private static Treasure_Leaf SetupMajorRodTypes()
        {
            var rod_list = new Treasure_Leaf();
            rod_list.AddTreasure(new ItemType("Medium Rod", 0, 4, new CommonItem(11000, "gp", "Rod of Cancellation")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 5, 6, new CommonItem(11000, "gp", "Rod of Metamagic (Enlarge)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 7, 8, new CommonItem(11000, "gp", "Rod of Metamagic (Extend)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 9, 10, new CommonItem(11000, "gp", "Rod of Lesser Metamagic (Silent)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 11, 14, new CommonItem(12000, "gp", "Rod of Wonder")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 15, 18, new CommonItem(13000, "gp", "Rod of Python")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 19, 21, new CommonItem(15000, "gp", "Rod of Flame Extinguishing")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 22, 25, new CommonItem(19000, "gp", "Rod of Viper")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 26, 30, new CommonItem(23500, "gp", "Rod of Enemy Detection")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 31, 36, new CommonItem(24500, "gp", "Rod of Greater Metamagic (Enlarge)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 37, 42, new CommonItem(24500, "gp", "Rod of Greater Metamagic (Extend)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 43, 48, new CommonItem(24500, "gp", "Rod of Greater Metamagic (Silent)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 49, 53, new CommonItem(25000, "gp", "Rod of Splendor")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 54, 58, new CommonItem(25000, "gp", "Rod of Withering")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 59, 64, new CommonItem(32500, "gp", "Rod of Metamagic (Empower)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 65, 69, new CommonItem(33000, "gp", "Rod of Thunder and Lightning")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 70, 73, new CommonItem(35000, "gp", "Rod of Lesser Metamagic (Quicken)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 74, 77, new CommonItem(37000, "gp", "Rod of Negation")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 78, 80, new CommonItem(50000, "gp", "Rod of Absorption")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 81, 84, new CommonItem(50000, "gp", "Rod of Flailing")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 85, 86, new CommonItem(54000, "gp", "Metamagic (Maximize)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 87, 88, new CommonItem(60000, "gp", "Rod of Rulership")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 89, 90, new CommonItem(61000, "gp", "Rod of Security")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 91, 92, new CommonItem(70000, "gp", "Rod of Lordly Might")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 93, 94, new CommonItem(73000, "gp", "Rod of Greater Metamagic (Empower)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 95, 96, new CommonItem(75500, "gp", "Rod of Metamagic (Quicken)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 97, 98, new CommonItem(85000, "gp", "Alertness")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 99, 99, new CommonItem(121500, "gp", "Rod of Greater Metamagic (Maximize)")));
            rod_list.AddTreasure(new ItemType("Medium Rod", 100, 100, new CommonItem(170000, "gp", "Rod of Greater Metamagic (Quicken)")));
            return rod_list;
        }

        public static Treasure_Leaf CreateMajorRodsList()
        {
            return SetupMajorRodTypes();
        }

        private static Treasure_Composite SetupMajorWeaponsTypes()
        {
            var treasure_pile = new Treasure_Composite();

            var enchantment_3_wep_list = CreateEnchantmentType(3, true);
            var enchantment_4_wep_list = CreateEnchantmentType(4, true);
            var enchantment_5_wep_list = CreateEnchantmentType(5, true);

            var common_melee_list = SetupCommonMeleeWeaponItemTypes();
            var uncommon_melee_list = SetupUncommonMeleeWeaponItemTypes();
            var ranged_list = SetupRangedWeaponItemTypes();


            var special_list = new Treasure_Composite();
            special_list.AddTreasure(new TreasureType("Common Melee", 0, 70, SetupEnchantedItems(common_melee_list, SetupMajorMeleeWeaponEnchantmentTypes())));
            special_list.AddTreasure(new TreasureType("Uncommon Melee", 71, 80, SetupEnchantedItems(uncommon_melee_list, SetupMajorMeleeWeaponEnchantmentTypes())));
            special_list.AddTreasure(new TreasureType("Ranged", 81, 100, SetupEnchantedItems(ranged_list, SetupMajorMeleeWeaponEnchantmentTypes())));

            var bonus_3_list = new Treasure_Composite();
            bonus_3_list.AddTreasure(new TreasureType("Common Melee", 0, 70, SetupEnchantedItems(common_melee_list, enchantment_3_wep_list)));
            bonus_3_list.AddTreasure(new TreasureType("Uncommon Melee", 71, 80, SetupEnchantedItems(uncommon_melee_list, enchantment_3_wep_list)));
            bonus_3_list.AddTreasure(new TreasureType("Ranged", 81, 100, SetupEnchantedItems(ranged_list, enchantment_3_wep_list)));

            var bonus_4_list = new Treasure_Composite();
            bonus_4_list.AddTreasure(new TreasureType("Common Melee", 0, 70, SetupEnchantedItems(common_melee_list, enchantment_4_wep_list)));
            bonus_4_list.AddTreasure(new TreasureType("Uncommon Melee", 71, 80, SetupEnchantedItems(uncommon_melee_list, enchantment_4_wep_list)));
            bonus_4_list.AddTreasure(new TreasureType("Ranged", 81, 100, SetupEnchantedItems(ranged_list, enchantment_4_wep_list)));

            var bonus_5_list = new Treasure_Composite();
            bonus_5_list.AddTreasure(new TreasureType("Common Melee", 0, 70, SetupEnchantedItems(common_melee_list, enchantment_5_wep_list)));
            bonus_5_list.AddTreasure(new TreasureType("Uncommon Melee", 71, 80, SetupEnchantedItems(uncommon_melee_list, enchantment_5_wep_list)));
            bonus_5_list.AddTreasure(new TreasureType("Ranged", 81, 100, SetupEnchantedItems(ranged_list, enchantment_5_wep_list)));
            
            var specific_list = new Treasure_Leaf();
            specific_list.AddTreasure(new ItemType("Specific", 0, 4, new CommonItem(10302, "gp", "Assassin's Dagger")));
            specific_list.AddTreasure(new ItemType("Specific", 5, 7, new CommonItem(12780, "gp", "Shifter's Sorrow")));
            specific_list.AddTreasure(new ItemType("Specific", 8, 9, new CommonItem(18650, "gp", "Trident of Fish Command")));
            specific_list.AddTreasure(new ItemType("Specific", 10, 13, new CommonItem(20715, "gp", "Flame Tongue")));
            specific_list.AddTreasure(new ItemType("Specific", 14, 17, new CommonItem(22060, "gp", "Luck Blade (0 Wishes)")));
            specific_list.AddTreasure(new ItemType("Specific", 18, 24, new CommonItem(22310, "gp", "Sword of Subtlety")));
            specific_list.AddTreasure(new ItemType("Specific", 25, 31, new CommonItem(22315, "gp", "Sword of the Planes")));
            specific_list.AddTreasure(new ItemType("Specific", 32, 37, new CommonItem(23057, "gp", "Nine Lives Stealer")));
            specific_list.AddTreasure(new ItemType("Specific", 38, 42, new CommonItem(25715, "gp", "Sword of Life Stealing")));
            specific_list.AddTreasure(new ItemType("Specific", 43, 46, new CommonItem(25600, "gp", "Oathbow")));
            specific_list.AddTreasure(new ItemType("Specific", 47, 51, new CommonItem(38552, "gp", "Mace of Terror")));
            specific_list.AddTreasure(new ItemType("Specific", 52, 57, new CommonItem(40320, "gp", "Life-Drinker")));
            specific_list.AddTreasure(new ItemType("Specific", 58, 62, new CommonItem(47315, "gp", "Sylvan Scimitar")));
            specific_list.AddTreasure(new ItemType("Specific", 63, 67, new CommonItem(50320, "gp", "Rapier of Puncturing")));
            specific_list.AddTreasure(new ItemType("Specific", 68, 73, new CommonItem(50335, "gp", "Sun Blade")));
            specific_list.AddTreasure(new ItemType("Specific", 74, 79, new CommonItem(54475, "gp", "Frost Brand")));
            specific_list.AddTreasure(new ItemType("Specific", 80, 84, new CommonItem(60312, "gp", "Dwarven Thrower")));
            specific_list.AddTreasure(new ItemType("Specific", 85, 91, new CommonItem(62360, "gp", "Luck Blade (1 Wish)")));
            specific_list.AddTreasure(new ItemType("Specific", 92, 95, new CommonItem(75312, "gp", "Mace of Smiting")));
            specific_list.AddTreasure(new ItemType("Specific", 96, 97, new CommonItem(102660, "gp", "Luck Blade (2 Wishes)")));
            specific_list.AddTreasure(new ItemType("Specific", 98, 99, new CommonItem(120630, "gp", "Holy Avenger")));
            specific_list.AddTreasure(new ItemType("Specific", 100, 100, new CommonItem(142960, "gp", "Luck Blade (3 Wishes)")));

            treasure_pile.AddTreasure(new TreasureType("+3 Bonus", 0, 20, bonus_3_list));
            treasure_pile.AddTreasure(new TreasureType("+4 Bonus", 21, 38, bonus_4_list));
            treasure_pile.AddTreasure(new TreasureType("+5 Bonus", 39, 49, bonus_5_list));
            treasure_pile.AddTreasure(new TreasureType("Specific Weapon", 50, 63, specific_list));
            treasure_pile.AddTreasure(new TreasureType("Special Ability", 64, 100, special_list));

            return treasure_pile;
        }

        public static Treasure_Composite CreateMajorWeaponsList()
        {
            return SetupMajorWeaponsTypes();
        }

        private static Treasure_Composite SetupMajorArmourShieldTypes()
        {
            var treasure_pile = new Treasure_Composite();

            // Specific Armour
            var specific_armour_list = new Treasure_Leaf();
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 0, 10, new CommonItem(10200, "gp", "Adamantine Breastplate")));
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 11, 20, new CommonItem(16500, "gp", "Dwarven Plate")));
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 21, 32, new CommonItem(18900, "gp", "Banded Mail of Luck")));
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 33, 50, new CommonItem(22400, "gp", "Celestial Armour")));
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 51, 60, new CommonItem(24650, "gp", "Plate Armour of the Deep")));
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 61, 75, new CommonItem(25400, "gp", "Breastplate of Command")));
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 76, 90, new CommonItem(26500, "gp", "Mithral Full Plate of Speed")));
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 91, 100, new CommonItem(52260, "gp", "Demon Armour")));

            // Specific Shield
            var specific_shield_list = new Treasure_Leaf();
            specific_shield_list.AddTreasure(new ItemType("Specific Shield", 0, 20, new CommonItem(3153, "gp", "Caster's Shield")));
            specific_shield_list.AddTreasure(new ItemType("Specific Shield", 21, 40, new CommonItem(5580, "gp", "Spined Shield")));
            specific_shield_list.AddTreasure(new ItemType("Specific Shield", 41, 60, new CommonItem(9170, "gp", "Lion's Shield")));
            specific_shield_list.AddTreasure(new ItemType("Specific Shield", 61, 90, new CommonItem(17257, "gp", "Winged Shield")));
            specific_armour_list.AddTreasure(new ItemType("Specific Armour", 91, 100, new CommonItem(50170, "gp", "Absorbing Shield")));

            // Special ability compound
            var special_item_list = new Treasure_Composite();
            special_item_list.AddTreasure(new TreasureType("Special Armour", 0, 50, SetupEnchantedItems(SetupArmourTypeToEnchant(), CreateMajorArmourEnchantmentsList())));
            special_item_list.AddTreasure(new TreasureType("Special Shield", 51, 100, SetupEnchantedItems(SetupShieldTypeToEnchant(), CreateMajorShieldEnchantmentsList())));

            treasure_pile.AddTreasure(new TreasureType("+3 Shield", 0, 8, SetupEnchantedItems(SetupShieldTypeToEnchant(), CreateEnchantmentType(3, false))));
            treasure_pile.AddTreasure(new TreasureType("+3 Armour", 9, 16, SetupEnchantedItems(SetupArmourTypeToEnchant(), CreateEnchantmentType(3, false))));
            treasure_pile.AddTreasure(new TreasureType("+4 Shield", 17, 27, SetupEnchantedItems(SetupShieldTypeToEnchant(), CreateEnchantmentType(4, false))));
            treasure_pile.AddTreasure(new TreasureType("+4 Armour", 28, 38, SetupEnchantedItems(SetupArmourTypeToEnchant(), CreateEnchantmentType(4, false))));
            treasure_pile.AddTreasure(new TreasureType("+5 Shield", 39, 49, SetupEnchantedItems(SetupShieldTypeToEnchant(), CreateEnchantmentType(5, false))));
            treasure_pile.AddTreasure(new TreasureType("+5 Armour", 50, 57, SetupEnchantedItems(SetupArmourTypeToEnchant(), CreateEnchantmentType(5, false))));
            treasure_pile.AddTreasure(new TreasureType("Specific Armour", 58, 60, specific_armour_list));
            treasure_pile.AddTreasure(new TreasureType("Specific Shield", 61, 63, specific_shield_list));
            treasure_pile.AddTreasure(new TreasureType("Special Ability", 64, 100, special_item_list));

            return treasure_pile;
        }

        public static Treasure_Composite CreateMajorArmourShieldList()
        {
            return SetupMajorArmourShieldTypes();
        }

        private static Treasure_Leaf SetupMajorRingTypes()
        {
            var treasure_pile = new Treasure_Leaf();
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 0, 2, new CommonItem(12000, "gp", "Ring of Minor Energy Resistance")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 3, 7, new CommonItem(18000, "gp", "Ring of Protection +3")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 8, 10, new CommonItem(18000, "gp", "Ring of Minor Spell Storing")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 11, 15, new CommonItem(20000, "gp", "Ring of Invisibility")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 16, 19, new CommonItem(20000, "gp", "Ring of Wizardry (I)")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 20, 25, new CommonItem(25000, "gp", "Ring of Evasion")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 26, 28, new CommonItem(25000, "gp", "Ring of X-Ray Vision")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 29, 32, new CommonItem(27000, "gp", "Ring of Blinking")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 33, 36, new CommonItem(27000, "gp", "Ring of Meld into Stone")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 37, 43, new CommonItem(28000, "gp", "Ring of Major Energy Resistance")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 44, 50, new CommonItem(32000, "gp", "Ring of Protection +4")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 51, 55, new CommonItem(40000, "gp", "Ring of Wizardry (II)")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 56, 60, new CommonItem(40000, "gp", "Ring of Freedom of Movement")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 61, 63, new CommonItem(44000, "gp", "Ring of Greater Energy Resistance")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 64, 65, new CommonItem(50000, "gp", "Ring of Friend Shield (Pair)")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 66, 70, new CommonItem(50000, "gp", "Ring of Protection +5")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 71, 74, new CommonItem(50000, "gp", "Ring of Shooting Stars")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 75, 79, new CommonItem(50000, "gp", "Ring of Spell Storing")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 80, 83, new CommonItem(70000, "gp", "Ring of Wizardry (III)")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 84, 86, new CommonItem(75000, "gp", "Ring of Telekinesis")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 87, 88, new CommonItem(90000, "gp", "Ring of Regeneration")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 89, 89, new CommonItem(97950, "gp", "Ring of Three Wishes")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 90, 92, new CommonItem(98280, "gp", "Ring of Spell Turning")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 93, 94, new CommonItem(100000, "gp", "Ring of Wizardry (IV)")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 95, 95, new CommonItem(125000, "gp", "Ring of Djinni Calling")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 96, 96, new CommonItem(200000, "gp", "Ring of Elemental Command (Air)")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 97, 97, new CommonItem(200000, "gp", "Ring of Elemental Command (Earth)")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 98, 98, new CommonItem(200000, "gp", "Ring of Elemental Command (Fire)")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 99, 99, new CommonItem(200000, "gp", "Ring of Elemental Command (Water)")));
            treasure_pile.AddTreasure(new ItemType("Medium Ring", 100, 100, new CommonItem(200000, "gp", "Ring of Major Spell Storing")));
            return treasure_pile;
        }

        public static Treasure_Leaf CreateMajorRingsList()
        {
            return SetupMajorRingTypes();
        }

        private static Treasure_Leaf SetupMajorWandTypes()
        {
            var wand_list = new Treasure_Leaf();
            wand_list.AddTreasure(new ItemType("Major Wand", 0, 2, new CommonItem(5250, "gp", "Wand of Magic Missile (7th level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 3, 5, new CommonItem(6750, "gp", "Wand of Magic Missile (9th level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 6, 7, new CommonItem(11250, "gp", "Wand of Call Lightning (5th level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 8, 8, new CommonItem(11250, "gp", "Wand of Charm Person, Heightened (3rd level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 9, 10, new CommonItem(11250, "gp", "Wand of Contagion")));
            wand_list.AddTreasure(new ItemType("Major Wand", 11, 13, new CommonItem(11250, "gp", "Wand of Cure Serious Wounds")));
            wand_list.AddTreasure(new ItemType("Major Wand", 14, 15, new CommonItem(11250, "gp", "Wand of Dispel Magic")));
            wand_list.AddTreasure(new ItemType("Major Wand", 16, 17, new CommonItem(11250, "gp", "Wand of Fireball (5th level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 18, 19, new CommonItem(11250, "gp", "Wand of Keen Edge")));
            wand_list.AddTreasure(new ItemType("Major Wand", 20, 21, new CommonItem(11250, "gp", "Wand of Lightning Bolt (5th level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 22, 23, new CommonItem(11250, "gp", "Wand of Major Image")));
            wand_list.AddTreasure(new ItemType("Major Wand", 24, 25, new CommonItem(11250, "gp", "Wand of Slow")));
            wand_list.AddTreasure(new ItemType("Major Wand", 26, 27, new CommonItem(11250, "gp", "Wand of Suggestion")));
            wand_list.AddTreasure(new ItemType("Major Wand", 28, 29, new CommonItem(11250, "gp", "Wand of Summon Monster III")));
            wand_list.AddTreasure(new ItemType("Major Wand", 30, 31, new CommonItem(13500, "gp", "Wand of Fireball (6th level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 32, 33, new CommonItem(13500, "gp", "Wand of Lightning Bolt (6th)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 34, 35, new CommonItem(13500, "gp", "Wand of Searing Light (6th Level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 36, 37, new CommonItem(18000, "gp", "Wand of Call Lightning (8th level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 38, 39, new CommonItem(18000, "gp", "Wand of Fireball (8th level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 40, 41, new CommonItem(18000, "gp", "Wand of Lightning Bolt (8th level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 42, 45, new CommonItem(21000, "gp", "Wand of Charm Monster")));
            wand_list.AddTreasure(new ItemType("Major Wand", 46, 50, new CommonItem(21000, "gp", "Wand of Cure Critical Wounds")));
            wand_list.AddTreasure(new ItemType("Major Wand", 51, 52, new CommonItem(21000, "gp", "Wand of Dimensional Anchor")));
            wand_list.AddTreasure(new ItemType("Major Wand", 53, 55, new CommonItem(21000, "gp", "Wand of Fear")));
            wand_list.AddTreasure(new ItemType("Major Wand", 56, 59, new CommonItem(21000, "gp", "Wand of Greater Invisibility")));
            wand_list.AddTreasure(new ItemType("Major Wand", 60, 60, new CommonItem(21000, "gp", "Wand of Heightened Hold Person")));
            wand_list.AddTreasure(new ItemType("Major Wand", 61, 65, new CommonItem(21000, "gp", "Wand of Ice Storm")));
            wand_list.AddTreasure(new ItemType("Major Wand", 66, 68, new CommonItem(21000, "gp", "Wand of Inflict Critical Wounds")));
            wand_list.AddTreasure(new ItemType("Major Wand", 69, 72, new CommonItem(21000, "gp", "Wand of Neutralize Poison")));
            wand_list.AddTreasure(new ItemType("Major Wand", 73, 74, new CommonItem(21000, "gp", "Wand of Poison")));
            wand_list.AddTreasure(new ItemType("Major Wand", 75, 77, new CommonItem(21000, "gp", "Wand of Polymorph")));
            wand_list.AddTreasure(new ItemType("Major Wand", 78, 78, new CommonItem(21000, "gp", "Wand of Heightened Ray of Enfeeblement (4th level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 79, 79, new CommonItem(21000, "gp", "Wand of Heightened Suggestion (4th level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 80, 82, new CommonItem(21000, "gp", "Wand of Summon Monster IV")));
            wand_list.AddTreasure(new ItemType("Major Wand", 83, 86, new CommonItem(21000, "gp", "Wand of Wall of Fire")));
            wand_list.AddTreasure(new ItemType("Major Wand", 87, 90, new CommonItem(21000, "gp", "Wand of Wall of Ice")));
            wand_list.AddTreasure(new ItemType("Major Wand", 91, 91, new CommonItem(22500, "gp", "Wand of Dispel Magic (10th level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 92, 92, new CommonItem(22500, "gp", "Wand of Fireball (10th level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 93, 93, new CommonItem(22500, "gp", "Wand of Lightning Bolt (10th level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 94, 94, new CommonItem(24000, "gp", "Wand of Chaos Hammer (8th level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 95, 95, new CommonItem(24000, "gp", "Wand of Holy Smite (8th level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 96, 96, new CommonItem(24000, "gp", "Wand of Order's Wrath (8th level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 97, 97, new CommonItem(24000, "gp", "Wand of Unholy Blight (8th level)")));
            wand_list.AddTreasure(new ItemType("Major Wand", 98, 99, new CommonItem(26000, "gp", "Wand of Restoration")));
            wand_list.AddTreasure(new ItemType("Major Wand", 100, 100, new CommonItem(33500, "gp", "Wand of Stoneskin")));
            return wand_list;
        }

        public static Treasure_Leaf CreateMajorWandsList()
        {
            return SetupMajorWandTypes();
        }

        private static Treasure_Leaf SetupMajorStaffTypes()
        {
            var staff_list = new Treasure_Leaf();
            staff_list.AddTreasure(new ItemType("Medium Staff", 0, 3, new CommonItem(16500, "gp", "Staff of Charming")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 4, 9, new CommonItem(28500, "gp", "Staff of Fire")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 10, 11, new CommonItem(24750, "gp", "Staff of Swarming Insects")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 12, 17, new CommonItem(27750, "gp", "Staff of Healing")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 18, 19, new CommonItem(29000, "gp", "Staff of Size Alteration")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 20, 24, new CommonItem(48250, "gp", "Staff of Illumination")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 25, 31, new CommonItem(56250, "gp", "Staff of Frost")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 32, 38, new CommonItem(58250, "gp", "Staff of Defense")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 39, 43, new CommonItem(65000, "gp", "Staff of Abjuration")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 44, 48, new CommonItem(65000, "gp", "Staff of Conjuration")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 49, 53, new CommonItem(65000, "gp", "Staff of Enchantment")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 54, 58, new CommonItem(65000, "gp", "Staff of Evocation")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 59, 63, new CommonItem(65000, "gp", "Staff of Illusion")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 64, 68, new CommonItem(65000, "gp", "Staff of Necromancy")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 69, 73, new CommonItem(65000, "gp", "Staff of Transmutation")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 74, 77, new CommonItem(73500, "gp", "Staff of Divination")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 78, 82, new CommonItem(80500, "gp", "Staff of Earth and Stone")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 83, 87, new CommonItem(101250, "gp", "Staff of Woodlands")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 88, 92, new CommonItem(155750, "gp", "Staff of Life")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 93, 97, new CommonItem(170500, "gp", "Staff of Passage")));
            staff_list.AddTreasure(new ItemType("Medium Staff", 98, 100, new CommonItem(211000, "gp", "Staff of Power")));
            return staff_list;
        }

        public static Treasure_Leaf CreateMajorStaffsList()
        {
            return SetupMajorStaffTypes();
        }

        private static Treasure_Leaf SetupMajorWondrousTypes()
        {
            var item_list = new Treasure_Leaf();
            item_list.AddTreasure(new ItemType("Major Wondrous", 0, 1, new CommonItem(28000, "gp", "Dimensional Shackles")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 2, 2, new CommonItem(28500, "gp", "Obsidian Steed Figurine of Wondrous Power")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 3, 3, new CommonItem(30000, "gp", "Drums of Panic")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 4, 4, new CommonItem(30000, "gp", "Ioun Stone, Orange")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 5, 5, new CommonItem(30000, "gp", "Ioun Stone, Pale Green Prism")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 6, 6, new CommonItem(30000, "gp", "Lantern of Revealing")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 7, 7, new CommonItem(30000, "gp", "Robe of Blending")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 8, 8, new CommonItem(32000, "gp", "Amulet of Natural Armour +4")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 9, 9, new CommonItem(35000, "gp", "Amulet of Proof against Detection and Location")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 10, 10, new CommonItem(35000, "gp", "Carpet of Flying (5ft. by 10ft.)")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 11, 11, new CommonItem(35000, "gp", "Iron Golem Manual")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 12, 12, new CommonItem(36000, "gp", "Amulet of Health +6")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 13, 13, new CommonItem(36000, "gp", "Belt of Giant's Strength +6")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 14, 14, new CommonItem(36000, "gp", "Bracers of Armour +6")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 15, 15, new CommonItem(36000, "gp", "Cloak of Charisma +6")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 16, 16, new CommonItem(36000, "gp", "Gloves of Dexterity +6")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 17, 17, new CommonItem(36000, "gp", "Headband of Intellect +6")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 18, 18, new CommonItem(36000, "gp", "Ioun Stone, Vibrant Purple Prism")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 19, 19, new CommonItem(36000, "gp", "Pearl of Power (6th level)")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 20, 20, new CommonItem(36000, "gp", "Periapt of Wisdom +6")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 21, 21, new CommonItem(38000, "gp", "Scarab of Protection")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 22, 22, new CommonItem(40000, "gp", "Ioun Stone, Lavender and Green Ellipsoid")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 23, 23, new CommonItem(40000, "gp", "Ring Gates")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 24, 24, new CommonItem(42000, "gp", "Crystal Ball")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 25, 25, new CommonItem(44000, "gp", "Greater Stone Golem Manual")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 26, 26, new CommonItem(48000, "gp", "Orb of Storms")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 27, 27, new CommonItem(49000, "gp", "Boots of Teleportation")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 28, 28, new CommonItem(49000, "gp", "Bracers of Armour +7")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 29, 29, new CommonItem(49000, "gp", "Pearl of Power (7th level)")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 30, 30, new CommonItem(50000, "gp", "Amulet of Natural Armour +5")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 31, 31, new CommonItem(50000, "gp", "Major Cloak of Displacement")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 32, 32, new CommonItem(50000, "gp", "Crystal Ball with See Invisibility")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 33, 33, new CommonItem(50000, "gp", "Horn of Valhalla")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 34, 34, new CommonItem(51000, "gp", "Crystal Ball with Detect Thoughts")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 35, 35, new CommonItem(53000, "gp", "Carpet of Flying (6ft. by 9ft.)")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 36, 36, new CommonItem(54000, "gp", "Amulet of Mighty Fists +3")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 37, 37, new CommonItem(54000, "gp", "Wings of Flying")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 38, 38, new CommonItem(55000, "gp", "Cloak of Etherealness")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 39, 39, new CommonItem(55000, "gp", "Instant Fortress")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 40, 40, new CommonItem(55000, "gp", "Manual of Bodily Health +2")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 41, 41, new CommonItem(55000, "gp", "Manual of Gainful Experience +2")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 42, 42, new CommonItem(55000, "gp", "Manual of Quickness of Action +2")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 43, 43, new CommonItem(55000, "gp", "Tome of Clear Thought +2")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 44, 44, new CommonItem(55000, "gp", "Tome of Leadership and Influence +2")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 45, 45, new CommonItem(55000, "gp", "Tome of Understanding +2")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 46, 46, new CommonItem(56000, "gp", "Eyes of Charming")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 47, 47, new CommonItem(58000, "gp", "Robe of Stars")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 48, 48, new CommonItem(60000, "gp", "Carpet of Flying (10ft. by 10ft.)")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 49, 49, new CommonItem(60000, "gp", "Darkskull")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 50, 50, new CommonItem(62000, "gp", "Cube of Force")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 51, 51, new CommonItem(64000, "gp", "Bracers of Armour +8")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 52, 52, new CommonItem(64000, "gp", "Pearl of Power (8th level)")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 53, 53, new CommonItem(70000, "gp", "Crystal Ball with Telepathy")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 54, 54, new CommonItem(70000, "gp", "Greater Horn of Blasting")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 55, 55, new CommonItem(70000, "gp", "Pearl of Power (two spells)")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 56, 56, new CommonItem(73500, "gp", "Helm of Teleportation")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 57, 57, new CommonItem(75000, "gp", "Gem of Seeing")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 58, 58, new CommonItem(75000, "gp", "Robe of the Archmagi")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 59, 59, new CommonItem(76000, "gp", "Mantle of Faith")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 60, 60, new CommonItem(80000, "gp", "Crystal Ball with True Seeing")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 61, 61, new CommonItem(81000, "gp", "Pearl of Power (9th level)")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 62, 62, new CommonItem(82000, "gp", "Well of Many Words")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 63, 63, new CommonItem(82500, "gp", "Manual of Bodily Health +3")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 64, 64, new CommonItem(82500, "gp", "Manual of Gainful Experience +3")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 65, 65, new CommonItem(82500, "gp", "Manual of Quickness of Action +3")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 66, 66, new CommonItem(82500, "gp", "Tome of Clear Thought +3")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 67, 67, new CommonItem(82500, "gp", "Tome of Leadership and Influence +3")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 68, 68, new CommonItem(82500, "gp", "Tome of Understanding +3")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 69, 69, new CommonItem(90000, "gp", "Apparatus of the Crab")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 70, 70, new CommonItem(90000, "gp", "Mantle of Spell Resistance")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 71, 71, new CommonItem(92000, "gp", "Mirror of Opposition")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 72, 72, new CommonItem(95800, "gp", "Greater Strand of Prayer Beads")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 73, 73, new CommonItem(96000, "gp", "Amulet of Mighty Fists +4")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 74, 74, new CommonItem(98000, "gp", "Eyes of Petrification")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 75, 75, new CommonItem(100000, "gp", "Bowl of Commanding Water Elementals")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 76, 76, new CommonItem(100000, "gp", "Brazier of Commanding Fire Elementals")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 77, 77, new CommonItem(100000, "gp", "Censer of Controlling Air Elementals")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 78, 78, new CommonItem(100000, "gp", "Stone of Controlling Earth Elementals")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 79, 79, new CommonItem(110000, "gp", "Manual of Bodily Health +4")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 80, 80, new CommonItem(110000, "gp", "Manual of Gainful Experience +4")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 81, 81, new CommonItem(110000, "gp", "Manual of Quickness of Action +4")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 82, 82, new CommonItem(110000, "gp", "Tome of Clear Thought +4")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 83, 83, new CommonItem(110000, "gp", "Tome of Leadership and Influence +4")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 84, 84, new CommonItem(110000, "gp", "Tome of Understanding +4")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 85, 85, new CommonItem(120000, "gp", "Amulet of the Planes")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 86, 86, new CommonItem(120000, "gp", "Robe of Eyes")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 87, 87, new CommonItem(125000, "gp", "Helm of Brilliance")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 88, 88, new CommonItem(137500, "gp", "Manual of Bodily Health +5")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 89, 89, new CommonItem(137500, "gp", "Manual of Gainful Experience +5")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 90, 90, new CommonItem(137500, "gp", "Manual of Quickness of Action +5")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 91, 91, new CommonItem(137500, "gp", "Tome of Clear Thought +5")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 92, 92, new CommonItem(137500, "gp", "Tome of Leadership and Influence +5")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 93, 93, new CommonItem(137500, "gp", "Tome of Understanding")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 94, 94, new CommonItem(145000, "gp", "Efreeti Bottle")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 95, 95, new CommonItem(150000, "gp", "Amulet of Mighty Fists +5")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 96, 96, new CommonItem(160000, "gp", "Chaos Diamond")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 97, 97, new CommonItem(164000, "gp", "Cubic Gate")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 98, 98, new CommonItem(170000, "gp", "Iron Flask")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 99, 99, new CommonItem(175000, "gp", "Mirror of Mental Powers")));
            item_list.AddTreasure(new ItemType("Major Wondrous", 100, 100, new CommonItem(200000, "gp", "Mirror of Life Trapping")));
            return item_list;
        }

        public static Treasure_Leaf CreateMajorWondrousList()
        {
            return SetupMajorWondrousTypes();
        }

        private static Treasure_Composite SetupMajorItemTypes()
        {
            var treasure_pile = new Treasure_Composite();
            treasure_pile.AddTreasure(new TreasureType("Major Armour and Shields", 0, 10, CreateMajorArmourShieldList()));
            treasure_pile.AddTreasure(new TreasureType("Major Weapons", 11, 20, CreateMajorWeaponsList()));
            treasure_pile.AddTreasure(new TreasureType("Major Potions", 21, 25, CreateMajorPotionsList()));
            treasure_pile.AddTreasure(new TreasureType("Major Rings", 26, 35, CreateMajorRingsList()));
            treasure_pile.AddTreasure(new TreasureType("Major Rods", 36, 45, CreateMajorRodsList()));
            treasure_pile.AddTreasure(new TreasureType("Major Scrolls", 46, 55, CreateMajorScrollList()));
            treasure_pile.AddTreasure(new TreasureType("Major Staffs", 56, 75, CreateMajorStaffsList()));
            treasure_pile.AddTreasure(new TreasureType("Major Wands", 76, 80, CreateMajorWandsList()));
            treasure_pile.AddTreasure(new TreasureType("Major Wondrous", 81, 100, CreateMajorWondrousList()));
            return treasure_pile;
        }

        public static Treasure_Composite CreateMajorItemList()
        {
            return SetupMajorItemTypes();
        }
    }

}
